/** @type {import('next').NextConfig} */
const { i18n } = require('./next-i18next.config');

const securityHeaders = [];
const nextConfig = {
    distDir: 'dist',
    compiler: {
    // ssr and displayName are configured by default
        styledComponents: true,
    },
    experimental: {
        forceSwcTransforms: true,
    },
    webpack(config, {isServer}) {
        if (!isServer) {
            config.resolve = {
                ...config.resolve,
                fallback: {
                    ...config.resolve.fallback,
                    fs: false,
                },
            };
        }
        config.module = {
            ...config.module,
            exprContextCritical: false,
        };
        config.module.rules.push({
            test: /\.svg$/,
            use: ['@svgr/webpack'],
        });
        return config;
    },
    env: {
        BASE_URL: process.env.BASE_URL,
        API_URL: process.env.API_URL,
        GOOGLE_RECAPTCHA_KEY: process.env.GOOGLE_RECAPTCHA_KEY,
    },
    images: {
        domains: ['res.cloudinary.com'],
    },
    eslint: {
        ignoreDuringBuilds: true,
    },
    outputFileTracing: true,
    trailingSlash: true,
    i18n,
    webpackDevMiddleware: (config) => {
        config.watchOptions = {
            poll: 1000,  
            aggregateTimeout: 300,  
        };
        return config;
    },
    async headers() {
        return [
            {
                source: '/(.*)',
                headers: [
                    {
                        key: 'X-Frame-Options',
                        value: 'DENY',
                    },
                    {
                        key: 'Content-Security-Policy',
                        value:
                  'upgrade-insecure-requests',
                    },
                    {
                        key: 'X-Content-Type-Options',
                        value: 'nosniff',
                    },
                    {
                        key: 'Referrer-Policy',
                        value: 'origin-when-cross-origin',
                    },
                ],
            },
        ];
    },
};

module.exports = nextConfig;
