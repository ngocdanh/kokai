import Layout from '@common/Layout';
import { routeRoot } from '@common/config';
import Account from '@components/Account';
import { IAccountDetail } from '@components/Account/AccountDetail/types';
import { ITrackingOrder } from '@components/Account/TrackOrder/types';
import { IBreadcrumbItem } from '@helpers/BreadcrumbCustom/types';
import { GetStaticProps } from 'next';
import { useTranslation } from 'next-i18next';
import { serverSideTranslations } from 'next-i18next/serverSideTranslations';
import { useState } from 'react';
import { SubmitHandler } from 'react-hook-form';

const AccountPage = () => {
    
    const { t } = useTranslation();

    const [loadingSubmit, setLoadingSubmit] = useState<boolean>(false);


    const breadcrumbs:  IBreadcrumbItem[] = [
        {
            label:t('home'),
            url: routeRoot.home,
        },
        {
            label:t('login'),
            url: routeRoot.login
        },
    ];

    const yourOrderData = [
        {
            orderId: '#1357',
            date: 'March 15, 2021',
            status: 1,
            total: '$125.00 for 2 item',
            actions: 'View'
        },
        {
            orderId: '#1357',
            date: 'March 15, 2021',
            status: 0,
            total: '$125.00 for 2 item',
            actions: 'View'
        },
        {
            orderId: '#1357',
            date: 'March 15, 2021',
            status: 1,
            total: '$125.00 for 2 item',
            actions: 'View'
        },
        {
            orderId: '#1357',
            date: 'March 15, 2021',
            status: 0,
            total: '$125.00 for 2 item',
            actions: 'View'
        },
        {
            orderId: '#1357',
            date: 'March 15, 2021',
            status: 1,
            total: '$125.00 for 2 item',
            actions: 'View'
        },
    ];
    const AccountDetailData = {
        fistName: 'Minh Truong',
        lastName: 'Van',
        email: 'vanminhtruong678@gmail.com',
        phone: '0337672322',
    };

    const onSubmitTracking: SubmitHandler<ITrackingOrder> = (data: ITrackingOrder) => {
        console.log(data.orderId, data.email);
    };

    const onSubmitAccountDetail: SubmitHandler<IAccountDetail> = (data: IAccountDetail) => {
        console.log(data);
    };

    return (
        <Layout titlePage='account'>
            <Account 
                breadcrumbs={breadcrumbs}
                yourOrderData={yourOrderData}
                onSubmitTracking={onSubmitTracking}
                onSubmitAccountDetail={onSubmitAccountDetail}
                AccountDetailData={AccountDetailData}
            />
        </Layout>
    );
};

export const getStaticProps: GetStaticProps = async ({ locale }) => {
    return {
        props: {
            ...(await serverSideTranslations(locale!, ['common'])),
        },
    };
};

export default AccountPage;