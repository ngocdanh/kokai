import { routeRoot } from '@common/config';
import Layout from '@common/Layout';
import Register from '@components/Register';
import { IRegister } from '@components/Register/types';
import { IBreadcrumbItem } from '@helpers/BreadcrumbCustom/types';
import { GetStaticProps } from 'next';
import { useTranslation } from 'next-i18next';
import { serverSideTranslations } from 'next-i18next/serverSideTranslations';
import { SubmitHandler } from 'react-hook-form';
import * as React from 'react';
import { isEmpty } from 'lodash';
import { useDispatch, useSelector } from 'react-redux';
import { registerAction } from '@redux/actions';
import { toast } from 'react-toastify';

const RegisterPage = () => {
    const {t} = useTranslation();
    /* state */
    const [isRobot, setIsRobot] = React.useState<boolean>(true);
    const [loadingSubmit, setLoadingSubmit] = React.useState<boolean>(false);

    /* redux selector */
    const registerReducer = useSelector((state: any): {
        isLoading: boolean,
        isSuccess: boolean,
        message: string,
    } => state.register);

    React.useEffect(() => {
        const {isLoading, isSuccess, message} = registerReducer;
        if(isSuccess) {
            toast.success(message);
            setTimeout(() => {
                window.location.replace(routeRoot.home);
            }, 1000);
        }
    }, [registerReducer]);

    const dispatch = useDispatch();
    const breadcrumbs:IBreadcrumbItem[] = [
        {
            label:t('home'),
            url: routeRoot.home,
        },
        {
            label:t('register'),
        },
    ];

    const onSubmit: SubmitHandler<IRegister> = (data: IRegister) => {
        if(!isEmpty(data)){
            setLoadingSubmit(true);
            const dataSubmit: any = {};
            dataSubmit.fistName = data.fistName;
            dataSubmit.lastName = data.lastName;
            dataSubmit.password = data.password;
            dataSubmit.phone = data.phone;
            dataSubmit.email = data.email;
            dispatch(registerAction.fetchCreateNewAccount(dataSubmit));
        }
    }; 

    const onChangeGoogleRecaptcha = (value: string |null) => {
        setIsRobot(!value);
    };

    return (
        <Layout titlePage='register'>
            <Register 
                breadcrumbs={breadcrumbs}
                loadingSubmit={loadingSubmit}
                onSubmit={onSubmit}
                onChangeGoogleRecaptcha={onChangeGoogleRecaptcha}
                isRobot={isRobot}
            />
        </Layout>
    );
};
export const getStaticProps: GetStaticProps = async ({ locale }) => {
    return {
        props: {
            ...(await serverSideTranslations(locale!, ['common'])),
        },
    };
};

export default RegisterPage;