import Layout from '@common/Layout';
import Category from '@components/Category';
import React, { useEffect, useState } from 'react';
import {  
    getStaticPaths, 
    getStaticProps } from '@lib/query/ssr/category.ssr';
import { useRouter } from 'next/router';
import { useQueryListProduct, useQueryListProductPopular } from '@lib/query';
import { useQueryCategoryDetail } from '@lib/query/categories';
import queryString from 'query-string';

export { 
    getStaticPaths,
    getStaticProps };

export interface IFilterCategory  {
    page?: number,
    limit?: number,
    sortKey?: 'featured' | 'selling' | 'trending' | 'rated' | 'recently',
}

const initialFilterDefault: IFilterCategory = {
    page: 1,
    limit: 10,
    sortKey: 'featured'
};

const CategoryPage = () => {
    const router = useRouter();
    const { slug } = router.query;

    const [filters, setFilters] = useState<IFilterCategory>({});
    const [totalProductOfCategory, setTotalProductOfCategory] = useState<number>(0);
    const [pageCountProductOfCategory, setPageCountOfCategory] = useState<number>(0);
    const { listProductPopular } = useQueryListProductPopular({limit: 4});
    const {
        list: listProductOfCategory,
        paginator: paginatorProductOfCategory
    } = useQueryListProduct({
        category: slug, 
        limit: filters.limit, 
        sortKey: filters.sortKey,
        page: filters.page,
    }, {}, true);

    useEffect(() => {
        window.scroll({top: 250, left: 0, behavior: 'smooth' });
    }, [filters]);

    useEffect(() => {
        const {itemCount, pageCount} = paginatorProductOfCategory;
        setTotalProductOfCategory(itemCount);
        setPageCountOfCategory(pageCount);
    }, [paginatorProductOfCategory]);
    
    const { categoryDetail } = useQueryCategoryDetail({slug});
    const {
        name: titleCategory,
    } = categoryDetail;

    useEffect(() => {
        const query = window.location.search.substring(1);
        if(query) {
            const objParams = queryString.parse(query);
            setFilters({
                ...initialFilterDefault,
                ...objParams
            });
        } else {
            const query = `?${queryString.stringify(initialFilterDefault)}`;
            setFilters(initialFilterDefault);
            window.history.pushState('', '', query);
        }
    }, []);


    /* FUNCTION */
    const handleChangeFilters = (name: 'page' | 'limit' | 'sortKey', value: any) => {
        const filterTemp = {...filters};
        filterTemp[name] = value;

        setFilters(filterTemp);
        const query = `?${queryString.stringify(filterTemp)}`;
        window.history.pushState('', '', query);
    };
    
    const handleChangePage = (e: React.ChangeEvent<unknown>, value: number) => {
        setFilters({
            ...filters,
            page: value
        });
    };

    return (
        <Layout titlePage={titleCategory || 'category'}>
            <Category 
                categoryDetail={categoryDetail}
                dataListProduct={listProductPopular}
                filters={filters}
                listProductOfCategory={listProductOfCategory}
                handleChangeFilters={handleChangeFilters}
                totalProductOfCategory={totalProductOfCategory}
                pageCountProductOfCategory={pageCountProductOfCategory}
                handleChangePage={handleChangePage}
            />
        </Layout>
    );
};

export default CategoryPage;