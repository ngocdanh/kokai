import Layout from '@common/Layout';
import NotFound from '@components/NotFound';
import { GetStaticProps } from 'next';
import { serverSideTranslations } from 'next-i18next/serverSideTranslations';

const NotFoundPage = () => {
    return (
        <Layout titlePage='notFoundTitle'>
            <NotFound/>
        </Layout>
    );
};

export const getStaticProps: GetStaticProps = async ({ locale }) => {
    return {
        props: {
            ...(await serverSideTranslations(locale!, ['common'])),
        },
    };
};


export default NotFoundPage;