import Layout from '@common/Layout';
import Compare from '@components/Compare';
import { GetStaticProps } from 'next';
import { serverSideTranslations } from 'next-i18next/serverSideTranslations';

const ComparePage = () => {
    return (
        <Layout titlePage='blogs'>
            <Compare />
        </Layout>
    );
};

export const getStaticProps: GetStaticProps = async ({ locale }) => {
    return {
        props: {
            ...(await serverSideTranslations(locale!, ['common'])),
        },
    };
};

export default ComparePage;