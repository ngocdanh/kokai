import Layout from '@common/Layout';
import { routeRoot } from '@common/config';
import ResetPasswordComponent from '@components/ResetPassword';
import { IResetPassword } from '@components/ResetPassword/types';
import { IBreadcrumbItem } from '@helpers/BreadcrumbCustom/types';
import { GetStaticProps } from 'next';
import { useTranslation } from 'next-i18next';
import { serverSideTranslations } from 'next-i18next/serverSideTranslations';
import { useState } from 'react';
import { SubmitHandler } from 'react-hook-form';

const ResetPasswordPage = () => {

    const { t } = useTranslation();

    const [loadingSubmit, setLoadingSubmit] = useState<boolean>(false);


    const breadcrumbs:  IBreadcrumbItem[] = [
        {
            label:t('home'),
            url: routeRoot.home,
        },
        {
            label:t('login'),
            url: routeRoot.login
        },
    ];

    const onSubmit: SubmitHandler<IResetPassword> = (data: IResetPassword) => {
        setLoadingSubmit(true);
        setTimeout(() => {
            setLoadingSubmit(false);
        },2000);
        console.log({data});
    };

    return (
        <Layout titlePage='reset password'>
            <ResetPasswordComponent 
                breadcrumbs={breadcrumbs}
                onSubmit={onSubmit}
                loadingSubmit={loadingSubmit}
            />
        </Layout>
    );
};

export const getStaticProps: GetStaticProps = async ({ locale }) => {
    return {
        props: {
            ...(await serverSideTranslations(locale!, ['common'])),
        },
    };
};

export default ResetPasswordPage;