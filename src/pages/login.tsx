import Layout from '@common/Layout';
import Login from '@components/Login';
import { IBreadcrumbItem } from '@helpers/BreadcrumbCustom/types';
import { GetStaticProps } from 'next';
import { useTranslation } from 'next-i18next';
import * as React from 'react';
import { serverSideTranslations } from 'next-i18next/serverSideTranslations';
import { routeRoot } from '../common/config';
import { SubmitHandler } from 'react-hook-form';
import { ILogin } from '@components/Login/types';
import { useDispatch, useSelector } from 'react-redux';
import { toast } from 'react-toastify';
import { isEmpty } from 'lodash';
import { loginAction } from '@redux/actions';

const LoginPage = () => {
    const {t} = useTranslation();
    const [loadingSubmit, setLoadingSubmit] = React.useState<boolean>(false);
    const [isRobot, setIsRobot]  = React.useState<boolean>(true);

    const dispatch = useDispatch();
    /* redux selector */
    const loginData = useSelector((state: any): {
        isLoading: boolean,
        isSuccess: boolean,
        message: string,
    } => state.login);

    React.useEffect(() => {
        const {isLoading, isSuccess, message} = loginData;
        if(isSuccess) {
            toast.success(message);
            setTimeout(() => {
                window.location.replace(routeRoot.home);
            }, 1000);
        }
    }, [loginData]);

    const breadcrumbs:IBreadcrumbItem[] = [
        {
            label:t('home'),
            url: routeRoot.home,
        },
        {
            label:t('login'),
        },
   
    ];

    const onSubmit: SubmitHandler<ILogin> = (data: ILogin) => {
        if(!isEmpty(data)){
            setLoadingSubmit(true);
            const dataSubmit: any = {};
            dataSubmit.password = data.password;
            dataSubmit.email = data.email;
            dispatch(loginAction.fetchLoginAccount(dataSubmit));
        }
    };  

    const onChangeGoogleRecaptcha = (value: string |null) => {
        setIsRobot(!value);
    };
    return (
        <Layout titlePage='login'>
            <Login 
                breadcrumbs={breadcrumbs}
                loadingSubmit={loadingSubmit}
                onSubmit={onSubmit}
                onChangeGoogleRecaptcha={onChangeGoogleRecaptcha}
                isRobot={isRobot}
            />
        </Layout>
    );
};

export const getStaticProps: GetStaticProps = async ({ locale }) => {
    return {
        props: {
            ...(await serverSideTranslations(locale!, ['common'])),
        },
    };
};

export default LoginPage;