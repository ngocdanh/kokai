import Layout from '@common/Layout';
import { userInfo } from '@common/shared';
import HomeComponent from '@components/Home';
import { dataTrending } from '@components/Home/component/ProductTabs/types';
import { IHomePage } from '@components/Home/types';
import { useQueryListProduct, useQueryListProductBestSeller, useQueryListProductSaleOfDay } from '@lib/query';
import { useQueryListCategories } from '@lib/query/categories';
import { useQueryListSliderHome } from '@lib/query/others';
import { getStaticProps, getStaticPaths } from '@lib/query/ssr/home-page.ssr';
import { get, isEmpty, unionBy, uniqueId } from 'lodash';
import { useTranslation } from 'next-i18next';
import { MouseEvent, useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { toast } from 'react-toastify';
import { useQueryListProductPopular } from '../lib/query/products';
import * as React from 'react';
import { wishlistAction } from '@redux/actions';
import { showLoading } from '@redux/actions/global.action';

/* get static data */
export { getStaticProps, getStaticPaths };

const HomePage = (props: IHomePage) => {
    const { t } = useTranslation();
    const dispatch = useDispatch();

    React.useEffect(()=> {
        dispatch(showLoading());
    }, []);

    const defaultAllCategory = {
        slug: '',
        name: t('All'),
        icon: '',
    };
    /* Query data */
    const {listCategories} = useQueryListCategories();
    const {listProductBestSeller} = useQueryListProductBestSeller();
    const {listProductPopular} = useQueryListProductPopular();
    const {listProductSaleOfDay} = useQueryListProductSaleOfDay({}, {limit: 4});
    const {list: dataSlider} = useQueryListSliderHome();

    const {list: listTopSelling} = useQueryListProduct({sortKey: 'purchase', limit: 3}, {});
    const {list: listTopRate} = useQueryListProduct({sortKey: 'vote', limit: 3}, {});
    const {list: listRecently} = useQueryListProduct({sortKey: 'created_at', limit: 3}, {});
    const {list: listTrendingProducts} = useQueryListProduct({sortKey: 'view', limit: 3}, {});
    const dataTrending: dataTrending = { 
        listTopSelling ,
        listTopRate, 
        listRecently, 
        listTrendingProducts 
    };

    /* STATE */
    const [listCategoryProduct, setListCategoryProduct]: any = useState([]);
    const [listCategoryBestSeller, setListCategoryBestSeller]: any = useState([]);
    const [activeMenuProduct, setActiveMenuProduct] = useState(defaultAllCategory);
    const [activeMenuBestSeller, setActiveMenuBestSeller] = useState(defaultAllCategory);
    const {isSuccess: isSuccessWishlist, message:messageWishlist} = useSelector((state: any) => state.wishlist);    

    useEffect(()=> {
        if(!isEmpty(listProductPopular)) {
            let listCate = [defaultAllCategory];
            for(let i=0; i<listProductPopular.length; i++) {
                const categoryItem = get(listProductPopular[i], ['categories', '0'], defaultAllCategory);
                listCate = unionBy(listCate, [categoryItem], 'slug');
            }
            setListCategoryProduct(listCate);
        }
    }, [JSON.stringify(listProductPopular)]);

    useEffect(()=> {
        if(!isEmpty(listProductBestSeller)) {
            let listSeller = [defaultAllCategory];
            for(let i=0; i<listProductBestSeller.length; i++) {
                const sellerItem = get(listProductBestSeller[i], ['categories', '0'], defaultAllCategory);
                listSeller = unionBy(listSeller, [sellerItem], 'slug');
            }
            setListCategoryBestSeller(listSeller);
        }
    }, [JSON.stringify(listProductBestSeller)]);

    React.useEffect(() => {
        if(isSuccessWishlist && messageWishlist) {
            toast.success(messageWishlist, {
                toastId: uniqueId()
            });
        }
    }, [messageWishlist]);


    const handleChangeMenuProduct = (e: MouseEvent<HTMLLIElement>,cat: any) => {
        setActiveMenuProduct(cat);
    };
    const handleChangeMenuBestSeller = (e: MouseEvent<HTMLLIElement>,seller: any) => {
        setActiveMenuBestSeller(seller);
    };

    const handleAddToWishlist = (e: MouseEvent<HTMLElement>, id: string) => {
        e.stopPropagation();
        const productObjId = id;
        if(!userInfo?.userObjId && !productObjId) return;
        const set = {
            product: productObjId,
            userObjId: userInfo?.userObjId
        };
        dispatch(wishlistAction.fetchAddWishlist(set));
    };

    return (
        <Layout>
            <HomeComponent 
                dataListProduct={listProductPopular}
                dataListProductBestSeller={listProductBestSeller}
                dataListProductSaleOfDay={listProductSaleOfDay}
                dataSlider={dataSlider}
                dataCategory={listCategories}
                handleChangeMenuProduct={handleChangeMenuProduct}
                activeMenuProduct={activeMenuProduct}
                handleChangeMenuBestSeller={handleChangeMenuBestSeller}
                activeMenuBestSeller={activeMenuBestSeller}
                dataTrending={dataTrending}
                listCategoryProduct={listCategoryProduct}
                listCategoryBestSeller={listCategoryBestSeller}
                handleAddToWishlist={handleAddToWishlist}
            />
        </Layout>
    );
};

export default HomePage;
