import Layout from '@common/Layout';
import Search from '@components/Search';
import { GetStaticProps } from 'next';
import { serverSideTranslations } from 'next-i18next/serverSideTranslations';

const SearchPage = () => {
    return (
        <Layout titlePage='wishlist'>
            <Search />
        </Layout>
    );
};

export const getStaticProps: GetStaticProps = async ({ locale }) => {
    return {
        props: {
            ...(await serverSideTranslations(locale!, ['common'])),
        },
    };
};

export default SearchPage;