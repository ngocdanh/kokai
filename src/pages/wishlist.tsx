import { routeRoot } from '@common/config';
import Layout from '@common/Layout';
import Wishlist from '@components/Wishlist';
import { IBreadcrumbItem } from '@helpers/BreadcrumbCustom/types';
import { useQueryWishlist } from '@lib/query/wishlist';
import { GetStaticProps } from 'next';
import { useTranslation } from 'next-i18next';
import { serverSideTranslations } from 'next-i18next/serverSideTranslations';
import * as React from 'react';
import { isEmpty } from 'lodash';
import { userInfo } from '@common/shared';
import { useDispatch, useSelector } from 'react-redux';
import { wishlistAction } from '@redux/actions';
import { toast } from 'react-toastify';

const WishlistPage = () => {
    const { t } = useTranslation();
    const {listProducts, refetch} = useQueryWishlist({
        userObjId: userInfo?.userObjId,
    });
    const dispatch = useDispatch();
    const [total, setTotal] = React.useState(0);
    const {isSuccess, message} = useSelector((state: any)=> state.wishlist);

    const breadcrumbs:  IBreadcrumbItem[] = [
        {
            label:t('home'),
            url: routeRoot.home,
        },
        {
            label:t('wishlist'),
            url: routeRoot.wishlist
        },
    ];
    React.useEffect(()=> {
        if(isSuccess) {
            refetch();
        }
    }, [isSuccess]); 

    React.useEffect(() => {
        if(message) {
            toast.success(message);
        }
    }, [message]);

    React.useEffect(() =>{
        if(!isEmpty(listProducts)) {
            setTotal(listProducts?.length);
        }
    },[listProducts]);

    const handleDelete = (id: string) => {
        const set: any = {};
        set.userObjId = userInfo?.userObjId;
        set.product = id;
        dispatch(wishlistAction.fetchDeleteWishlist(set));
    };
      
    return (
        <Layout titlePage='wishlist'>
            <Wishlist breadcrumb={breadcrumbs}
                wishListData={listProducts}
                total={total}
                handleDelete={handleDelete} />
        </Layout>
    );
};

export const getStaticProps: GetStaticProps = async ({ locale }) => {
    return {
        props: {
            ...(await serverSideTranslations(locale!, ['common'])),
        },
    };
};

export default WishlistPage;