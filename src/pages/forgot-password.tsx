import Layout from '@common/Layout';
import { routeRoot } from '@common/config';
import ForgotPassword from '@components/ForgotPassword';
import { IForgotPassword } from '@components/ForgotPassword/types';
import { IBreadcrumbItem } from '@helpers/BreadcrumbCustom/types';
import { GetStaticProps } from 'next';
import { useTranslation } from 'next-i18next';
import { serverSideTranslations } from 'next-i18next/serverSideTranslations';
import { useRouter } from 'next/router';
import { useState } from 'react';
import { SubmitHandler } from 'react-hook-form';

const ForgotPasswordPage = () => {

    const { t } = useTranslation();
    const router = useRouter();

    const [loadingSubmit, setLoadingSubmit] = useState<boolean>(false);
    const [isRobot, setIsRobot]  = useState<boolean>(true);

    const breadcrumbs:  IBreadcrumbItem[] = [
        {
            label:t('home'),
            url: routeRoot.home,
        },
        {
            label:t('login'),
            url: routeRoot.login
        },
    ];

    const onSubmit: SubmitHandler<IForgotPassword> = (data: IForgotPassword) => {
        setLoadingSubmit(true);
        setTimeout(() => {
            setLoadingSubmit(false);
            router.push({
                pathname: '/reset-password',
            });
        },2000);
        console.log({data});
    };

    const onChangeGoogleRecaptcha = (value: string |null) => {
        setIsRobot(!value);
    };

    return (
        <Layout titlePage='forgot password'>
            <ForgotPassword 
                breadcrumbs={breadcrumbs}
                onSubmit={onSubmit}
                loadingSubmit={loadingSubmit}
                onChangeGoogleRecaptcha={onChangeGoogleRecaptcha}
                isRobot={isRobot}
            />
        </Layout>
    );
};

export const getStaticProps: GetStaticProps = async ({ locale }) => {
    return {
        props: {
            ...(await serverSideTranslations(locale!, ['common'])),
        },
    };
};

export default ForgotPasswordPage;