import Layout from '@common/Layout';
import Blogs from '@components/Blog';
import { GetStaticProps } from 'next';
import { serverSideTranslations } from 'next-i18next/serverSideTranslations';

const BlogPage = () => {
    return (
        <Layout titlePage='blogs'>
            <Blogs />
        </Layout>
    );
};

export const getStaticProps: GetStaticProps = async ({ locale }) => {
    return {
        props: {
            ...(await serverSideTranslations(locale!, ['common'])),
        },
    };
};

export default BlogPage;