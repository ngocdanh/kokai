import Layout from '@common/Layout';
import { routeRoot } from '@common/config';
import Cart from '@components/Cart';
import { IBreadcrumbItem } from '@helpers/BreadcrumbCustom/types';
import { GetStaticProps } from 'next';
import { useTranslation } from 'next-i18next';
import { serverSideTranslations } from 'next-i18next/serverSideTranslations';
import { useState } from 'react';


const CartPage = () => {

    const { t } = useTranslation();

    const breadcrumbs:  IBreadcrumbItem[] = [
        {
            label:t('home'),
            url: routeRoot.home,
        },
        // {
        //     label:t('shop'),
        //     url: routeRoot.wishlist
        // },
        {
            label:t('cart'),
            url: routeRoot.cart
        },
    ];

    const cartViewData = [
        {
            'name': 'Blood Drinker',
            'slug': 'blood-drinker',
            'price': 2.5,
            'sale_price': 2.5,
            'quantity': 1000,
            'cart_quantity': 1,
            'in_stock': 1,
            'id_': '1',
            'image': {
                'original': 'https://pickbazarlaravel.s3.ap-southeast-1.amazonaws.com/1620/Thriller-7.jpg',
                'thumbnail': 'https://pickbazarlaravel.s3.ap-southeast-1.amazonaws.com/1620/conversions/Thriller-7-thumbnail.jpg'
            },
            'categories': [
                {
                    'name': 'Thriller',
                    'slug': 'thriller',
                }
            ],
            'vote': 3.42,
            'discount': null,
            'total': 934,
            'seller_rank': 29700,
            'sold': 260,
            'promotion_code': null
        },
        {
            'name': 'Forest Killer',
            'slug': 'forest-killer',
            'price': 260,
            'sale_price': 260,
            'quantity': 500,
            'cart_quantity': 1,
            'in_stock': 1,
            'id_': '2',
            'image': {
                'original': 'https://pickbazarlaravel.s3.ap-southeast-1.amazonaws.com/1614/Thriller.jpg',
                'thumbnail': 'https://pickbazarlaravel.s3.ap-southeast-1.amazonaws.com/1614/conversions/Thriller-thumbnail.jpg'
            },
            'categories': [
                {
                    'name': 'Thriller',
                    'slug': 'thriller',
                }
            ],
            'vote': 3.19,
            'discount': null,
            'total': 487,
            'seller_rank': 10500,
            'sold': 154,
            'promotion_code': null
        },
        {
            'name': 'Fleash Eater',
            'slug': 'fleash-eater',
            'price': null,
            'sale_price': null,
            'quantity': 500,
            'cart_quantity': 1,
            'in_stock': 1,
            'id_': '3',
            'image': {
                'original': 'https://pickbazarlaravel.s3.ap-southeast-1.amazonaws.com/1615/Thriller-1.jpg',
                'thumbnail': 'https://pickbazarlaravel.s3.ap-southeast-1.amazonaws.com/1615/conversions/Thriller-1-thumbnail.jpg'
            },
            'categories': [
                {
                    'name': 'Thriller',
                    'slug': 'thriller',
                }
            ],
            'vote': 3.49,
            'discount': 6,
            'total': 445,
            'seller_rank': 10000,
            'sold': 182,
            'promotion_code': null
        },
        {
            'name': 'The Boneyard Man',
            'slug': 'the-boneyard-man',
            'price': 120,
            'sale_price': 120,
            'quantity': 1000,
            'cart_quantity': 1,
            'in_stock': 1,
            'id_': '4',
            'image': {
                'original': 'https://pickbazarlaravel.s3.ap-southeast-1.amazonaws.com/1616/Thriller-2.jpg',
                'thumbnail': 'https://pickbazarlaravel.s3.ap-southeast-1.amazonaws.com/1616/conversions/Thriller-2-thumbnail.jpg'
            },
            'categories': [
                {
                    'name': 'Thriller',
                    'slug': 'thriller',
                }
            ],
            'vote': 4.64,
            'discount': null,
            'total': 339,
            'seller_rank': 24400,
            'sold': 233,
            'promotion_code': null
        },
        {
            'name': 'The Physco Killer First Part',
            'slug': 'the-physco-killer-first-part',
            'price': 200,
            'sale_price': 200,
            'quantity': 500,
            'cart_quantity': 2,
            'in_stock': 1,
            'id_': '5',
            'image': {
                'original': 'https://pickbazarlaravel.s3.ap-southeast-1.amazonaws.com/1617/Thriller-3.jpg',
                'thumbnail': 'https://pickbazarlaravel.s3.ap-southeast-1.amazonaws.com/1617/conversions/Thriller-3-thumbnail.jpg'
            },
            'categories': [
                {
                    'name': 'Thriller',
                    'slug': 'thriller',
                }
            ],
            'vote': 3.75,
            'discount': null,
            'total': 576,
            'seller_rank': 8300,
            'sold': 46,
            'promotion_code': null
        }
    ];

    const [cartData, setCartData] = useState(cartViewData);
    const [total, setTotal] = useState(cartData?.length);
    const handleCoupon = (coupon?: string) => {
        console.log(coupon);
    };
    const handleDelete = (id?: string) => {
        if (id === undefined) {
            setTotal(0);
            setCartData([]);
        }else {
            const newData = cartData.filter(item => item.id_ != id);
            setTotal(newData?.length);
            setCartData(newData);
        }
        
    };

    const QuantityHasChange = (id: string, quantity: number) => {
        const newData = cartData?.map((item) => {
            if(item.id_ === id) {
                return{
                    ...item,
                    cart_quantity: quantity
                };
            }else {
                return item;
            }
        });
        setCartData(newData);
    };
    

    return (
        <Layout titlePage='blogs'>
            <Cart 
                breadcrumb={breadcrumbs}
                cartData={cartData}
                total={total}
                handleCoupon={handleCoupon}
                handleDelete={handleDelete}
                QuantityHasChange={QuantityHasChange}
            />
        </Layout>
    );
};

export const getStaticProps: GetStaticProps = async ({ locale }) => {
    return {
        props: {
            ...(await serverSideTranslations(locale!, ['common'])),
        },
    };
};

export default CartPage;