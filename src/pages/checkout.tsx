import Layout from '@common/Layout';
import Checkout from '@components/Checkout';
import { GetStaticProps } from 'next';
import { serverSideTranslations } from 'next-i18next/serverSideTranslations';

const CheckoutPage = () => {
    return (
        <Layout titlePage='blogs'>
            <Checkout />
        </Layout>
    );
};

export const getStaticProps: GetStaticProps = async ({ locale }) => {
    return {
        props: {
            ...(await serverSideTranslations(locale!, ['common'])),
        },
    };
};

export default CheckoutPage;