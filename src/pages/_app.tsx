import type { AppProps } from 'next/app';
import '../../public/assets/scss/globals.scss';
import '../../public/assets/scss/custom.scss';
import { StylesProvider, createGenerateClassName } from '@mui/styles';
import 'swiper/css';
import 'swiper/css/pagination';
import { theme } from '@common/theme';
import { ThemeProvider } from '@mui/material';
import { Provider } from 'react-redux';
import configureStore from '@redux/configureStore';
import { appWithTranslation } from 'next-i18next';
import { Hydrate, QueryClient, QueryClientProvider } from 'react-query';
import { useState } from 'react';
import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
const generateClassName = createGenerateClassName({
    productionPrefix: 'c',
});

const store = configureStore();

function MyApp({ Component, pageProps }: AppProps) {
    const [queryClient] = useState(() => new QueryClient({
        defaultOptions: {
            queries: {
                refetchOnWindowFocus: false,
            },
        },
    }));
    return (
        <QueryClientProvider client={queryClient}>
            <Hydrate state={pageProps.dehydratedState}>
                <Provider store={store}>
                    <ThemeProvider theme={theme}>
                        <StylesProvider generateClassName={generateClassName}>
                            <ToastContainer
                                position="top-right"
                                autoClose={1500}
                                hideProgressBar={true}
                                closeOnClick
                                pauseOnHover
                                theme="light"
                                limit={1}
                                newestOnTop={false}
                                rtl={false}
                                pauseOnFocusLoss={false}
                            />
                            <Component {...pageProps} />
                        </StylesProvider>
                    </ThemeProvider>
                </Provider>
            </Hydrate>
        </QueryClientProvider>
    );
}

export default appWithTranslation(MyApp);
