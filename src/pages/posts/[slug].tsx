import Layout from '@common/Layout';
import Posts from '@components/Posts';

const CategoryPage = () => {
    return (
        <Layout titlePage='category'>
            <Posts />
        </Layout>
    );
};

export default CategoryPage;