import Layout from '@common/Layout';
import ProductDetail from '@components/ProductDetail';
import React from 'react';
import { 
    getStaticPaths,
    getStaticProps } from '@lib/query/ssr/product.ssr';
import { useRouter } from 'next/router';
import { useQueryProductDetail, useQueryListProduct } from '@lib/query';

export { 
    getStaticPaths, 
    getStaticProps };
const ProductDetailPage = () => {
    const router = useRouter();
    
    const { slug } = router.query;
    const { product } = useQueryProductDetail({ slug });
    const { list: listRelatedProducts } = useQueryListProduct({ limit: 4 }, {});
    const { list: listProductPopular } = useQueryListProduct({ limit: 4, sortKey: 'view' }, {});

    return (
        <Layout titlePage={product?.name || 'product'}>
            <ProductDetail product={product}
                listRelatedProducts={listRelatedProducts}
                listProductPopular={listProductPopular}
            />
        </Layout>
    );
};

export default ProductDetailPage;