import Layout from '@common/Layout';
import ContactUS from '@components/ContactUs';
import { GetStaticProps } from 'next';
import { serverSideTranslations } from 'next-i18next/serverSideTranslations';

const AboutPage = () => {
    return (
        <Layout titlePage='contact-us'>
            <ContactUS />
        </Layout>
    );
};


export const getStaticProps: GetStaticProps = async ({ locale }) => {
    return {
        props: {
            ...(await serverSideTranslations(locale!, ['common'])),
        },
    };
};

export default AboutPage;