import * as yup from 'yup';

const regexPassword = /^(?=.*[a-z])(?=.*[0-9])(?=.{4,})/;
const regexPhone = /(03|05|07|08|09|01[2|6|8|9])+([0-9]{8})\b/;

// Mỗi Form phải dùng schema khác nhau vì useForm sẽ dùng các trường trong schema để đăng ký Field
// Khi nhập đủ Field thì mới submit được
const schemaRegister = yup.object({

    fistName: yup.string()
        .required('fistName is required')
        .min(1, 'fistName must be of 1 characters.')
        .max(16, 'fistName max of 16 characters.'),
    lastName: yup.string()
        .required('lastName is required')
        .min(1, 'lastName must be of 1 characters.')
        .max(16, 'lastName max of 16 characters.'),
    email: yup.string()
        .required('Email is required')
        .email('Email must be email'),
    phone: yup.string()
        .required('Password is required')
        .matches(regexPhone,'Phone number is not in the correct format'),
    password: yup.string()
        .required('Password is required')
        .min(4, 'Password must be of 5 characters')
        .matches(regexPassword,'Password must contain One Lowercase, One Number'),

    confirm_password: yup.string()
        .required('Confirm Password is required')
        .oneOf([yup.ref('password')], 'Your passwords does not match'),
});

const schemaLogin = yup.object({
    email: yup.string()
        .required('Email is required')
        .email('Email must be email'),
    password: yup.string()
        .required('Password is required')
        .min(4, 'Password must be of 5 characters')
        .matches(regexPassword,'Password must contain One Lowercase, One Number'),
});

const schemaForgotPassword = yup.object({
    emailOrUsername: yup.string()
        .required('Email or Username is required')
        .email('Email or Username  must be email'),
});

const schemaResetPassword = yup.object({
    password: yup.string()
        .required('Password is required')
        .min(4, 'Password must be of 5 characters')
        .matches(regexPassword,'Password must contain One Lowercase, One Number'),

    confirm_password: yup.string()
        .required('Confirm Password is required')
        .oneOf([yup.ref('password')], 'Your passwords does not match'),

});

const schemaTrackingOrder = yup.object({
    orderId: yup.string()
        .required('OrderID is required')
        .min(3, 'OrderID must be of 4 characters'),
    email: yup.string()
        .required('Email is required')
        .email('Email must be email'),
});

const schemaAccountDetail = yup.object({
    fistName: yup.string()
        .required('fistName is required')
        .min(1, 'fistName must be of 1 characters.')
        .max(16, 'fistName max of 16 characters.'),
    lastName: yup.string()
        .required('lastName is required')
        .min(1, 'lastName must be of 1 characters.')
        .max(16, 'lastName max of 16 characters.'),
    email: yup.string()
        .required('Email is required')
        .email('Email must be email'),
    phone: yup.string()
        .required('Password is required')
        .matches(regexPhone,'Phone number is not in the correct format'),
});

export { schemaRegister, schemaLogin, schemaForgotPassword, schemaResetPassword, schemaTrackingOrder, schemaAccountDetail };
