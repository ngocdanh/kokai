export const FONT_UICONS = {
    fontFamily: 'uicons-regular-straight  !important',
};

/* METHOD <GET, POST, DELETE, PUT> */
export const GET = 'get';
export const POST = 'post';
export const PUT = 'put';
export const DELETE = 'delete';
export const PATCH = 'patch';
export const OPTIONS = 'options';

export const lowerCase = (str: string) => str.toLowerCase(); 
export const upperCase = (str: string) => str.toUpperCase(); 

export const LANG_VI = 'vi';
export const LANG_EN = 'en';

export const STATUS_CODE = {
    SUCCESS: 200,
    CREATED: 201,
    UPDATED: 202,
};

export const colorize = (args: any) => ({
    black: `\x1b[30m${args.join(' ')}`,
    red: `\x1b[31m${args.join(' ')}`,
    green: `\x1b[32m${args.join(' ')}`,
    yellow: `\x1b[33m${args.join(' ')}`,
    blue: `\x1b[34m${args.join(' ')}`,
    magenta: `\x1b[35m${args.join(' ')}`,
    cyan: `\x1b[36m${args.join(' ')}`,
    white: `\x1b[37m${args.join(' ')}`,
    bgBlack: `\x1b[40m${args.join(' ')}\x1b[0m`,
    bgRed: `\x1b[41m${args.join(' ')}\x1b[0m`,
    bgGreen: `\x1b[42m${args.join(' ')}\x1b[0m`,
    bgYellow: `\x1b[43m${args.join(' ')}\x1b[0m`,
    bgBlue: `\x1b[44m${args.join(' ')}\x1b[0m`,
    bgMagenta: `\x1b[45m${args.join(' ')}\x1b[0m`,
    bgCyan: `\x1b[46m${args.join(' ')}\x1b[0m`,
    bgWhite: `\x1b[47m${args.join(' ')}\x1b[0m`
});

export const STORAGE = {
    USER: 'USER_INFO',
    ACCESS_TOKEN: 'ACCESS_TOKEN_USER',
    REFRESH_TOKEN: 'REFRESH_TOKEN_USER',
};