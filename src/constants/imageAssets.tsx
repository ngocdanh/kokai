/* PNG/JPG */
const imageAppstore = require('../../public/assets/imgs/theme/app-store.jpg');
const googlePlay = require('../../public/assets/imgs/theme/google-play.jpg');
const payment = '/assets/imgs/theme/payment-method.png';
const facebook = '/assets/imgs/theme/icons/icon-facebook-white.svg';
const twitter = '/assets/imgs/theme/icons/icon-twitter-white.svg';
const instagram = '/assets/imgs/theme/icons/icon-instagram-white.svg';
const youtube = '/assets/imgs/theme/icons/icon-youtube-white.svg';
const loading = '/assets/imgs/theme/loading.gif';
const banner1 = '/assets/imgs/banner/banner-1.png';
const banner2 = '/assets/imgs/banner/banner-2.png';
const banner3 = '/assets/imgs/banner/banner-3.png';
const banner12 = '/assets/imgs/banner/banner-12.png';
const banner13 = '/assets/imgs/banner/banner-13.png';
const iconVN = '/assets/imgs/theme/icon-vi.png';
const iconUS = '/assets/imgs/theme/icon-us.png';
const detailAd = '/assets/imgs/banner/detail-ad.png';

const thumbnail4 = '/assets/imgs/shop/thumbnail-4.jpg';
const thumbnail5 = '/assets/imgs/shop/thumbnail-5.jpg';
const thumbnail11 = '/assets/imgs/shop/thumbnail-11.jpg';
const thumbnail12 = '/assets/imgs/shop/thumbnail-12.jpg';

const loginImg = '/assets/imgs/page/login-1.png';
const banner11 = '/assets/imgs/banner/banner-11.png';
const filterWidgetBg = '/assets/imgs/banner/fillter-widget-bg.png';
const headerBackground = '/assets/imgs/blog/header-bg.png';
const touchId = '/assets/imgs/page/touch-id.png';
const updatePass = '/assets/imgs/page/update-pass.png';

/* SVG */
const iconRating = require('../../public/assets/imgs/theme/icons/rating.svg').default;
const imageLogo = require('../../public/assets/imgs/theme/logo.svg').default;
const imgGoogle = require('../../public/assets/imgs/theme/icons8-google.svg').default;
const imgFacebook = require('../../public/assets/imgs/theme/icons8-facebook.svg').default;

export const IconRating = iconRating;
export const ImageLogo = imageLogo;
export const ImgGoogle = imgGoogle;
export const ImgFacebook = imgFacebook;

export default {
    imageAppstore,
    googlePlay,
    payment,
    facebook,
    twitter,
    instagram,
    youtube,
    loading,
    banner1,
    banner2,
    banner3,
    banner12,
    banner13,
    iconVN,
    iconUS,
    detailAd,
    thumbnail4,
    thumbnail5,
    thumbnail11,
    thumbnail12,
    loginImg,
    banner11,
    filterWidgetBg,
    headerBackground,
    touchId,
    updatePass,
};