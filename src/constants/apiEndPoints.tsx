import { API_URL, API_URL_AUTH } from '@common/config';
import { GET, POST } from './constants';

/* ==== API OTHER ==== */
export const API_MENU_LIST = {
    key: 'configs/menu-list',
    url: API_URL + 'configs/menu-list',
    method: GET
};
export const API_MENU_FOOTER = './static/home/menuFooter.json';
export const API_HOME_SLIDER = {
    key: 'home-slider',
    url: API_URL + 'home-slider',
    method: GET
};
/* ==== END API OTHER ==== */

/* ==== API PRODUCT ==== */
export const API_PRODUCT_LIST = {
    key: 'products/list',
    url: API_URL + 'products/list',
    method: GET
};
export const API_PRODUCT_LIST_POPULAR = {
    key: 'products/popular',
    url: API_URL + 'products/popular',
    method: GET
};
export const API_PRODUCT_LIST_BEST_SELLER = {
    key: 'products/best-seller',
    url: API_URL + 'products/best-seller',
    method: GET
};
export const API_PRODUCT_LIST_SALE_OF_DAY = {
    key: 'products/sale-of-day',
    url: API_URL + 'products/sale-of-day',
    method: GET
};
export const API_PRODUCT_DETAIL = {
    key: 'products/detail',
    url: API_URL + 'products/detail',
    method: GET
};
/* ==== END API PRODUCT ==== */

/* ==== API CATEGORY ==== */
export const API_CATEGORY_LIST = {
    key: 'categories/featured-categories',
    url: API_URL + 'categories/featured-categories',
    method: GET
};
export const API_CATEGORY_FEATURED = {
    key: 'categories/featured-categories',
    url: API_URL + 'categories/featured-categories',
    method: GET
};
export const API_CATEGORY_DETAIL = {
    key: 'categories/detail',
    url: API_URL + 'categories/detail',
    method: GET
};
/* ==== END API CATEGORY ==== */

/* ==== API ACCOUNTS ==== */
export const API_REGISTER_ACCOUNT = {
    key: 'register',
    url: API_URL + 'register',
    method: POST
};
export const API_LOGIN_ACCOUNT = {
    key: 'login',
    url: API_URL + 'login'
};
export const API_REFRESH_TOKEN = {
    key: 'refresh-token',
    url: API_URL + 'refresh-token',
    method: POST
};
/* ==== END API ACCOUNT ==== */

/* ==== API WISHLIST ==== */
export const API_WISHLIST_DETAIL = {
    key: 'wishlist/detail',
    url: API_URL_AUTH + 'wishlist/detail',
    method: GET
};
export const API_WISHLIST_ADD = {
    key: 'wishlist/add',
    url: API_URL_AUTH + 'wishlist/add',
    method: POST
};
export const API_WISHLIST_DELETE = {
    key: 'wishlist/delete',
    url: API_URL_AUTH + 'wishlist/delete',
    method: POST
};
/* ===== END API WISHLIST ===== */