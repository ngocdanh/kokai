import axiosServices from '@common/axiosServices';
import { API_REGISTER_ACCOUNT } from '@constants/apiEndPoints';
import { POST } from '@constants/index';

export const fetchCreateNewAccountAPI = (dataSubmit: any) => {
    return axiosServices({
        method: POST,
        url: API_REGISTER_ACCOUNT.url,
        body: dataSubmit,
    });
};
