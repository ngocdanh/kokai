import axiosServices from '@common/axiosServices';
import { API_WISHLIST_ADD, API_WISHLIST_DELETE } from '@constants/apiEndPoints';

export const fetchAddWishlistAPI = (dataSubmit: any) => {
    return axiosServices({
        method: 'post',
        url: API_WISHLIST_ADD.url,
        body: dataSubmit,
    });
};
export const fetchDeleteWishlistAPI = (dataSubmit: any) => {
    return axiosServices({
        method: 'post',
        url: API_WISHLIST_DELETE.url,
        body: dataSubmit,
    });
};
