import axiosServices from '@common/axiosServices';
import { API_LOGIN_ACCOUNT } from '@constants/apiEndPoints';
import { POST } from '@constants/index';

export const fetchLoginAccountAPI = (dataSubmit: any) => {
    return axiosServices({
        method: POST,
        url: API_LOGIN_ACCOUNT.url,
        body: dataSubmit,
    });
};
