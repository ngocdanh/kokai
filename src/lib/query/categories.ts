import axiosServices from '@common/axiosServices';
import { API_CATEGORY_DETAIL, API_CATEGORY_FEATURED } from '@constants/apiEndPoints';
import { GET } from '@constants/index';
import { get } from 'lodash';
import { useInfiniteQuery } from 'react-query';

export const useQueryListCategories = (options: any = {}) => {
    const {key, url} = API_CATEGORY_FEATURED;
    const {
        data,
        isLoading,
        error,
        fetchNextPage,
        hasNextPage,
        isFetching,
        isFetchingNextPage,
    } = useInfiniteQuery([key, options], () => axiosServices({method: GET, url})); 
    
    function handleLoadMore() {
        fetchNextPage();
    }

    return (
        {
            listCategories: data?.pages?.flatMap(o => o?.items) ?? [],
            paginator: data?.pages?.flatMap(o => o?.paginator) ?? {},
            isLoading,
            error,
            isFetching,
            isLoadingMore: isFetchingNextPage,
            loadMore: handleLoadMore,
            hasMore: Boolean(hasNextPage),
        }
    );
};

export const useQueryCategoryDetail: any = (params: {}, options: any = {}) => {
    const {key, url} = API_CATEGORY_DETAIL;

    const {
        data,
        isLoading,
        error,
        fetchNextPage,
        hasNextPage,
        isFetching,
        isFetchingNextPage,

    } = useInfiniteQuery([key, options], () => axiosServices({method: GET, url, params: params})); 

    function handleLoadMore() {
        fetchNextPage();
    }
    return (
        {
            categoryDetail: get(data, ['pages', '0'], {}),
            isLoading,
            error,
            isFetching,
            isLoadingMore: isFetchingNextPage,
            loadMore: handleLoadMore,
            hasMore: Boolean(hasNextPage),
        }
    );
};


