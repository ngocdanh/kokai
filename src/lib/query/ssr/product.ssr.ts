import axiosServices from '@common/axiosServices';
import { isDev } from '@common/config';
import { API_PRODUCT_DETAIL, API_PRODUCT_LIST } from '@constants/apiEndPoints';
import { GET } from '@constants/index';
import { GetStaticPaths, GetStaticProps } from 'next';
import { serverSideTranslations } from 'next-i18next/serverSideTranslations';
import { dehydrate, QueryClient } from 'react-query';

export const getStaticPaths: GetStaticPaths = async ({locales}) => {
    if(isDev) { 
        return ({
            paths: [],
            fallback: 'blocking'
        });
    } else {
        const {key, url} = API_PRODUCT_LIST;
        const data = await axiosServices({method: GET, url, params: {limit: 1000}}); 
        const paths = data?.items?.flatMap((product: any) =>
            locales?.map((locale: any) => ({ params: { slug: product?.slug }, locale }))
        );

        return ({
            paths: paths,
            fallback: 'blocking'
        });
    }
};

export const getStaticProps: GetStaticProps = async({locale, params}) => {
    const {slug} = params!;
    
    const queryClient = new QueryClient();

    /* query product detail */
    await queryClient.prefetchQuery([API_PRODUCT_DETAIL.key], (() => 
        axiosServices({method: GET, url: API_PRODUCT_DETAIL.url, params: {slug}})));

    return {
        props: {
            ...(await serverSideTranslations(locale!, ['common'])),
            dehydratedState: JSON.parse(JSON.stringify(dehydrate(queryClient)))
        }
    };
};