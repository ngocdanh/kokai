import axiosServices from '@common/axiosServices';
import { API_MENU_LIST } from '@constants/apiEndPoints';
import { GET } from '@constants/index';
import { GetStaticProps } from 'next';
import { serverSideTranslations } from 'next-i18next/serverSideTranslations';
import { dehydrate, QueryClient } from 'react-query';


export const getStaticProps: GetStaticProps = async({locale, params}) => {
    const queryClient = new QueryClient();

    /* query list menu */
    await queryClient.prefetchQuery([API_MENU_LIST.key], (() => 
        axiosServices({method: GET, url: API_MENU_LIST.url})));
  
    return {
        props: {
            ...(await serverSideTranslations(locale!, ['common'])),
            dehydratedState: JSON.parse(JSON.stringify(dehydrate(queryClient)))
        }
    };
};