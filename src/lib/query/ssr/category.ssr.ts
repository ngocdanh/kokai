import axiosServices from '@common/axiosServices';
import { isDev } from '@common/config';
import { API_CATEGORY_DETAIL, API_CATEGORY_LIST } from '@constants/apiEndPoints';
import { GET } from '@constants/index';
import { GetStaticPaths, GetStaticProps } from 'next';
import { serverSideTranslations } from 'next-i18next/serverSideTranslations';
import { dehydrate, QueryClient } from 'react-query';

export const getStaticPaths: GetStaticPaths = async ({locales}) => {
    if(isDev) {
        return ({
            paths: [],
            fallback: 'blocking'
        });
    } else {
        const {key, url} = API_CATEGORY_LIST;
        const data = await axiosServices({method: GET, url, params: {limit: 1000}}); 
        const paths = data?.items?.flatMap((category: any) =>
            locales?.map((locale: any) => ({ params: { slug: category?.slug }, locale }))
        );

        return ({
            paths,
            fallback: 'blocking'
        });
    }
};


export const getStaticProps: GetStaticProps = async({locale, params}) => {
    const {slug} = params!;

    const queryClient = new QueryClient();

    await queryClient.prefetchQuery([API_CATEGORY_DETAIL.key], (() => 
        axiosServices({method: GET, url: API_CATEGORY_DETAIL.url, params: {slug}})));

    return {
        props: {
            ...(await serverSideTranslations(locale!, ['common'])),
            dehydratedState: JSON.parse(JSON.stringify(dehydrate(queryClient)))
        }
    };
};