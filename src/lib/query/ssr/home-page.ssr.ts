import axiosServices from '@common/axiosServices';
import { isDev } from '@common/config';
import { API_CATEGORY_FEATURED, API_HOME_SLIDER, API_PRODUCT_LIST, API_PRODUCT_LIST_BEST_SELLER, API_PRODUCT_LIST_POPULAR, API_PRODUCT_LIST_SALE_OF_DAY } from '@constants/apiEndPoints';
import { GET } from '@constants/index';
import { GetStaticPaths, GetStaticProps } from 'next';
import { serverSideTranslations } from 'next-i18next/serverSideTranslations';
import { QueryClient, dehydrate } from 'react-query';
type ParsedQueryParams = {
    pages: string[];
};

export const getStaticPaths: GetStaticPaths<ParsedQueryParams> = async ({
    locales,
}) => {
    if(isDev) { 
        return ({
            paths: [],
            fallback: 'blocking'
        });
    } else {
        const paths: any[] = [];
        return {
            paths: paths.concat(
                locales?.map((locale) => ({ params: { pages: [] }, locale }))
            ),
            fallback: 'blocking',
        };
    }
};

export const getStaticProps: GetStaticProps = async({locale, params}) => {
    const queryClient = new QueryClient();

    /* query list product */
    await queryClient.prefetchQuery([API_PRODUCT_LIST.key], (() => 
        axiosServices({method: GET, url: API_PRODUCT_LIST.url})));

    /* query list product popular*/
    await queryClient.prefetchQuery([API_PRODUCT_LIST_POPULAR.key], (() => 
        axiosServices({method: GET, url: API_PRODUCT_LIST_POPULAR.url})));

    /* query list product best seller*/
    await queryClient.prefetchQuery([API_PRODUCT_LIST_BEST_SELLER.key], (() => 
        axiosServices({method: GET, url: API_PRODUCT_LIST_BEST_SELLER.url})));
  
    /* query list categories */
    await queryClient.prefetchQuery([API_CATEGORY_FEATURED.key], (() => 
        axiosServices({method: GET, url: API_CATEGORY_FEATURED.url})));

    /* query list Sale of day */
    await queryClient.prefetchQuery([API_PRODUCT_LIST_SALE_OF_DAY.key], (() =>
        axiosServices({method: GET, url: API_PRODUCT_LIST_SALE_OF_DAY.url})));
   
    /* query list top rated */
    await queryClient.prefetchQuery([API_PRODUCT_LIST.key + 'vote'], (() => 
        axiosServices({method: GET, url: API_PRODUCT_LIST.url, params: {sortKey:'vote'}})));

    /* Query list product sale */
    await queryClient.prefetchQuery([API_PRODUCT_LIST.key + 'discount'], (() => 
        axiosServices({method: GET, url: API_PRODUCT_LIST.url, params: {sortKey:'discount'}})));

    /* query list lasted */
    await queryClient.prefetchQuery([API_PRODUCT_LIST.key + 'created_at'], (() => 
        axiosServices({method: GET, url: API_PRODUCT_LIST.url, params: {sortKey:'created_at'}})));

    /* query list top selling */
    await queryClient.prefetchQuery([API_PRODUCT_LIST.key + 'purchase'], (() => 
        axiosServices({method: GET, url: API_PRODUCT_LIST.url, params: {sortKey:'purchase'}})));

    /* query list top view */
    await queryClient.prefetchQuery([API_PRODUCT_LIST.key + 'view'], (() => 
        axiosServices({method: GET, url: API_PRODUCT_LIST.url, params: {sortKey:'view'}})));
    
    /* query list slider banner */
    await queryClient.prefetchQuery([API_HOME_SLIDER.key], (() => 
        axiosServices({method: GET, url: API_HOME_SLIDER.url})));
    
    return {
        props: {
            ...(await serverSideTranslations(locale!, ['common'])),
            dehydratedState: JSON.parse(JSON.stringify(dehydrate(queryClient)))
        }
    };
};