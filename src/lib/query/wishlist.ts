import axiosServices from '@common/axiosServices';
import { API_WISHLIST_DETAIL } from '@constants/apiEndPoints';
import { GET } from '@constants/index';
import { useQuery } from 'react-query';
import queryString from 'query-string';
import { useDispatch, useSelector } from 'react-redux';
import React from 'react';
import { hideLoading } from '@redux/actions/global.action';

export const useQueryWishlist: any = (params = {}, options: any = {}) => {
    const {key, url} = API_WISHLIST_DETAIL;
    const {
        data,
        isLoading,
        error,
        isFetching,
        refetch,
    } = useQuery(
        [key + queryString.stringify(params), options], 
        () => axiosServices({method: GET, url, params}),
    ); 

    const isLoadingGlobal = useSelector((state: any) => state.global.isLoading);
    const dispatch = useDispatch();
    React.useEffect(() => {
        if(!isLoading && isLoadingGlobal) {
            dispatch(hideLoading());
        }
    }, [isLoading, isLoadingGlobal]);


    return (
        {
            refetch,
            wishlist: data ?? [],
            listProducts: data?.products ?? [],
            isLoading,
            error,
            isFetching,
        }
    );
};
