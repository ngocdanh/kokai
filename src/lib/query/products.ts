import axiosServices from '@common/axiosServices';
import { API_PRODUCT_DETAIL, API_PRODUCT_LIST, API_PRODUCT_LIST_BEST_SELLER, API_PRODUCT_LIST_POPULAR, API_PRODUCT_LIST_SALE_OF_DAY } from '@constants/apiEndPoints';
import { GET } from '@constants/index';
import { hideLoading } from '@redux/actions/global.action';
import queryString from 'query-string';
import * as React from 'react';
import { useQuery } from 'react-query';
import { useDispatch, useSelector } from 'react-redux';

export const useQueryListProduct: any = (params: {}, options: any = {}) => {
    const {key, url} = API_PRODUCT_LIST;
    const {
        data,
        isLoading,
        error,
        isFetching,

    } = useQuery(
        [key + queryString.stringify(params), options], 
        () => axiosServices({method: GET, url, params: params}),
    ); 
    const isLoadingGlobal = useSelector((state: any) => state.global.isLoading);
    const dispatch = useDispatch();
    React.useEffect(() => {
        if(!isLoading && isLoadingGlobal) {
            dispatch(hideLoading());
        }
    }, [isLoading, isLoadingGlobal]);

    return (
        {
            list: data?.items ?? [],
            paginator: data?.paginator ?? {},
            isLoading,
            error,
            isFetching,
        }
    );
};

export const useQueryListProductPopular: any = (params = {}, options: any = {}) => {
    const {key, url} = API_PRODUCT_LIST_POPULAR;
    const {
        data,
        isLoading,
        error,
        isFetching,

    } = useQuery(
        [key + queryString.stringify(params), options], 
        () => axiosServices({method: GET, url, params: params}),
    ); 

    const isLoadingGlobal = useSelector((state: any) => state.global.isLoading);
    const dispatch = useDispatch();
    React.useEffect(() => {
        if(!isLoading && isLoadingGlobal) {
            dispatch(hideLoading());
        }
    }, [isLoading, isLoadingGlobal]);

    return (
        {
            listProductPopular: data?.items ?? [],
            paginator: data?.paginator ?? {},
            isLoading,
            error,
            isFetching,
        }
    );
};

export const useQueryListProductBestSeller: any = (options: any = {}, params = {}) => {
    const {key, url} = API_PRODUCT_LIST_BEST_SELLER;
    const {
        data,
        isLoading,
        error,
        isFetching,

    } = useQuery(
        [key + queryString.stringify(params), options], 
        () => axiosServices({method: GET, url, params: params}),
    ); 

    const isLoadingGlobal = useSelector((state: any) => state.global.isLoading);
    const dispatch = useDispatch();
    React.useEffect(() => {
        if(!isLoading && isLoadingGlobal) {
            dispatch(hideLoading());
        }
    }, [isLoading, isLoadingGlobal]);

    return (
        {
            listProductBestSeller: data?.items ?? [],
            paginator: data?.paginator ?? {},
            isLoading,
            error,
            isFetching,
        }
    );
};

export const useQueryListProductSaleOfDay: any = (options: any = {}, params = {}) => {
    const {key, url} = API_PRODUCT_LIST_SALE_OF_DAY;
    const {
        data,
        isLoading,
        error,
        isFetching,

    } = useQuery(
        [key + queryString.stringify(params), options], 
        () => axiosServices({method: GET, url, params: params}),
    ); 

    const isLoadingGlobal = useSelector((state: any) => state.global.isLoading);
    const dispatch = useDispatch();
    React.useEffect(() => {
        if(!isLoading && isLoadingGlobal) {
            dispatch(hideLoading());
        }
    }, [isLoading, isLoadingGlobal]);

    return (
        {
            listProductSaleOfDay: data?.items ?? [],
            paginator: data?.paginator ?? {},
            isLoading,
            error,
            isFetching,
        }
    );
};

export const useQueryProductDetail: any = (params = {}, options: any = {}) => {
    const {key, url} = API_PRODUCT_DETAIL;
    const {
        data,
        isLoading,
        error,
        isFetching,

    } = useQuery(
        [key + queryString.stringify(params), options], 
        () => axiosServices({method: GET, url, params: params}),
    ); 

    const isLoadingGlobal = useSelector((state: any) => state.global.isLoading);
    const dispatch = useDispatch();
    React.useEffect(() => {
        if(!isLoading && isLoadingGlobal) {
            dispatch(hideLoading());
        }
    }, [isLoading]);

    return (
        {
            product: data ?? {},
            paginator: data?.paginator ?? {},
            isLoading,
            error,
            isFetching,
        }
    );
};





