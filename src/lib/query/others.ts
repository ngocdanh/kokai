import axiosServices from '@common/axiosServices';
import { API_HOME_SLIDER } from '@constants/apiEndPoints';
import { GET } from '@constants/index';
import { useQuery } from 'react-query';
import queryString from 'query-string';
import { hideLoading } from '@redux/actions/global.action';
import { useDispatch, useSelector } from 'react-redux';
import React from 'react';

export const useQueryListSliderHome: any = (params: any = {}, options: any = {}) => {
    const {key, url} = API_HOME_SLIDER;
    const {
        data,
        isLoading,
        error,
        isFetching,

    } = useQuery(
        [key + queryString.stringify(params), options], 
        () => axiosServices({method: GET, url, params: params}),
    ); 
    const isLoadingGlobal = useSelector((state: any) => state.global.isLoading);
    const dispatch = useDispatch();
    React.useEffect(() => {
        if(!isLoading && isLoadingGlobal) {
            dispatch(hideLoading());
        }
    }, [isLoading, isLoadingGlobal]);


    return (
        {
            list: data?.items ?? [],
            paginator: data?.paginator ?? {},
            isLoading,
            error,
            isFetching,
        }
    );
};


