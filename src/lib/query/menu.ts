import axiosServices from '@common/axiosServices';
import { API_MENU_LIST } from '@constants/apiEndPoints';
import { GET } from '@constants/index';
import { hideLoading } from '@redux/actions/global.action';
import { flatten } from 'lodash';
import React from 'react';
import { useInfiniteQuery } from 'react-query';
import { useDispatch, useSelector } from 'react-redux';

export const useQueryMenu = (options: any = {}) => {
    const {key, url} = API_MENU_LIST;
    const {
        data,
        isLoading,
        error,
        fetchNextPage,
        hasNextPage,
        isFetching,
        isFetchingNextPage,
    } = useInfiniteQuery([key, options], () => axiosServices({method: GET, url})); 
  
    function handleLoadMore() {
        fetchNextPage();
    }

    const isLoadingGlobal = useSelector((state: any) => state.global.isLoading);
    const dispatch = useDispatch();
    React.useEffect(() => {
        if(!isLoading && isLoadingGlobal) {
            dispatch(hideLoading());
        }
    }, [isLoading, isLoadingGlobal]);


    return (
        {
            listMenu: flatten(data?.pages) ?? [],
            isLoading,
            error,
            isFetching,
            isLoadingMore: isFetchingNextPage,
            loadMore: handleLoadMore,
            hasMore: Boolean(hasNextPage),
        }
    );
};
