import axios from 'axios';
import { GET } from '@constants/index';
import { debounce, isEmpty } from 'lodash';
import { useEffect, useMemo, useState } from 'react';
import queryString from 'query-string';

// axios.defaults.baseURL = API_URL;
type Methods = 'head' | 'options' | 'put' | 'post' | 'patch' | 'delete' | 'get';
interface IAxios {
    url: string, 
    method?: Methods, 
    body?: any, 
    headers?: any, 
    delay?: number, 
    query?: object
}
const useAxios = (props: IAxios) => {
    const { url = '', method = GET, body = null, headers = null, delay = 0, query = {}} = props;
    const [response, setResponse] = useState(null);
    const [error, setError] = useState('');
    const [loading, setLoading] = useState(true);
    let uri = url;
    
    if(method === GET && !isEmpty(query)) {
        uri = `${url}?${queryString.stringify(query)}`;
    }
    const fetchData = useMemo(() => 
        debounce(() => {
            axios[method](uri, JSON.parse(headers), JSON.parse(body))
                .then((res: any) => {
                    setResponse(res.data);
                })
                .catch((err: any) => {
                    setError(err);
                })
                .finally(() => {
                    setLoading(false);
                });
        }, delay),
    []
    ); 

    useEffect(() => {
        fetchData();
    }, [method, url, body, headers]);

    return { response, error, loading };
};

export default useAxios;