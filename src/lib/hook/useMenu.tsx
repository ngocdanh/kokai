import { MouseEvent, useState } from 'react';

const useMenu = () => {
    const [open, setOpen] = useState<null | HTMLElement>(null);

    const openMenu = (event: MouseEvent<HTMLElement>) => {
        setOpen(event.currentTarget);
    };
    const closeMenu = () => {
        setOpen(null);
    };
    return ({open, openMenu, closeMenu});
};

export default useMenu;