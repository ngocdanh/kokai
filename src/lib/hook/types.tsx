import { MouseEvent } from 'react';

export interface IUserMenu {
    openMenu?: (event: MouseEvent<HTMLElement>) => void,
    onClose: () => void,
    open: null | HTMLElement
}