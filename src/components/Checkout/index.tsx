import { Box } from '@mui/material';
import { WithStyles, withStyles } from '@mui/styles';
import { useTranslation } from 'next-i18next';
import styles from './styles';
import { IProps } from './types';


const CartComponent = (props: IProps & WithStyles<typeof styles>) => {
    const {
        classes,
    } = props;

    const {t} = useTranslation();
 
    return (
        <Box className={classes.root}>
            Hello cart
        </Box>
    );
};

export default withStyles(styles)(CartComponent);