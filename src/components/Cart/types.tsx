import { typeCartItem } from '@common/types';
import { IBreadcrumbItem } from '@helpers/BreadcrumbCustom/types';

export interface IProps {
    breadcrumb: IBreadcrumbItem[];
    cartData: Array<typeCartItem>;
    total: number;
    // totalAmount: number;
    handleCoupon: ( coupon?: string) => void;
    handleDelete: ( id?: string ) => void;
    QuantityHasChange: (id: string, quantity: number) => void;
}

export interface ICartItem {
    CartItemData: typeCartItem | any;
    handleDelete: ( id: string) => void;
    QuantityHasChange: (id: string, quantity: number) => void;
}