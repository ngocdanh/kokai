import { Box, Paper, Table, TableBody, TableCell, TableContainer, TableHead, TableRow } from '@mui/material';
import TextCustom from '@helpers/TextCustom';
import { WithStyles, withStyles } from '@mui/styles';
import { useTranslation } from 'next-i18next';
import styles from './styles';
import { IProps } from './types';
import BreadcrumbCustom from '@helpers/BreadcrumbCustom';
import { COLORS } from '@constants/colors';
import { isEmpty, uniqueId } from 'lodash';
import CartItem from './CartItem';
import { typeCartItem } from '@common/types';
import { useEffect, useState } from 'react';
import ButtonCustom from '@helpers/ButtonCustom';
import { routeRoot } from '@common/config';
import Link from 'next/link';


const CartComponent = (props: IProps & WithStyles<typeof styles>) => {
    const {
        classes,
        breadcrumb,
        cartData,
        total,
        handleCoupon,
        handleDelete,
        QuantityHasChange,
    } = props;

    const {t} = useTranslation();

    const [totalAmount, setTotalAmount] = useState (cartData?.reduce((sum , item) => {
        return sum + item.price;
    }, 0));

    useEffect(() => {
        const newTotal = cartData?.reduce((sum, item) => {
            return sum + (item.price * item.cart_quantity);
        }, 0);
        setTotalAmount(newTotal);
    }, [cartData]);
 
    return (
        <Box className={classes.root}>
            <BreadcrumbCustom list={breadcrumb}/>
            <Box className='flex flex-col'>
                <TextCustom h2
                    color={COLORS.textHeading}>
                    {t('yourCart')}
                </TextCustom>
                {total?
                    <Box className='flex justify-between items-center'>
                        <TextCustom h6
                            className='!mt-[20.4px]'
                            color={COLORS.textBody}
                            dangerouslySetInnerHTML={{__html: t('totalProductWishList', {
                                total: total
                            })}}>
                        </TextCustom>
                        <Box className='flex items-center gap-3 cursor-pointer !mr-[32%]'
                            onClick={() => handleDelete()} >
                            <i className="fi fi-rs-trash" 
                                color={COLORS.textMuted}></i>
                            <TextCustom h6 
                                color={COLORS.textMuted}>
                                {t('clearCart')}
                            </TextCustom>
                            
                        </Box>
                    </Box>
                    :
                    null
                }
                <Box className="flex gap-8 flex-wrap">
                    {total?
                        <TableContainer component={Paper}
                            elevation={0}
                            className={'table-container flex flex-col'}>
                            <Table sx={{ minWidth: 650 }}
                                aria-label="wish-list-table">
                                <TableHead
                                    className={'header-table'}>
                                    <TableRow className='header-row'>
                                        <TableCell align='center'>
                                            <TextCustom h6
                                                color={COLORS.textHeading2}>{t('products')}</TextCustom>
                                        </TableCell>
                                        <TableCell align="center">
                                            <TextCustom h6
                                                color={COLORS.textHeading2}>{t('unitPrice')}</TextCustom>
                                        </TableCell>
                                        <TableCell align="center"><TextCustom h6
                                            color={COLORS.textHeading2}>{t('quantity')}</TextCustom></TableCell>
                                        <TableCell align="center"><TextCustom h6
                                            color={COLORS.textHeading2}>{t('subtotal')}</TextCustom></TableCell>
                                        <TableCell align="center"><TextCustom h6
                                            color={COLORS.textHeading2}>{t('remove')}</TextCustom></TableCell>
                                    </TableRow>
                                </TableHead>
                                <TableBody>
                                    {
                                        !isEmpty(cartData) &&  cartData?.map((item: typeCartItem) => (
                                            <CartItem CartItemData={item}
                                                handleDelete={handleDelete}
                                                QuantityHasChange={QuantityHasChange}
                                                key={uniqueId()}/>
                                        ))
                                    }
                                </TableBody>
                            </Table>
                            <Box className="border-line-table"></Box>
                            <Link href={routeRoot.home}>
                                <ButtonCustom className='btn-shopping'>
                                    <TextCustom h6 
                                        className='!flex items-center'>
                                        <i className="fi fi-rr-arrow-left !text-white !mr-3 !mt-1"></i>
                                        {t('continueShopping')}
                                    </TextCustom>
                                </ButtonCustom>
                            </Link>
                        </TableContainer>
                        
                        :
                        <TextCustom h3
                            className={'!pt-4 !pb-4 pl-10'}
                            color={COLORS.black}>
                            {t('noProducts')}
                        </TextCustom>
                    }
                    <Box className={classes.Coupon}>
                        <Box className="coupon-heading flex flex-col">
                            <TextCustom h4>
                                {t('applyCoupon')}
                            </TextCustom>
                            <TextCustom textMedium
                                color={COLORS.textBody}>
                                {t('usingPromoCode')}
                            </TextCustom>
                        </Box>
                        <Box component={'form'} 
                            className="box-input flex justify-between items-center overflow-hidden gap-2">
                            <Box className='!flex  items-center'>
                                <i className="fi fi-rr-ticket !text-[16px] !mr-2 !mt-1"></i>
                                <input type="text" 
                                    className='!text-[14px] w-100' 
                                    placeholder={t('couponCode')} />
                            </Box>
                           
                            <ButtonCustom borderRadius='initial' 
                                className='btn-apply'>
                                <TextCustom h6>
                                    {t('apply')}
                                </TextCustom>
                            </ButtonCustom>
                        </Box>
                        <Box className="total-amount">
                            <Box className="subtotal text-line">
                                <TextCustom h6 
                                    color={COLORS.textMuted}>
                                    {t('subtotal')}
                                </TextCustom>
                                <TextCustom h4 
                                    color={COLORS.brand1}>
                                    {`$${totalAmount}`}
                                </TextCustom>
                            </Box>
                            <Box className="border-line"></Box>
                            <Box className="shipping text-line">
                                <TextCustom h6
                                    color={COLORS.textMuted}>
                                    {t('shipping')}
                                </TextCustom>
                                <TextCustom h5
                                    color={COLORS.textHeading}>
                                        FreeShipping
                                </TextCustom>
                            </Box>
                            <Box className="shipping text-line">
                                <TextCustom h6
                                    color={COLORS.textMuted}>
                                    {t('estimateFor')}
                                </TextCustom>
                                <TextCustom h5
                                    color={COLORS.textHeading}>
                                        Ha Noi, Viet Nam
                                </TextCustom>
                            </Box>
                            <Box className="border-line"></Box>
                            <Box className="total text-line">
                                <TextCustom h6 
                                    color={COLORS.textMuted}>
                                    {t('total')}
                                </TextCustom>
                                <TextCustom h4 
                                    color={COLORS.brand1}>
                                    {`$${totalAmount}`}
                                </TextCustom>
                            </Box>
                            <ButtonCustom className='btn-checkout'>
                                <TextCustom h6
                                    className='!flex items-center'>
                                    {t('proceedToCheckout')}
                                    <i className='fi fi-rr-ticket !text-white !ml-2 !mt-1' 
                                        color={COLORS.white}></i>
                                </TextCustom>
                            </ButtonCustom>
                        </Box>
                    </Box>
                </Box>
            </Box>
        </Box>
    );
};

export default withStyles(styles)(CartComponent);