import { COLORS } from '@constants/colors';
import { Theme } from '@mui/material';
import { createStyles } from '@mui/styles';
import { } from '@mui/styles/';

const styles = (theme: Theme) => createStyles({
    root: {

        '& .header-table': {
            backgroundColor: '#F5F5F6',
        },
        '& .MuiTableRow-root th:first-child': {
            borderRadius: '15px 0 0 15px',
        },
          
        '& .MuiTableRow-root th:last-child': {
            borderRadius: '0 15px 15px 0',
        },

        '& .MuiTableCell-root': {
            border: 0,
        },

        '& .table-container': {
            display: 'flex',
            margin : '40px auto',
            width: '68%',
        },


        '& .product-quantity': {
            color: COLORS.brand1
        },

        '& i': {
            color: COLORS.textMuted,
        },

        '& .border-line-table': {
            marginTop: '2rem',
            width: '100%',
            height: '1.5px',
            backgroundColor: COLORS.borderColor
        },

        '& .btn-shopping': {
            width: 'fit-content',
            padding: '15px 28px',
            marginTop: '2.4rem',
            backgroundColor: `${COLORS.warring} !important`
        }
       
    },

    CartItem: {

        '& .delete': {
            fontSize: 20,
            cursor: 'pointer',
            color: COLORS.textMuted

        },

        '& .in-stock': {
            fontWeight: 600,
            color: COLORS.brand1,
            backgroundColor: '#DEF9EC',
            padding: '4px 10px',
            borderRadius: 5,
        },

        '& .out-stock': {
            fontWeight: 600,
            color: COLORS.white,
            backgroundColor: COLORS.textHeading2,
            padding: '4px 10px',
            borderRadius: 5,
        },

        '& .btn-card': {
            borderRadius: 4,
            backgroundColor: `${COLORS.brand1}`,
            padding: '15px 22px',
            color: COLORS.white,
            fontSize: 16,
            fontWeight: 600,
            lineHeight: '20px',
            boxShadow: 'none',
            '&:hover': {
                backgroundColor: `${COLORS.brand2}`,
            }
        },

        '& .input-quantity': {
            display: 'flex',
            justifyContent: 'space-between',
            width: 120,
            alignItems: 'center',
            border: `2px solid ${COLORS.borderColor}`,
            color: COLORS.textBody,
            fontSize: 20,
            borderRadius: 5,
            padding: '0px 15px 0px 20px',
            fontWeight: 700,
            '& input': {
                width: '100%',
            },
            '& .up-down': {
                display: 'flex',
                flexDirection: 'column',
                fontSize: 14,
                gap: 8,
            },


        },

        '&:hover': {
            '& .input-quantity':{
                border: `2px solid ${COLORS.brand1}`,
                color: COLORS.brand1,
                '& input': {
                    backgroundColor: '#F5F5F6 !important',
                },

                
            }
        }

    },

    NameItem: {
        display: 'inline-flex',
        alignItems: 'center',
        padding: 0,
        overflow: 'hidden',
        '& .image': {
            display: 'flex',
            justifyContent: 'center',
            width: 100,  
            height: 100,
            '& img': {
                width: '100%',
                height: '100%',
                objectFit: 'contain',
                transition: 'all .3s ease',
            },
      
        },
        '& .content': {
            width: 'fit-content',
            '& .title': {
                transition: 'all .3s ease',
                '&:hover': {
                    color: `${COLORS.brand1} !important`,
                }
            },
            '& .rating': {
                '& i': {
                    color: '#CDCDCD',
                },
                '& .active': {
                    color: COLORS.warring,
                },
                '& .not-active': {
                    color: '#CDCDCD',
                }
            },
        },
    },


    Coupon: {
        width: '28%',
        minWidth: '330px',
        margin: '40px auto',
        display: 'flex',
        flexDirection: 'column',
        gap: '2rem',
        

        '& .box-input': {
            border: `1px solid ${COLORS.borderColor}`,
            borderRadius: '10px 10px',
            paddingLeft: '1.5rem',
            boxShadow: '5px 5px 15px rgba(24, 24, 24, 0.05)',
        },
        '& .btn-apply': {
            padding: '0 2rem',
            height: '42px',
            lineHeight: '1rem'

        },
        '& .total-amount': {
            display: 'flex',
            gap: '1.2rem',
            flexDirection: 'column',
            padding: '2rem 1.8rem',
            border: `1px solid ${COLORS.borderColor}`,
            borderRadius: '15px',
            boxShadow: '5px 5px 15px rgba(24, 24, 24, 0.05)',

            '& .text-line': {
                display: 'flex',
                justifyContent: 'space-between',
                alignItems: 'center',
            },

            '& .border-line': {
                width: '100%',
                height: '1.5px',
                backgroundColor: COLORS.borderColor
            },

            '& .btn-checkout': {
                height: '3rem',


            }
        }

    },

    Modal: {

        position: 'absolute',
        top: '50%',
        left: '50%',
        transform: 'translate(-50%, -50%)',
        width: 'fit-content',
        backgroundColor: COLORS.white,
        borderRadius: '12px',
        padding: '15px 25px',

        '& .confirm-delete': {
            marginTop: '10px',
            float: 'right',

            '& button': {
                height: 'fit-content'
            }
        }

    }
});

export default styles;