import { routeRoot } from '@common/config';
import { convertGroupPrice } from '@common/convertData';
import { COLORS } from '@constants/colors';
import TextCustom from '@helpers/TextCustom';
import { Box, TableCell, TableRow, Modal, Typography } from '@mui/material';
import { WithStyles, withStyles } from '@mui/styles';
import { isEmpty } from 'lodash';
import { useTranslation } from 'next-i18next';
import styles from './styles';
import { ICartItem } from './types';
import 'react-toastify/dist/ReactToastify.css';
import Link from 'next/link';
import { useState, useEffect } from 'react';
import ButtonCustom from '@helpers/ButtonCustom';

const CartItem = (props: ICartItem & WithStyles<typeof styles>) => {
    const {
        classes,
        CartItemData,
        handleDelete,
        QuantityHasChange
    } = props;

    const itemQuantity = Number(CartItemData?.quantity);
    const {t} = useTranslation();
    const { price , sale_price } = convertGroupPrice(CartItemData);


    const [quantity, setQuantity] = useState<number>(CartItemData.cart_quantity);
    const [quantityInput, setQuantityInput] = useState<number>(quantity);
    const [openModal, setOpenModal] = useState<boolean>(false);
    const handleOpenModal = () => setOpenModal(true);
    const handleCloseModal = () => {
        setQuantityInput(CartItemData.cart_quantity);
        setOpenModal(false);
    };

    const handleBlur = (e: React.FormEvent<EventTarget>) => {
        const target = e.target as HTMLInputElement;
        if(Number(target.value) == 0){
            handleOpenModal();
        }else{
            setQuantity(Number(target.value));
        }
        
    };
    
    const subtotal = CartItemData.sale_price*CartItemData.cart_quantity;
    const handleChangeQuantity = (e: React.FormEvent<EventTarget>) => {
        const target = e.target as HTMLInputElement;
        if(Number(target.value)){
            if(Number(target.value) > itemQuantity){
                setQuantityInput(itemQuantity);
            }else{
                setQuantityInput(Number(target.value));
            }
        }else{
            setQuantityInput(0);
        }
        
    };

    const handleIncreaseQuantity = () => {
        if(itemQuantity > quantity) {
            setQuantity(quantity + 1);
        }
    };
    const handleDecreaseQuantity = () => {
        if(quantity !== 1){
            setQuantity(quantity - 1);
        }else {
            handleOpenModal();
        }
    };

    useEffect(() => {
        if(quantity != CartItemData.cart_quantity) {
            QuantityHasChange(CartItemData.id_, quantity);
        }
    }, [quantity]);


    return (
        <>
            <TableRow
                className={classes.CartItem}
                hover={true}
                key={CartItemData.name}
                sx={{ border: 0 }}
            >
                <TableCell>
                    <Box className={ classes.NameItem }>
                        <Box className="image">
                            {!isEmpty(CartItemData?.image) && (
                                <img src={CartItemData?.image.original} />
                            )}
                        </Box>
                        <Box className="content pl-5 pr-2">
                            <Link href={`${routeRoot.product}/${CartItemData?.slug}`}>
                                <a>
                                    <TextCustom headingSM
                                        className='mb-1 mt-1 title'
                                        color={COLORS.textHeading}>{CartItemData?.name}</TextCustom>
                                </a>
                            </Link>
                        </Box>
                    </Box>
                </TableCell>
                <TableCell align="center">
                    <Box className="box-price !flex !items-center justify-center">
                        <TextCustom className='!mr-2'
                            h4
                            color={COLORS.brand1}>{ sale_price || price }</TextCustom>  
                        {CartItemData?.price && !!CartItemData?.sale_price && <TextCustom
                            textXS
                            className='line-through'
                            color={COLORS.textBody}>{`$${CartItemData.price}`}</TextCustom>}

                    </Box>
                </TableCell>
                <TableCell align="center">
                    <Box className='input-quantity'>
                        <input type="text"
                            value={(quantityInput != quantity)? quantityInput: quantity}
                            min={0}
                            max={itemQuantity}
                            onChange={(e) => handleChangeQuantity(e)}
                            onBlur={(e) => handleBlur(e)}
                        >
                        </input>
                        <Box className='up-down'>
                            <i className="fi fi-br-angle-up cursor-pointer"
                                onClick={handleIncreaseQuantity}></i>
                            <i className="fi fi-br-angle-down cursor-pointer"
                                onClick={handleDecreaseQuantity}></i> 
                        </Box>
                                            
                    </Box>
                </TableCell>
                <TableCell align="center">
                    <TextCustom h4 
                        color={COLORS.brand1}>
                        {`$${subtotal}`}
                    </TextCustom>
                </TableCell>
                <TableCell align="center">
                    <i className="fi fi-rr-cross-circle delete"
                        onClick={(e: React.MouseEvent<HTMLElement>) => 
                            handleDelete(CartItemData?.id_)}></i>                
                </TableCell>
            </TableRow>
            <Modal
                open={openModal}
                onClose={handleCloseModal}
                aria-labelledby="modal-modal-title"
                aria-describedby="modal-modal-description"
            >
                <Box className={classes.Modal}>
                    <Typography id="modal-modal-title"
                        variant="h6"
                        component="h2">
                        Xóa sản phẩm
                    </Typography>
                    <Typography id="modal-modal-description"
                        sx={{ mt: 2 }}>
                        Số lượng sản phẩm bằng không bạn có muốn xóa khỏi giỏ hàng?
                    </Typography>
                    <Box className={'confirm-delete flex gap-3'}>
                        <ButtonCustom
                            onClick={() => handleDelete(CartItemData.id_)}>
                            Có
                        </ButtonCustom>
                        <ButtonCustom
                            onClick={handleCloseModal}>
                            Không
                        </ButtonCustom>
                    </Box>
                </Box>
            </Modal>
        </>
    );
};

export default withStyles(styles)(CartItem);