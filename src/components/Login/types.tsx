import { IBreadcrumbItem } from '@helpers/BreadcrumbCustom/types';

export type ILogin = {
    email: string,
    password: string,
}

export interface IProps {
    breadcrumbs: IBreadcrumbItem[],
    loadingSubmit: boolean,
    onSubmit: (data: ILogin) => void,
    onChangeGoogleRecaptcha: (token: string| null) => void,
    isRobot: boolean,
}
