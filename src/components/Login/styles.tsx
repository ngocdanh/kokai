import { COLORS } from '@constants/colors';
import { Theme } from '@mui/material';
import { createStyles } from '@mui/styles';
import { } from '@mui/styles/';

const styles = (theme: Theme) => createStyles({
    root: {
        
    },

    LoginForm: {
        paddingTop: 52,
        paddingBottom: 52,
        gap: 24,
        alignItems: 'center',

        '& img': {
            borderRadius: 15,
        },

        '& .register-main': {
            display: 'flex',
            alignItems: 'flex-start',
            justifyContent: 'space-between  ',

        },
    },

    TextFieldCss: {
        width: '100%',
        display: 'flex',
        gap: 20,
        flexDirection: 'column',
        '& .security-code': {
            display: 'flex',
            gap: 20,
            '& .security-number':{
                height: '100%',
                padding: 20,
                borderRadius: 10,
            }
        },
        '& .terms-policy': {
            display: 'flex',
            justifyContent: 'space-between',
            alignItems: 'center',
        },
        '& .btn-submit': {
            minWidth: 120,
            width: 'fit-content',
            borderRadius: 10,   
            fontWeight: 700,
            lineHeight: 3,
            padding: '10px 35px',
            backgroundColor: '#253D4E',
            color: COLORS.white,
        },
    },
});

export default styles;