import BreadcrumbCustom from '@helpers/BreadcrumbCustom';
import { Button, Grid } from '@mui/material';
import { WithStyles, withStyles } from '@mui/styles';
import { useTranslation } from 'next-i18next';
import React from 'react';
import styles from './styles';
import { ILogin, IProps } from './types';
import imageAssets from '@constants/imageAssets';
import TextCustom from '@helpers/TextCustom';
import { Box } from '@mui/material';
import { COLORS } from '@constants/colors';
import TextFieldCustom from '@helpers/TextFieldCustom';
import { useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import { schemaLogin } from '@constants/Validate';
import { routeRoot } from '@common/config';
import Link from 'next/link';
// @ts-ignore
import ReCAPTCHA from 'react-google-recaptcha';
const googleRecaptchaKey = process.env.GOOGLE_RECAPTCHA_KEY || '';

const LoginComponent = (props: IProps & WithStyles<typeof styles>) => {
    const {
        classes,
        breadcrumbs,
        loadingSubmit,
        onSubmit,
        onChangeGoogleRecaptcha,
        isRobot,
    } = props;

    const {t} = useTranslation();
    const { handleSubmit, formState: { errors }, control } = useForm<ILogin>({
        resolver: yupResolver(schemaLogin)
    });
  
    return (
        <React.Fragment>
            <BreadcrumbCustom list={breadcrumbs}/>
            <Grid container
                justifyContent={'center'}
                spacing={3}
                columns={{ xs: 4, sm: 8, md: 12 }}
                className={classes.LoginForm}>
                <Grid item
                    md={4.5}>
                    <img src={imageAssets.loginImg}
                        alt="login-img" />
                </Grid>
                <Grid item
                    md={4}>
                    <Box className="register-header flex flex-col ">
                        
                        <TextCustom className="title"
                            h1
                            color={' #253D4E'}>
                            {t('login')}
                        </TextCustom>
                        <TextCustom 
                            textSmall
                            color={COLORS.textMuted}
                            className="sub-title !mt-2 !mb-10">
                            {t('createAccountTitle')}
                            <Link 
                                href={routeRoot.register}>
                                <a>
                                    <TextCustom textSmall
                                        className='pl-2 pr-2 !font-bold'
                                        color={COLORS.brand1}>{t('createHere')}</TextCustom>
                                </a>
                            </Link>
                        </TextCustom>
                        
                    </Box>
                    <Box className="register-main">
                        <Box component={'form'}
                            onSubmit={handleSubmit(onSubmit)}
                            className={classes.TextFieldCss}
                            autoComplete="off">
                            <TextFieldCustom control={control}
                                errors={errors.email}
                                name={'email'}
                                label={t('email')}/>
                            <TextFieldCustom control={control}
                                errors={errors.password}
                                name='password' 
                                label={t('password')}
                                type='password'/>
                            <Box className="security-code">
                                <ReCAPTCHA
                                    sitekey={googleRecaptchaKey}
                                    onChange={onChangeGoogleRecaptcha}
                                />
                            </Box>
                            <Box><Button variant="contained"
                                type='submit'
                                className='btn-submit'
                                disabled={isRobot}
                            >
                                { t('submitLogin')}
                            </Button></Box>
                        </Box>
                    </Box>
                </Grid>
            </Grid>
        </React.Fragment>
    );
};

export default withStyles(styles)(LoginComponent);