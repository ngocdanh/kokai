import { COLORS } from '@constants/colors';
import { Theme } from '@mui/material';
import { createStyles } from '@mui/styles';
import { } from '@mui/styles/';

const styles = (theme: Theme) => createStyles({

    root: {

    },

    ResetPassword: {

        margin: '65px auto 150px auto !important',

        '& .reset-pass-title': {
            padding: '26px 0 38px 0'
        },

        '& img': {
            width: '85px',
        },

        '& li': {
            fontWeight: '500',
            fontSize: '15px',
            lineHeight: '26px',
            marginLeft: '17px'
        },

        '& .list-style': {
            marginLeft: '15px'
        }

       
    },

    TextFieldCss: {
        width: '100%',
        display: 'flex',
        gap: 20,
        flexDirection: 'column',
        '& .security-code': {
            display: 'flex',
            gap: 20,
            '& .security-number':{
                height: '100%',
                padding: 20,
                borderRadius: 10,
            }
        },
        '& .terms-policy': {
            display: 'flex',
            justifyContent: 'space-between',
            alignItems: 'center',
        },
        '& .btn-submit': {
            minWidth: 120,
            width: 'fit-content',
            borderRadius: 10,   
            fontWeight: 700,
            lineHeight: 3,
            padding: '10px 35px',
            backgroundColor: '#253D4E',
            color: COLORS.white,
        },
    },
});

export default styles;