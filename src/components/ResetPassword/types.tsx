import { IBreadcrumbItem } from '@helpers/BreadcrumbCustom/types';

export interface IProps {
    breadcrumbs: IBreadcrumbItem[],
    onSubmit: (data: IResetPassword) => void,
    loadingSubmit: boolean,
  
}

export type IResetPassword = {
    password: string,
    confirm_password: string,
}