import { Box, Grid, Button, CircularProgress } from '@mui/material';
import { WithStyles, withStyles } from '@mui/styles';
import { useTranslation } from 'next-i18next';
import styles from './styles';
import { IProps, IResetPassword } from './types';
import BreadcrumbCustom from '@helpers/BreadcrumbCustom';
import TextFieldCustom from '@helpers/TextFieldCustom';
import { yupResolver } from '@hookform/resolvers/yup';
import { useForm } from 'react-hook-form';
import { schemaResetPassword } from '@constants/Validate';
import TextCustom from '@helpers/TextCustom';
import { COLORS } from '@constants/colors';
import { useRef, useEffect, useState} from 'react';
import imageAssets from '@constants/imageAssets';


const ResetPasswordComponent = (props: IProps & WithStyles<typeof styles>) => {
    const {
        classes,
        breadcrumbs,
        onSubmit,
        loadingSubmit
    } = props;


    const [height, setHeight] = useState<number | undefined>(0);
    const contentRef = useRef<HTMLDivElement>(null);
    const {t} = useTranslation();

    useEffect(() => {
        setHeight(contentRef.current?.offsetHeight);
    }, []);

    const passwordMust = [
        t('passwordMustSubtitle'),
        t('includeAtLeast'),
        [
            t('uppercaseCharacter'),
            t('lowercaseCharacter'),
            t('aNumber'),
            t('specialCharacter'),
        ]

    ];

    const passwordMustList = (
        <ul>
            {passwordMust.map((item, index) => {
                if (Array.isArray(item)) {
                    return (
                        <li key={index}>
                            <ul className={'list-style'}
                                style={{listStyleType: 'disc'}}>
                                {item.map((subItem, subIndex) => (
                                    <li key={subIndex}
                                        style={{color: `${COLORS.textBody}`}}>{subItem}</li>
                                ))}
                            </ul>
                        </li>
                    );
                } else {
                    return <li key={index}
                        style={{color: `${COLORS.textBody}`}}>{item}</li>;
                }
            })}
        </ul>
    );

    const { handleSubmit, formState: { errors }, control } = useForm<IResetPassword>({
        resolver: yupResolver(schemaResetPassword)
    });

    
 
    return (
        <Box className={classes.root}>
            <BreadcrumbCustom list={breadcrumbs}/>
            <Grid container
                justifyContent={'center'}
                spacing={0}
                columns={{ xs: 4, sm: 8, md: 12 }}
                className={classes.ResetPassword}
            >
                <Grid item
                    md={4}>
                    <img src={imageAssets.updatePass}
                        alt="Reset-pass" />
                    <Box className="reset-pass-title flex flex-col"
                        component={'div'}
                        ref={contentRef}>
                        
                        <TextCustom className="title"
                            h1
                            color={' #253D4E'}>
                            {t('setNewPassword')}
                        </TextCustom>
                        <TextCustom className={'subtitle pt-[24px]'}
                            textMedium
                            color={COLORS.textBody}>
                            {t('setNewPasswordSubtitle')}

                        </TextCustom>
                                
                    </Box>
                    
                    <Box className="reset-pass-main">
                        <Box component={'form'}
                            onSubmit={handleSubmit(onSubmit)}
                            className={classes.TextFieldCss}
                            autoComplete="off">
                            <TextFieldCustom control={control}
                                errors={errors.password}
                                name={'password'}
                                label={t('password')}
                                type='password'/>
                            <TextFieldCustom control={control}
                                errors={errors.confirm_password}
                                name='confirm_password' 
                                label={t('confirmPassword')}
                                type='password'/>

                            <Box><Button variant="contained"
                                type='submit'
                                className='btn-submit'
                                disabled={false}
                            >
                                {loadingSubmit?
                                    <CircularProgress color="warning" />
                                    :
                                    t('setNewPassword')
                                }
                            </Button></Box>
                        </Box>
                    </Box>
                </Grid>
                <Grid item
                    md={3}
                    className='!ml-[80px]'>
                    <Box style={{ height: `calc(${height}px + 95px)`}}
                    ></Box>
                    <TextCustom color={COLORS.textBody}
                        h6
                        className='!mb-3'
                    >
                        {t('passwordMust')}
                    </TextCustom>
                    {passwordMustList}
                </Grid>
            </Grid>
        </Box>
    );
};

export default withStyles(styles)(ResetPasswordComponent);