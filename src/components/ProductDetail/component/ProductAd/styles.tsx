import { Theme } from '@mui/material';
import { createStyles } from '@mui/styles';
import { } from '@mui/styles/';
import { COLORS } from '@constants/colors';

const styles = (theme: Theme) => createStyles({
    root: {

    },

    detailAdComponent: {
        boxShadow: '0px 3px 20px rgba(24, 24, 24, 0.07)',
        borderRadius: 20,
        '& .btn-shop-now': {
            backgroundColor: `${COLORS.brand1}`,
            color: COLORS.white,
            padding: '12px 15px',
            fontSize: 12,
            fontWeight: 700,
            lineHeight: '15px',
            boxShadow: 'none',
            '&:hover': {
                backgroundColor: `${COLORS.brand2}`,
                boxShadow: 'none',
            }
        },
    },

});
export default styles;