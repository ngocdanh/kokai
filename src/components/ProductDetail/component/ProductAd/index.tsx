import { Box, Button } from '@mui/material';
import { WithStyles, withStyles } from '@mui/styles';
import TextCustom from '@helpers/TextCustom';
import imageAssets from '@constants/imageAssets';
import { useTranslation } from 'next-i18next';
import styles from './styles';
import { IProductAd } from './types';
import { COLORS } from '@constants/colors';
import Link from 'next/link';



const ProductAdComponent = (props: IProductAd & WithStyles<typeof styles>) => {
    const {
        classes,
    } = props;
    const { t } = useTranslation();

    const { detailAd } = imageAssets;

    return (
        <Box className={classes.detailAdComponent} >
            <Box className="container relative flex justify-center">
                <img src={detailAd}
                    alt="" />
                <Box className="absolute top-0 bottom-0 !px-[48px] !py-[34px] flex flex-col justify-between">
                    <Box>
                        <TextCustom
                            h5
                            className=""
                            color={COLORS.brand2}
                        >{t('summerDeal')}</TextCustom>
                        <TextCustom
                            h2
                            className="uppercase !mt-[5px]"
                        >{t('hotHealthy')}</TextCustom>
                        <TextCustom
                            textLarge
                            className="!mt-[5px] pr-[60px]"
                            color={COLORS.textBody}
                        >{t('productDiscounts')}</TextCustom>
                    </Box>
                    <Box>
                        <Link
                            href={'/products/angie’s-boomchickapop-sweet-&-salty'} >
                            <a>
                                <Button variant="contained"
                                    className="btn-shop-now"
                                    endIcon={<i className="fi fi-rr-arrow-small-right !text-[18px]"></i>}
                                >
                                    {t('shopNow')}
                                </Button>
                            </a>
                        </Link>
                    </Box>
                </Box>
            </Box>
        </Box >
    );
};

export default withStyles(styles)(ProductAdComponent);