import { Box } from '@mui/material';
import { WithStyles, withStyles } from '@mui/styles';
import TextCustom from '@helpers/TextCustom';
import { useTranslation } from 'next-i18next';
import styles from './styles';
import { IPopularProducts } from './types';
import { uniqueId, isEmpty } from 'lodash';
import ProductItemSimple from '@helpers/ProductItem/Simple';



const PopularProductsComponent = (props: IPopularProducts & WithStyles<typeof styles>) => {
    const {
        classes,
        listProductPopular,
    } = props;
    const { t } = useTranslation();
    return (
        <Box className={classes.popularProductsComponent} >
            <TextCustom
                h4
                className="heading"
            >
                {t('popularItems')}
            </TextCustom>
            <Box className="flex flex-col">
                {!isEmpty(listProductPopular) && listProductPopular?.map(product =>
                    <ProductItemSimple dataItem={product}
                        key={uniqueId()} />)}
            </Box>
        </Box >
    );
};

export default withStyles(styles)(PopularProductsComponent);