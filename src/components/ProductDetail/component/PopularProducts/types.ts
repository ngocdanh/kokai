import { typeProductItem } from '@common/types';

export interface IPopularProducts {
    listProductPopular: typeProductItem[],
}


export interface IPopularProductItem {
    popularItem?: typeProductItem;
}

