import { Theme } from '@mui/material';
import { createStyles } from '@mui/styles';
import { } from '@mui/styles/';
import { COLORS } from '@constants/colors';


const styles = (theme: Theme) => createStyles({
    root: {

    },

    popularProductsComponent: {
        marginBottom: 55,
        padding: 29,
        borderRadius: 15,
        border: `1px solid ${COLORS.borderColor}`,
        boxShadow: '0px 3px 20px rgba(24, 24, 24, 0.07)',
        '& .heading': {
            paddingBottom: 15,
            borderBottom: `1px solid ${COLORS.borderColor}`,
            display: 'block',
            marginBottom: 0
        },

    }
});

export default styles;