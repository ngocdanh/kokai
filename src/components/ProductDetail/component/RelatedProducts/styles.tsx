import { Theme } from '@mui/material';
import { createStyles } from '@mui/styles';
import { } from '@mui/styles/';
import { COLORS } from '@constants/colors';

const styles = (theme: Theme) => createStyles({
    root: {

    },
    relatedProductComponent: {
        margin: '0 auto',
        marginTop: 110,
        '& .related-heading': {
            marginBottom: 52,
        },
        '& .related-item': {
            position: 'relative',
            border: `1px solid ${COLORS.bgGrey}`,
            borderRadius: 14,
            transition: 'all .3s linear',
            '&:hover': {
                border: `1px solid ${COLORS.brand1}`,
                boxShadow: '20px 20px 40px rgba(24, 24, 24, 0.07)',
            },
            '& .btn': {
                color: `${COLORS.brand1}`,
                fontSize: 13,
                minWidth: 62,
                height: 27,
                display: 'inline-flex',
                padding: '0 5px',
                backgroundColor: '#DEF9EC',
                boxShadow: 'none',
                transition: 'all .3s linear',
                '& .css-9tj150-MuiButton-endIcon': {
                    marginLeft: 2
                },
                '&:hover': {
                    transition: 'all .3s linear',
                    transform: 'translateY(-5px)',
                    backgroundColor: `${COLORS.brand1}`,
                    color: `${COLORS.white}`,
                },
            },
            '& .discount-status': {
                position: 'absolute',
                width: 60,
                height: 32,
                top: 20,
                left: -1,
                textAlign: 'center',
                justifyContent: 'center',
                display: 'flex',
                alignItems: 'center',
                backgroundColor: COLORS.brand1,
                borderRadius: '0px 30px 30px 0px',
                zIndex: 2,
                '&.red': {
                    backgroundColor: COLORS.danger,
                },
                '&.yellow': {
                    backgroundColor: COLORS.warring,
                },
            },
        }
    },

});
export default styles;