import { typeProductItem } from '@common/types';

export interface IRelatedProducts {
    listRelatedProducts?: typeProductItem[];
    productItem?: typeProductItem;
}

