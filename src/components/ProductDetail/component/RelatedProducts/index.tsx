
import TextCustom from '@helpers/TextCustom';
import { Box, Grid } from '@mui/material';

import { WithStyles, withStyles } from '@mui/styles';
import { useTranslation } from 'next-i18next';
import styles from './styles';
import { IRelatedProducts } from './types';
import { uniqueId, isEmpty } from 'lodash';

import { memo } from 'react';
import ProductItem from '@helpers/ProductItem/Default';

const RelatedProductsComponent = (props: IRelatedProducts & WithStyles<typeof styles>) => {
    const { classes, listRelatedProducts } = props;


    const { t } = useTranslation();
    return <>
        <Box className={classes.relatedProductComponent}>
            <TextCustom h3
                center
                className="related-heading">{t('relatedProducts')}</TextCustom>
            <Grid container
                spacing={3} >
                {!isEmpty(listRelatedProducts) && listRelatedProducts?.map(item => (<Grid item
                    xs={12}
                    lg={3}
                    key={uniqueId()}>
                    <ProductItem dataItem={item}
                        disableAction />
                </Grid>))
                }
            </Grid>
        </Box>
    </>;
};

export default memo(withStyles(styles)(RelatedProductsComponent));