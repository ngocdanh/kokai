import { typeProductDetail } from '@components/ProductDetail/types';

export interface IProductBreadcrumbs {
    product?: typeProductDetail
}

export interface ICategoryItem { slug?: string, name?: string }
