import { Box } from '@mui/material';

import 'react-toastify/dist/ReactToastify.css';

import { WithStyles, withStyles } from '@mui/styles';
import { get } from 'lodash';
import { useTranslation } from 'next-i18next';
import styles from './styles';
import { ICategoryItem, IProductBreadcrumbs } from './types';
import { IBreadcrumbItem } from '@helpers/BreadcrumbCustom/types';
import { routeRoot } from '@common/config';
import BreadcrumbCustom from '@helpers/BreadcrumbCustom';

const ProductBreadcrumbsComponent = (props: IProductBreadcrumbs & WithStyles<typeof styles>) => {
    const { classes, product,  } = props;
    const {
        categories,
    } = product ?? {};

    const category: ICategoryItem = get(categories, ['0'], { slug: '', name: '' });

    const { t } = useTranslation();

    const breadcrumbs: IBreadcrumbItem[] = [
        {
            label: t('home'),
            url: routeRoot.home,
        },
        {
            label: category?.name,
            url: `${routeRoot.category}/${category?.slug}`,
        },
        {
            label: product?.name,
        },
    ];

    return <>
        <Box className={classes.ProductBreadcrumbComponent}>
            <BreadcrumbCustom
                list={breadcrumbs}
            />
        </Box>
    </>;
};

export default withStyles(styles)(ProductBreadcrumbsComponent);