import { typeProductDetail } from '@components/ProductDetail/types';

export interface IProduct {
    product?: typeProductDetail
}
