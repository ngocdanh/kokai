import { COLORS } from '@constants/colors';
import TextCustom from '@helpers/TextCustom';
import { Box, Button, Grid, Rating } from '@mui/material';

import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

import { WithStyles, withStyles } from '@mui/styles';
import { useTranslation } from 'next-i18next';
import styles from './styles';
import { IProduct } from './types';

import { useCallback, useState, memo } from 'react';
import Link from 'next/link';
import { routeRoot } from '@common/config';
import { isEmpty, uniqueId } from 'lodash';

const ProductComponent = (props: IProduct & WithStyles<typeof styles>) => {
    const { classes, product } = props;

    const [countQuantity, setCountQuantity] = useState<number>(1);
    const { t } = useTranslation();
    const handleChangeQuantity = (e: React.FormEvent<EventTarget>) => {
        const target = e.target as HTMLInputElement;
        setCountQuantity(Number(target.value));
    };

    const handleIncreaseQuantity = () => {
        setCountQuantity(() => {
            return countQuantity + 1;
        });

    };
    const handleDecreaseQuantity = () => {
        setCountQuantity(() => {
            return countQuantity - 1;
        });
        if (countQuantity <= 1) setCountQuantity(1);
    };

    const notify = useCallback((message: string) => toast.success(message), []);
    const notifyWarn = useCallback((message: string) => toast.warn(message), []);

    const handleAddToCard = () => {
        if (countQuantity === 0) notifyWarn(t('emptyQuantity'));
        else if (countQuantity <= product!.quantity) {
            notify(t('addedToCart'));
        }
        else notifyWarn(t('OverOfStock'));
    };

    return !isEmpty(product) ?
        <Grid container
            className={classes.productComponent}>
            <Grid item
                className="detail-image item"
                border={`1px solid ${COLORS.borderColor}`}
                xs={12}
                md={6} >
                <img className="w-full h-full py-[20px] object-contain"
                    src={product?.image?.original}
                    alt="" />
            </Grid>
            <Grid item
                className="item md:pl-[40px] "
                xs={12}
                md={6}>
                {
                    product?.in_stock === 1 ? <TextCustom textSmall
                        className="!mt-[20px] md:!mt-0 detail-label bg-[#DEF9EC]"
                        color={COLORS.brand1}
                    >In Stock</TextCustom> : ''
                }
                <TextCustom
                    h2
                    className="detail-name"
                    color={COLORS.textHeading}
                >{product?.name}</TextCustom>
                <Box className="mt-[32px]">
                    <Rating
                        name="simple-controlled"
                        value={product?.vote}
                        precision={0.5}
                        max={5}
                        size={'small'}
                        readOnly
                    />
                    <TextCustom
                        textXS
                        className="!ml-2 translate-y-[-4px]"
                        color={COLORS.textBody}
                    >({product?.vote.toFixed(1)})</TextCustom>
                </Box>
                <Box
                    marginTop={2.4}
                >
                    <TextCustom display2
                        className="detail-price"
                        color={COLORS.brand1}
                    >
                        {
                            product?.price ? `$${product?.sale_price || product?.price}` : ''
                        }
                        {
                            !!product?.sale_price && <TextCustom h3
                                className="detail-price_sale "

                                color={COLORS.textMuted}
                            >
                                ${product?.price}
                            </TextCustom>
                        }
                    </TextCustom>
                </Box>
                <Box
                    marginTop={3.2}
                >
                    <TextCustom
                        textLarge
                        color={COLORS.textBody}
                    >
                        {product?.description}
                    </TextCustom>
                </Box>
                <Box
                    marginTop={6.4}
                    className="flex space-x-[10px] h-[50px]"
                >
                    <Box className="detail-quantity "
                        border={`2px solid ${COLORS.brand1}`}
                        color={COLORS.brand1}
                    >
                        <input type="number"
                            min="1"
                            value={countQuantity}
                            onChange={(e) => handleChangeQuantity(e)} />
                        <Box className="flex flex-col justify-center space-y-[4px]">

                            <i className={`fi fi-sr-angle-small-up cursor-pointer text-[${COLORS.brand1}]`}
                                onClick={() => handleIncreaseQuantity()}
                            ></i>
                            <i className={`fi fi-sr-angle-small-down cursor-pointer text-[${COLORS.brand1}]`}
                                onClick={() => handleDecreaseQuantity()}
                            ></i>
                        </Box>
                    </Box>
                    <Button variant="contained"
                        className="btn-card"
                        startIcon={<i className="fi fi-rs-shopping-cart mr-[5px] !text-[16px] translate-y-[2px]"></i>}
                        onClick={handleAddToCard}
                    >
                        {t('addToCart')}
                    </Button>
                    <button
                        className='btn-icon'
                        onClick={() => notify(t('addedToFavorite'))}
                    >
                        <i className="fi fi-rs-heart"></i>

                    </button>
                    <button
                        className='btn-icon'
                        onClick={() => notify(t('addedToComparisonList'))}
                    >
                        <i className="fi fi-rs-shuffle"></i>

                    </button>

                </Box>

                <Box className="mt-[60px]">
                    <Box>
                        <TextCustom
                            textLarge
                            className='flex'
                        >
                            <span className='inline-block w-[110px]'>{t('vendor')}:{' '}</span>
                            <Link href={`${routeRoot.vendor}/${product?.shop?.slug}`}>
                                <a className="detail-info">{product?.shop?.name}</a>
                            </Link>
                        </TextCustom>
                    </Box>

                    <Box className="mt-2">
                        <TextCustom
                            className='flex'
                            textLarge
                        >
                            <span className='inline-block w-[110px]'>SKU:{' '}</span>
                            <span className="detail-info">{product?.sku}</span>
                        </TextCustom>
                    </Box>
                    {!isEmpty(product?.categories) && (
                        <Box className="mt-2" >
                            <TextCustom
                                className='flex'
                                textLarge
                            >
                                <span className='inline-block w-[110px]'>{t('category')}:{' '}</span>
                                {product?.categories?.slice(0, 5)?.map(o => (
                                    <Link key={uniqueId()}
                                        href={`${routeRoot.category}/${o?.slug}`}>
                                        <a>
                                            <span className="detail-info">{o?.name}, </span>
                                        </a>
                                    </Link>
                                ))}
                            </TextCustom>
                        </Box>
                    )}
                  
                </Box>
            </Grid>
        </Grid>: <></>;
};

export default memo(withStyles(styles)(ProductComponent));