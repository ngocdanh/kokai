import { Theme } from '@mui/material';
import { createStyles } from '@mui/styles';
import { } from '@mui/styles/';
import { COLORS } from '@constants/colors';

const styles = (theme: Theme) => createStyles({
    root: {

    },

    productComponent: {
        margin: '0 auto',
        '& .detail-image': {
            minHeight: 696,
            padding: '0 30px',
            borderRadius: 15
        },
        '& .item': {
            '& .detail-name': {
                display: 'block',
                marginTop: 24
            },
            '& .detail-label': {
                display: 'inline-block',
                padding: '5px 11px',
                borderRadius: 4,
                fontWeight: 700,
            },
            '& .detail-price': {
                '&_sale': {
                    textDecorationLine: 'line-through',
                    marginLeft: 32,
                }
            },
            '& .detail-quantity': {
                width: 118,
                borderRadius: 7,
                display: 'flex',
                paddingLeft: 2,
                '& input': {
                    display: 'inline-block',
                    width: '70%',
                    paddingLeft: '20%',
                    fontWeight: 700,
                    fontSize: 20,
                    '&::-webkit-outer-spin-button, &::-webkit-inner-spin-button': {
                        '-webkit-appearance': 'none',
                    },
                    '&[type=number]': {
                        '-moz-appearance': 'textfield'
                    }
                }
            },
            '& .btn-card': {
                borderRadius: 4,
                backgroundColor: `${COLORS.brand1}`,
                padding: '15px 22px',
                fontSize: 18,
                color: COLORS.white,
                fontWeight: 700,
                lineHeight: '20px',
                boxShadow: 'none',
                '&:hover': {
                    backgroundColor: `${COLORS.brand2}`,
                }
            },
            '& .btn-icon': {
                display: 'inline-block',
                borderRadius: 5,
                border: `1px solid ${COLORS.bgGrey}`,
                fontSize: 16,
                padding: 17.5,
                fontWeight: 700,
                color: `${COLORS.textMuted}`,
                transition: 'all .3s linear',
                '&:hover': {
                    transition: 'all .3s linear',
                    transform: 'translateY(-5px)',
                    backgroundColor: `${COLORS.brand1}`,
                    color: `${COLORS.white}`,
                },
            },
            '& .detail-info': {
                cursor: 'pointer',
                color: `${COLORS.textMuted}`,
                '&:hover': {
                    color: `${COLORS.brand2}`,
                }
            }
        },
    },

});
export default styles;