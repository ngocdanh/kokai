import { typeProductItem } from '@common/types';

export interface IProductDetail {
    product?: typeProductDetail;
    listRelatedProducts?: typeProductItem[];
    popularItem?: typeProductItem;
    listProductPopular: typeProductItem[],
}


export type typeProductDetail = {
    name: string,
    slug: string,
    in_stock: number,
    image: {
        original: string,
        thumbnail: string,
    },
    shop: {
        name: string,
        slug: string,
    };
    sku: string,
    price: number | null,
    sale_price: number | null,
    description: string,
    categories: {
        id: number,
        name: string,
        slug: string,
    }[],
    vote: number,
    quantity: number,
};
