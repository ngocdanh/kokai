import { Box, Grid } from '@mui/material';

import 'react-toastify/dist/ReactToastify.css';

import { WithStyles, withStyles } from '@mui/styles';
import styles from './styles';
import { IProductDetail } from './types';
import ProductComponent from './component/Product';
import ProductBreadcrumbsComponent from './component/ProductBreadcrumbs';
import RelatedProductComponent from './component/RelatedProducts';
import ProductAdComponent from './component/ProductAd';
import PopularProducts from './component/PopularProducts';

import { useRef } from 'react';


const ProductDetailComponent = (props: IProductDetail & WithStyles<typeof styles>) => {
    const {
        classes,
        product,
        listRelatedProducts,
        listProductPopular
    } = props;

    const contentRef = useRef<HTMLDivElement>(null);

    return (
        <Box className={classes.root}>
            <ProductBreadcrumbsComponent product={product} />
            <Grid container
                spacing={3}>
                <Grid item
                    lg={9}
                    ref={contentRef}>
                    <ProductComponent product={product} />
                    <RelatedProductComponent listRelatedProducts={listRelatedProducts} />
                </Grid>
                <Grid item
                    lg={3}
                    style={{ minHeight: `${contentRef.current?.offsetHeight}` }}
                >
                    <Box className="sticky -top-[510px]" >
                        <PopularProducts listProductPopular={listProductPopular}/>
                        <ProductAdComponent />
                    </Box>
                </Grid>
            </Grid>
        </Box >
    );
};

export default withStyles(styles)(ProductDetailComponent);