import { IBreadcrumbItem } from '@helpers/BreadcrumbCustom/types';

export interface IProps {
    breadcrumbs: IBreadcrumbItem[],
    onSubmit: (data: IForgotPassword) => void,
    loadingSubmit: boolean,
    onChangeGoogleRecaptcha: (token: string| null) => void,
    isRobot: boolean,
  
}

export type IForgotPassword = {
    emailOrUsername: string,
}