import { Box, Button, CircularProgress } from '@mui/material';
import { WithStyles, withStyles } from '@mui/styles';
import { useTranslation } from 'next-i18next';
import styles from './styles';
import { IForgotPassword, IProps } from './types';
import BreadcrumbCustom from '@helpers/BreadcrumbCustom';
import imageAssets from '@constants/imageAssets';
import TextCustom from '@helpers/TextCustom';
import { yupResolver } from '@hookform/resolvers/yup';
import { schemaForgotPassword } from '@constants/Validate';
import ReCAPTCHA from 'react-google-recaptcha';
import TextFieldCustom from '@helpers/TextFieldCustom';
import { useForm } from 'react-hook-form';
import { COLORS } from '@constants/colors';

const googleRecaptchaKey = process.env.GOOGLE_RECAPTCHA_KEY || '';


const ForgotPasswordComponent = (props: IProps & WithStyles<typeof styles>) => {
    const {
        classes,
        breadcrumbs,
        onSubmit,
        loadingSubmit,
        onChangeGoogleRecaptcha,
        isRobot,
    } = props;

    const {t} = useTranslation();

    const { handleSubmit, formState: { errors }, control } = useForm<IForgotPassword>({
        resolver: yupResolver(schemaForgotPassword)
    });
 
    return (
        <Box className={classes.ForgotPassword}>
            <BreadcrumbCustom list={breadcrumbs} />
            <Box
                display={'flex'}
                justifyContent={'center'}>
                <Box className={'forgot-pass'}>
                    <img className='forgot-pass-img'
                        src={imageAssets.touchId} 
                        alt="Img Touch ID" />
                    <Box className='forgot-pass-title'>
                        <TextCustom h2
                            color={COLORS.textHeading}>
                            {t('forgotYourPassword')}
                        </TextCustom>
                        <TextCustom textMedium
                            color={COLORS.textBody}>
                            {t('forgotYourPasswordSubtitle')}
                        </TextCustom>
                    </Box>
                    <Box className="forgot-pass-main">
                        <Box component={'form'}
                            onSubmit={handleSubmit(onSubmit)}
                            className={classes.TextFieldCss}
                            autoComplete="off">
                            <TextFieldCustom control={control}
                                errors={errors.emailOrUsername}
                                name={'emailOrUsername'}
                                label={t('emailOrUsername')}/>
                            <Box className="security-code">
                                <ReCAPTCHA
                                    sitekey={googleRecaptchaKey}
                                    onChange={onChangeGoogleRecaptcha}
                                />
                            </Box>
                            <Box><Button variant="contained"
                                type='submit'
                                className='btn-submit'
                                disabled={(isRobot)}
                            >
                                {loadingSubmit?
                                    <CircularProgress color="success" />
                                    :
                                    t('resetPassword')
                                }
                            </Button></Box>
                        </Box>
                    </Box>

                </Box>


            </Box>
        </Box>
    );
};

export default withStyles(styles)(ForgotPasswordComponent);