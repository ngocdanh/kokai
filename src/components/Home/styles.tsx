import { Theme } from '@mui/material';
import { createStyles } from '@mui/styles';
import { } from '@mui/styles/';
import { COLORS } from '@constants/colors';

const styles = (theme: Theme) => createStyles({
    root: {
        paddingTop: 33,
        paddingBottom: 33,
    },
    categoryComponent: {
        marginTop: 66,
        marginBottom: 66,
        '& .swiper': {
            borderRadius: '10px',
        },
        '& .category-item': {
            padding: 20,
            height: 200,
            backgroundColor: '#F4F6FA',
            display: 'flex',
            flexDirection: 'column',
            justifyContent: 'center',
            border: '1px solid transparent',
            transition: 'all .3s ease',
            borderRadius: '10px',
            '& svg': {
                width: '100%',
                height: 70,
                marginBottom: 20,
                objectFit: 'contain',
            },
            '& .title': {
                transition: 'all .3s ease',
                '&:hover': {
                    color: COLORS.brand1,
                }
            },
            '& .amount': {

            },
            '& .category-slider': {
                position: 'relative',

            },

            '&:hover': {
                border: `1px solid ${COLORS.brand1}`,
                '& svg': {
                    color: COLORS.brand1
                },
                '& .title': {
                    color: COLORS.brand1
                }
            }
        },
        '& .slick-arrow': {
            width: 40,
            height: 40,
            background: COLORS.bgGrey,
            cursor: 'pointer',
            borderRadius: '40px',
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'center',
            transition: 'all .3s ease',
            border: '1px solid transparent',
            '& i': {
                fontSize: 24,
                lineHeight: '24px',
                color: COLORS.textBody,
            },
            '&:hover': {
                border: `1px solid ${COLORS.brand1}`,
                background: COLORS.brand1,
                '& i': {
                    color: COLORS.white,
                }
            }
        },
        '& .all-link': {
            '&:hover': {
                color: `${COLORS.brand1} !important`,
            }
        }
    },
    sliderComponent: {
        borderRadius: '10px',
        overflow: 'hidden',
        '& .swiper': {
            borderRadius: '25px',
            overflow: 'hidden',
        },
        '& .slider-item': {
            position: 'relative',
            borderRadius: '25px',
            overflow: 'hidden',
            '& img': {
                width: '100%',
                objectFit: 'cover',
                height: 'calc(100vh - 300px)',
                maxHeight: 'calc(100vh - 300px)',
                minHeight: 550,
                
            },
            '& .content': {
                position: 'absolute',
                top: 0,
                left: 0,
                width: '100%',
                height: '100%',
                maxWidth: 900,
                display: 'flex',
                flexDirection: 'column',
                justifyContent: 'center',
                padding: 100,
                paddingRight: 0,
                '& .title': {
                },
                '& .description': {
                    marginTop: 33,
                },
                '& .box-input': {
                    marginTop: 68,
                    position: 'relative',
                    width: 450,
                    '& input': {
                        borderRadius: '30px',
                        background: COLORS.white,
                        height: 50,
                        fontSize: 12,
                        fontWeight: 400,
                        width: '100%',
                        lineHeight: '18px',
                        color: COLORS.textBody,
                        paddingLeft: 57,
                        paddingTop: 15,
                        paddingBottom: 15,
                        '&::placeholder': {
                            color: '#B6B6B6',
                        }
                    },
                    '& .button-custom': {
                        position: 'absolute',
                        right: 0,
                        borderRadius: '30px !important',
                        width: 150,
                        display: 'flex',
                        justifyContent: 'center',
                        top: 0,
                        bottom: 0,
                    },
                    '& img': {
                        width: 24,
                        height: 24,
                    },
                    '& i': {
                        position: 'absolute',
                        left: -40,
                        borderRadius: '30px !important',
                        width: 150,
                        zIndex: 100,
                        display:'flex',
                        justifyContent: 'center',
                        top: 17,
                        bottom: 0,
                    }
                }
            }
        },
    },
});

export default styles;