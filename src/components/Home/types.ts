import { typeProductItem } from '@common/types';
import { MouseEvent } from 'react';
export interface IHomePage {
    listProduct: {
        items: {
            title: string,
            slug: string,
            id: string,
            price: number,
            oldPrice: number,
            category: string,
            weight: number,
            desc: string,
            images: {
                img: string
            }[],
            totalSell: number,
            ratingScore: number,
            rating: number,
            gallery: {
                thumb?: string
            }[],
        }[]
    },
}

export type typeActiveCategory = {
    name: string,
    slug: string,
    icon: string,
}

export type typeDataTrending = {
    listRecently: typeProductItem[],
    listTopRate: typeProductItem[],
    listTopSelling: typeProductItem[],
    listTrendingProducts: typeProductItem[],
};

export interface IDataItem {
    dataItem?: typeProductItem
}

export interface IHome extends ISlider {
    dataCategory?: {
        name: string,
        slug: string,
        icon: string,
        amount?: number,
    }[],
    dataListProduct: typeProductItem[],
    dataListProductSaleOfDay: typeProductItem[],
    dataListProductBestSeller: typeProductItem[],
    dataTrending: typeDataTrending,
    handleChangeMenuProduct: (e: MouseEvent<HTMLLIElement>,cat: typeProductItem) => void,
    activeMenuProduct: typeActiveCategory,
    handleChangeMenuBestSeller: (e: MouseEvent<HTMLLIElement>,seller: typeActiveCategory) => void,
    activeMenuBestSeller: typeActiveCategory,
    listCategoryProduct: typeActiveCategory[],
    listCategoryBestSeller: typeActiveCategory[],
    handleAddToWishlist?: (e: MouseEvent<HTMLElement>,_id: string) => void,
    isLoading?: boolean,
}

export interface ICategory {
    dataCategory?: {
        name: string,
        slug: string,
        icon: string,
        amount?: number,
    }[]
}
export interface ISlider {
    dataSlider: {
        title: {
            en: string,
            vi: string,
        },
        background: string,
    }[],
}