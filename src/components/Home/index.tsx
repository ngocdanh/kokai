import { Box } from '@mui/material';
import { WithStyles, withStyles } from '@mui/styles';
import { useTranslation } from 'next-i18next';
import Banner from './component/Banner';
import BestSeller from './component/BestSeller';
import Category from './component/Category';
import SaleOfDay from './component/SaleOfDay';
import ProductPopular from './component/ProductPopular';
import Slider from './component/Slider';
import Trending from './component/ProductTabs';
import styles from './styles';
import { IHome } from './types';


const HomeComponent = (props: IHome & WithStyles<typeof styles>) => {
    const {
        classes,
        dataSlider,
        dataCategory,
        dataListProduct,
        handleChangeMenuProduct,
        activeMenuProduct,
        handleChangeMenuBestSeller,
        activeMenuBestSeller,
        dataListProductBestSeller,
        dataListProductSaleOfDay,
        dataTrending,
        listCategoryProduct,
        listCategoryBestSeller,
        handleAddToWishlist,
    } = props;

    const {t} = useTranslation();
 
    return (
        <Box className={classes.root}>
            <Slider
                dataSlider={dataSlider}
            />
            <Category 
                dataCategory={dataCategory}
            />
            <Banner />
            <ProductPopular
                handleChangeMenuProduct={handleChangeMenuProduct}
                activeMenuProduct={activeMenuProduct}
                dataListProduct={dataListProduct}
                listCategoryProduct={listCategoryProduct}
                handleAddToWishlist={handleAddToWishlist}
            />
            <BestSeller
                handleChangeMenuBestSeller={handleChangeMenuBestSeller}
                activeMenuBestSeller={activeMenuBestSeller}
                dataBestSeller={dataListProductBestSeller}
                listCategoryBestSeller={listCategoryBestSeller}
            />
            <SaleOfDay dataSaleOfDay={dataListProductSaleOfDay}/>
            <Trending 
                dataTrending={dataTrending}
            />

        </Box>
    );
};

export default withStyles(styles)(HomeComponent);