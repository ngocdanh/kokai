import { Box } from '@mui/material';
import { WithStyles, withStyles } from '@mui/styles';
import clsx from 'clsx';
import { COLORS } from '@constants/colors';
import TextCustom from '@helpers/TextCustom';
import { isEmpty, range, uniqueId } from 'lodash';
import { useCallback, useRef } from 'react';
import { useTranslation } from 'next-i18next';
import { Autoplay } from 'swiper';
import { Swiper, SwiperSlide } from 'swiper/react';
import styles from './../styles';
import { ICategory } from './../types';
import { RenderIconCategory } from '@helpers/icons';
import { routeRoot } from '@common/config';
import Link from 'next/link';
import SkeletonCustom from '@helpers/SkeletonCustom';
import { useSelector } from 'react-redux';


const CategoryComponent = (props: ICategory & WithStyles<typeof styles>) => {
    const {
        classes,
        dataCategory,
    } = props;

    const { t } = useTranslation();

    const sliderRef: any = useRef(null);
    const {isLoading} = useSelector((state: any) => state.global);


    const handlePrev = useCallback(() => {
        if (!sliderRef.current) return;
        sliderRef.current && sliderRef.current?.swiper.slidePrev();
    }, []);

    const handleNext = useCallback(() => {
        if (!sliderRef.current) return;
        sliderRef.current && sliderRef.current?.swiper.slideNext();
    }, []);


    return (
        <Box className={classes.categoryComponent}>
            <Box
                display={'flex'}
                paddingBottom={5}
                justifyContent='space-between'
                alignItems={'flex-end'}
            >
                <Box className="flex">
                    <TextCustom className='!mr-9'
                        h3
                        color={COLORS.textHeading}>{t('featuredCategories')}</TextCustom>
                    <Box className='ml-4 flex items-center'>
                        <TextCustom textMedium
                            color={COLORS.textBody}>
                            <a className='flex items-center all-link'
                                href={routeRoot.category}>{t('allCategories')} 
                                <div className="ml-2">{'>'}</div></a>
                        </TextCustom>
                    </Box>
                </Box>
                <Box className='custom-arrow flex'>
                    <Box
                        className="slider-btn slider-prev slick-arrow custom-prev mr-2"
                        onClick={handlePrev}
                    >
                        <i className="fi-rs-arrow-small-left"></i>
                    </Box>
                    <Box
                        className="slider-btn slider-next slick-arrow custom-next"
                        onClick={handleNext}
                    >
                        <i className="fi-rs-arrow-small-right"></i>
                    </Box>
                </Box>
            </Box>
            <Box className="category-slider">
                <Swiper
                    spaceBetween={15}
                    onInit={(core) => {
                        sliderRef.current = core.el;
                    }}
                    breakpoints={{
                        480: {
                            slidesPerView: 2
                        },
                        640: {
                            slidesPerView: 3
                        },
                        768: {
                            slidesPerView: 4
                        },
                        1024: {
                            slidesPerView: 6
                        },
                        1200: {
                            slidesPerView: 8
                        }
                    }}
                    autoplay={true}
                    speed={3000}
                    navigation={{
                        prevEl: '.custom-prev',
                        nextEl: '.custom-next'
                    }}
                    modules={[Autoplay]}
                    loop={true}
                    loopFillGroupWithBlank={true}
                >
                    {!isEmpty(dataCategory) && dataCategory?.map(category => (
                        <SwiperSlide key={uniqueId()}>
                            <Box className={
                                clsx('category-item')
                            }>
                                <Link href={`${routeRoot.category}/${category?.slug}`}>
                                    <a>
                                        {RenderIconCategory(category?.icon)}
                                        <TextCustom className='title'
                                            center
                                            h6
                                            color={COLORS.textHeading}>
                                            {category?.name}
                                        </TextCustom>
                                        {category?.amount && (
                                            <TextCustom center
                                                className='amount'
                                                textMuted
                                                color={COLORS.textMuted}>
                                                {`${category?.amount} ${t('items')}`}
                                            </TextCustom>
                                        )}
                                    </a>
                                </Link>
                            </Box>
                        </SwiperSlide>
                    ))}
                    {isLoading && range(8).map(r => (
                        <SwiperSlide key={uniqueId()}>
                            <SkeletonCustom
                                imgHeight={200}
                                disableText
                            />
                        </SwiperSlide>
                    ))}

                </Swiper>

            </Box>
        </Box >
    );
};

export default withStyles(styles)(CategoryComponent);