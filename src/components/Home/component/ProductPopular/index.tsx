import { Box, Grid } from '@mui/material';
import { WithStyles, withStyles } from '@mui/styles';
import clsx from 'clsx';
import { COLORS } from '@constants/colors';
import TextCustom from '@helpers/TextCustom';
import { get, isEmpty, isEqual, range, uniqueId } from 'lodash';
import styles from './styles';
import { IProduct } from './types';
import { useTranslation } from 'next-i18next';
import ProductItem from '@helpers/ProductItem/Default';
import SkeletonCustom from '@helpers/SkeletonCustom';
import { useSelector } from 'react-redux';


const ProductPopularComponent = (props: IProduct & WithStyles<typeof styles>) => {
    const {
        classes,
        dataListProduct,
        handleChangeMenuProduct,
        activeMenuProduct,
        listCategoryProduct,
        handleAddToWishlist,
    } = props;
    const { t } = useTranslation();
    const {isLoading} = useSelector((state: any) => state.global);

    const listProductFilter = !isEmpty(dataListProduct) ?
        dataListProduct?.filter(o => {
            if(activeMenuProduct.slug === '') return true;
            return isEqual(get(o, ['categories', '0'], {}), activeMenuProduct);
        })
        : [];
        
    return (
        <Box className={classes.productComponent}>
            <Box className="flex items-end justify-between mb-13 box-title">
                <TextCustom h3
                    color={COLORS.textHeading}>{t('popularProduct')}</TextCustom>
                <Box className="flex"
                    component={'ul'}>
                    {!isEmpty(listCategoryProduct) && listCategoryProduct?.slice(0, 7)?.map((o: any) => (
                        <Box
                            component={'li'}
                            key={uniqueId()}
                            className='ml-7 cursor-pointer'
                            onClick={(e: any) => handleChangeMenuProduct(e, o)}
                        >
                            {
                                <TextCustom
                                    textMedium
                                    color={COLORS.textHeading}
                                    className={clsx({ '!font-bold': JSON.stringify(activeMenuProduct) === JSON.stringify(o) })}
                                >
                                    {o.name}
                                </TextCustom>
                            }
                        </Box>
                    ))}
                </Box>
            </Box>
            <Box className="list-product"
                paddingY={6}>
                <Grid container
                    alignItems={'stretch'}
                    minHeight={'100%'}
                    spacing={3}>
                    {!isEmpty(listProductFilter) && listProductFilter?.map(o => (
                        <Grid item
                            xs={12}
                            sm={6}
                            lg={12 / 5}
                            md={4}
                            key={uniqueId()}>
                            <ProductItem dataItem={o}
                                handleAddToWishlist={handleAddToWishlist}
                            />
                        </Grid>
                    ))}
                    {isLoading && range(10).map(r => (
                        <Grid item
                            xs={12}
                            sm={6}
                            lg={12 / 5}
                            md={4}
                            key={uniqueId()}>
                            <SkeletonCustom 
                                imgHeight={250}
                                className='mb-3'/>
                        </Grid>
                    ))}
                </Grid>
            </Box>
        </Box>
    );
};

export default withStyles(styles)(ProductPopularComponent);