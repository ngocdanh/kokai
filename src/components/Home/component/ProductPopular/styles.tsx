import { Theme } from '@mui/material';
import { createStyles } from '@mui/styles';
import { } from '@mui/styles/';
import { COLORS } from '@constants/colors';

const styles = (theme: Theme) => createStyles({
    root: {
        paddingTop: 33,
        paddingBottom: 33,
    },
    productComponent: {
        marginBottom: 66,
        '& .list-product': {

        },
        '& .box-title': {
            [theme.breakpoints.down('md')]: {
                display: 'block',
                '& ul': {
                    marginTop: 15,
                    '& li': {
                        marginLeft: 0,
                        marginRight: 28,
                    }
                }
            }
        }
    },
    productItem: {
        border: `1px solid ${COLORS.borderColor}`,
        borderRadius: '15px',
        padding: 19,
        overflow: 'auto',
        position: 'relative',
        transition: 'all .3s ease',
        '& .image': {
            position: 'relative',
            minHeight: 200,
            width: '100%',
            paddingBottom: 20,

            '& img': {
                transition: 'all .6s ease',
                width: '100%',
                objectFit: 'contain',
                height: 200,
            },
            [theme.breakpoints.down('md')]: {
                '& img': {
                    height: 300,
                }
            },
        },
        
        '& .discount-status': {
            position: 'absolute',
            width: 60,
            height: 32,
            top: 20,
            left: 0,
            textAlign: 'center',
            justifyContent: 'center',
            display: 'flex',
            alignItems: 'center',
            backgroundColor: COLORS.brand1,
            borderRadius: '0px 30px 30px 0px',
            zIndex: 2,
            '&.red': {
                backgroundColor: COLORS.danger,
            },
            '&.yellow': {
                backgroundColor: COLORS.warring,
            },
        },
        '& .content': {
            '& .category': {
                transition: 'all .3s ease',
                '&:hover': {
                    color: COLORS.brand1
                }
            },
            '& .title': {
                transition: 'all .3s ease',
                minHeight: 40,
                '&:hover': {
                    color: `${COLORS.brand1} !important`,
                }
            },
            '& .rating': {
                '& i': {
                    color: '#CDCDCD',
                    width: 12,
                    height: 12,
                },
                '& .active': {
                    color: COLORS.warring,
                },
                '& .not-active': {
                    color: '#CDCDCD',
                }
            },
            '& .button-add': {
                background: '#DEF9EC !important',
                color: `${COLORS.brand1} !important`,
                fontWeight: '700 !important',
                '& i': {
                    marginLeft: 8,
                    fontWeight: '700 !important',
                },
                '&:hover': {
                    backgroundColor: `${COLORS.warring} !important`,
                    color: `${COLORS.white} !important`,
                }
            }
        },
        '&:hover': {
            border: `1px solid ${COLORS.brand1}`,
            boxShadow: '20px 20px 40px rgba(24, 24, 24, 0.07)',
            '& .quick-action': {
                visibility: 'visible',
                opacity: 1,
            }
        }
    },
});

export default styles;