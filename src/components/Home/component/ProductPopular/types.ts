import { typeProductItem } from '@common/types';
import { typeActiveCategory } from '@components/Home/types';
import { MouseEvent } from 'react';
import * as React from 'react';

export interface IProduct {
    dataListProduct?: typeProductItem[],
    handleChangeMenuProduct: (e: MouseEvent<HTMLLIElement>,cat: typeProductItem) => void,
    activeMenuProduct: typeActiveCategory,
    listCategoryProduct: typeActiveCategory[],
    handleAddToWishlist?: (e: React.MouseEvent<HTMLElement>,_id: string) => void,
}
export interface IDataItem {
    dataItem?: typeProductItem
}