import { Theme } from '@mui/material';
import { createStyles } from '@mui/styles';
import { } from '@mui/styles/';
import { COLORS } from '@constants/colors';

const styles = (theme: Theme) => createStyles({
    root: {
        paddingTop: 33,
        paddingBottom: 33,
    },
   
    trendingComponent: {
        marginBottom: 30,
        '& .top-selling': {
            '& .title': {
                '&::after': {
                    content: '',
                    width: '80px',
                    height: '2px',
                    position: 'absolute',
                    bottom: '0',
                    left: '0',
                    backgroundColor: '#bce3c9'
                },
            }
        },
    },
    trendingItem: {
        objectFit: 'contain',
        marginTop: 25,
        border: `1px solid ${COLORS.white}`,
        display: 'flex',
        alignItems: 'center',
        padding: 0,
        overflow: 'hidden',
        transition: 'all .3s ease',
        '& .image': {
            display: 'flex',
            justifyContent: 'center',
            width: 120,  
            height: 120,
            '& img': {
                width: '100%',
                height: '100%',
                objectFit: 'contain',
                transition: 'all .3s ease',
            },
      
        },
        '& .content': {
            width: '62%',
            '& .title': {
                transition: 'all .3s ease',
                '&:hover': {
                    color: `${COLORS.brand1} !important`,
                }
            },
            '& .rating': {
                '& i': {
                    color: '#CDCDCD',
                },
                '& .active': {
                    color: COLORS.warring,
                },
                '& .not-active': {
                    color: '#CDCDCD',
                }
            },
        },
        '&:hover': {
            transform: 'translateY(-6px)',
        
        }

    }
});

export default styles;