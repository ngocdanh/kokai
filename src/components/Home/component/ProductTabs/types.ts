import { typeProductItem } from '@common/types';

export type dataTrending = {
    listRecently: typeProductItem[],
    listTopRate: typeProductItem[],
    listTopSelling: typeProductItem[],
    listTrendingProducts: typeProductItem[],
};



export interface IProductTabs {
    dataTrending: dataTrending,
}