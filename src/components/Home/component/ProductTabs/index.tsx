import { WithStyles, withStyles } from '@mui/styles';
import { Box, Grid } from '@mui/material';
import { COLORS } from '@constants/colors';
import TextCustom from '@helpers/TextCustom';
import { isEmpty, range, uniqueId } from 'lodash';
import styles from './styles';
import { IProductTabs } from './types';
import { useTranslation } from 'next-i18next';
import ProductItemSimple from '@helpers/ProductItem/Simple';
import SkeletonCustom from '@helpers/SkeletonCustom';
import { useSelector } from 'react-redux';

const ProductTabComponent = (props: IProductTabs & WithStyles<typeof styles>) => {

    const {
        classes,
        dataTrending
    } = props;
    const { listTopSelling , listTopRate, listRecently, listTrendingProducts } = dataTrending;
    
    const { t } = useTranslation();
    const {isLoading} = useSelector((state: any) => state.global);

    return (
        <Grid container
            spacing={{ xs: 0, md: 0 }}
            columns={{ xs: 4, sm: 8, md: 12 }}
            paddingTop={7}
            className={ classes.trendingComponent } >
            <Grid item
                xs={4}
                sm={4}
                md={3}
                className={'top-selling'}> 
                <TextCustom h4
                    className="pb-6 py-3 title "
                    color={COLORS.textHeading}>{t('topSelling')}</TextCustom>
                <Box>
                    {!isEmpty(listTopSelling) && listTopSelling?.map( (o) => (
   
                        <ProductItemSimple dataItem={o}
                            key={uniqueId()} />
                    ))
                    }
                    {isLoading && range(3).map((r) => (
                        <SkeletonCustom imgHeight={100}
                            key={uniqueId()}
                            imgWidth={100}
                            textWidth={'60%'}
                            inline/>
                    ))}
                </Box>
                
            </Grid>
            <Grid item
                xs={4}
                sm={4}
                md={3}
                className={'trending-products'}>
                <TextCustom h4 
                    className="pb-6 py-3 title "
                    color={COLORS.textHeading}>{t('trendProduct')}</TextCustom>
                <Box>
                    {!isEmpty(listTrendingProducts) && listTrendingProducts?.map( (o) => (
                        <ProductItemSimple dataItem={o}
                            key={uniqueId()} />  ))
                    }
                    {isLoading && range(3).map(r => (
                        <SkeletonCustom 
                            key={uniqueId()}
                            imgHeight={100}
                            imgWidth={100}
                            textWidth={'60%'}
                            inline/>
                    ))}
                </Box>
            </Grid>
                
            <Grid item
                xs={4}
                sm={4}
                md={3}
                className={'recently-added'}>
                <TextCustom h4 
                    className="pb-6 py-3 title"
                    color={COLORS.textHeading}>{t('recentlyAdded')}</TextCustom>
                <Box>
                    {!isEmpty(listRecently) && listRecently?.map( (o) => (
                        <ProductItemSimple dataItem={o}
                            key={uniqueId()} />
                    ))}
                    {isLoading && (range(3).map(r => (
                        <SkeletonCustom 
                            key={uniqueId()}
                            imgHeight={100}
                            imgWidth={100}
                            textWidth={'60%'}
                            inline/>
                    )))}
                </Box>
            </Grid>
            <Grid item
                xs={4}
                sm={4}
                md={3}
                className={'top-rate'}>
                <TextCustom h4 
                    className="pb-6 py-3 title "
                    color={COLORS.textHeading}>{t('topRated')}</TextCustom>
                <Box>
                    {!isEmpty(listTopRate) && listTopRate?.map( (o) => (
                        <ProductItemSimple dataItem={o}
                            key={uniqueId()} />
                    ))}
                    {isLoading && (range(3).map(r => (
                        <SkeletonCustom 
                            key={uniqueId()}
                            imgHeight={100}
                            imgWidth={100}
                            textWidth={'60%'}
                            inline/>
                    )))}
                </Box>
            </Grid>
        </Grid>
    );
};

export default withStyles(styles)(ProductTabComponent);
