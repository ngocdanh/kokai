import { Box, Grid } from '@mui/material';
import { WithStyles, withStyles } from '@mui/styles';
import clsx from 'clsx';
import { COLORS } from '@constants/colors';
import ButtonCustom from '@helpers/ButtonCustom';
import TextCustom from '@helpers/TextCustom';
import { get, isEmpty, isEqual, range, uniqueId } from 'lodash';
import { useTranslation } from 'next-i18next';
import styles from './styles';
import { IBestSeller } from './types';
import BestSellerItem from './BestSellerItem';
import SkeletonCustom from '@helpers/SkeletonCustom';
import { useSelector } from 'react-redux';



const BestSellerComponent = (props: IBestSeller & WithStyles<typeof styles>) => {
    const {
        classes,
        dataBestSeller,
        handleChangeMenuBestSeller, 
        activeMenuBestSeller,
        listCategoryBestSeller,
    } = props;

    const { t } = useTranslation();
    const {isLoading} = useSelector((state: any) => state.global);

    const listBestSellerFilter = !isEmpty(dataBestSeller) ?
        dataBestSeller?.filter(o => {
            if(activeMenuBestSeller.slug === '') return true;
            return isEqual(get(o, ['categories', '0'], {}), activeMenuBestSeller);
        })
        : [];
    

    return (
        <Box className={classes.bestSellerComponent}>
            <Box className="flex items-end justify-between mb-13">
                <TextCustom h3
                    color={COLORS.textHeading}>{t('bestSeller')}</TextCustom>
                <Box className="flex"
                    component={'ul'}>
                    {!isEmpty(listCategoryBestSeller) && listCategoryBestSeller?.map(o => (
                        <Box 
                            component={'li'} 
                            key={uniqueId()}
                            className='ml-7 cursor-pointer'
                            onClick={(e: any)=> handleChangeMenuBestSeller(e, o)}
                        >
                            {
                                <TextCustom 
                                    textMedium 
                                    color={COLORS.textHeading}
                                    className={clsx({'!font-bold': isEqual(activeMenuBestSeller, o) })}
                                >
                                    {o.name}
                                </TextCustom>
                            }
                        </Box>
                    ))}
                </Box>
            </Box>
            <Box className="list-seller flex"
                paddingTop={6}>
                <Box className='banner-seller'>
                    <TextCustom h2
                        color={COLORS.textHeading}>
                        {t('bannerSeller')}
                    </TextCustom>
                    <ButtonCustom iconEnd={'fi-rs-arrow-small-right ml-2'}>{t('shopNow')}</ButtonCustom>
                </Box>
                <Grid container
                    spacing={3}>
                    {!isEmpty(listBestSellerFilter) && listBestSellerFilter?.slice(0, 4)?.map(o => (
                        <Grid item
                            xs={12}
                            sm={6}
                            lg={3}
                            md={6}
                            key={uniqueId()}>
                            <BestSellerItem dataItem={o}/>                            
                        </Grid>
                    ))}
                    {isLoading && range(4).map(r => ( 
                        <Grid item
                            xs={12}
                            sm={6}
                            lg={3}
                            md={6}
                            key={uniqueId()}>
                            <SkeletonCustom 
                                imgHeight={300}/>                        
                        </Grid>
                    
                    ))}
                </Grid>
            </Box>
        </Box>
    );
};

export default withStyles(styles)(BestSellerComponent);