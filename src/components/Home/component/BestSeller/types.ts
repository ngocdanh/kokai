import { typeProductItem } from '@common/types';
import { typeActiveCategory } from '@components/Home/types';
import { MouseEvent } from 'react';

export interface IBestSeller {
    handleChangeMenuBestSeller: (e: MouseEvent<HTMLLIElement>,seller: typeActiveCategory) => void,
    activeMenuBestSeller: typeActiveCategory,
    listCategoryBestSeller: typeActiveCategory[],
    dataBestSeller: typeProductItem[],
}
