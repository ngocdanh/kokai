import { Box, Rating } from '@mui/material';
import { WithStyles, withStyles } from '@mui/styles';
import { COLORS } from '@constants/colors';
import ButtonCustom from '@helpers/ButtonCustom';
import TextCustom from '@helpers/TextCustom';
import { get, isEmpty } from 'lodash';
import Link from 'next/link';
import { useTranslation } from 'next-i18next';
import styles from './styles';
import { convertToDiscountStatus } from '@common/convertData';
import clsx from 'clsx';
import { IDataItem } from '@components/Home/types';
import { routeRoot } from '@common/config';
import { toast } from 'react-toastify';

const BestSellerItemComponent = (props: IDataItem & WithStyles<typeof styles>) => {
    const {
        classes,
        dataItem,
    } = props;

    const discount = convertToDiscountStatus(dataItem);
    const { t } = useTranslation();
    const slugFistCategory = get(dataItem, ['categories', '0', 'slug'],'');
    const handleAddToCart = (e: React.MouseEvent<HTMLButtonElement>) => {
        e.stopPropagation();
        toast.success(String(t('addedToCart')), {toastId: dataItem?.slug});
    };
    return (
        <Box className={classes.bestSellerItem}>
            <Box className='product'>
                {discount.value && (
                    <Box className={clsx('discount-status', discount.css_class)}>
                        <TextCustom textXS
                            color={COLORS.white}>{t(discount.value)}</TextCustom>
                    </Box>
                )}
                <Box className="image">
                    {!isEmpty(dataItem?.image) && (
                        <>
                            <img src={dataItem?.image?.original} />
                        </>
                    )}
                </Box>
                <Box className="content">
                    {!isEmpty(dataItem?.categories) && (
                        <TextCustom textXS
                            hoverColor
                            className='mb-1'
                            color={COLORS.textBody}>
                            <Link href={`${routeRoot.category}/${slugFistCategory}`}>
                                {get(dataItem, ['categories', '0', 'name'],'')}
                            </Link>
                        </TextCustom>
                    )}
                    <Link
                        href={`${routeRoot.product}/${dataItem?.slug}`}>
                        <a className='block w-fit'>
                            <TextCustom headingSM
                                className='mb-1 mt-1 title'
                                color={COLORS.textHeading}>{dataItem?.name}</TextCustom>
                        </a>
                    </Link>
                    <Box className="flex rating items-center mb-2">
                        <Rating
                            name="simple-controlled"
                            value={dataItem?.vote}
                            precision={0.5}
                            max={5}
                            size={'small'}
                            readOnly
                        />
                        <TextCustom textXS
                            className='!ml-3'
                            color={COLORS.textBody}>
                            {`(${dataItem?.total} items)`}
                        </TextCustom>
                    </Box>
                    <Box className="box-price my-3 flex items-end">
                        <TextCustom className='!mr-2'
                            h5
                            color={COLORS.brand1}>{`$${dataItem?.sale_price || dataItem?.price}`}</TextCustom>
                        {dataItem?.price && !!dataItem?.sale_price && <TextCustom
                            textXS
                            className='line-through'
                            color={COLORS.textBody}>{`$${dataItem?.price}`}</TextCustom>}

                    </Box>
                    {dataItem?.weight && (
                        <TextCustom textXS
                            className='!mt-1 !mb-2'
                            color={COLORS.textBody}>
                            {`${dataItem?.weight} gam`}
                        </TextCustom>

                    )}

                    <Box className="group-sold !mb-6">
                        <Box className="progress-sold !mb-1">
                            <span style={{
                                width: `${((dataItem?.sold || 0)/(dataItem?.total || 1))*100}%`
                            }}/>
                        </Box>
                        <TextCustom textXS
                            color={COLORS.textHeading}>
                            {`${t('sold')}: ${dataItem?.sold}/${dataItem?.total}`}
                        </TextCustom>
                    </Box>
                    <ButtonCustom cartBig
                        onClick={handleAddToCart}
                        iconStart={'fi-rs-shopping-cart'}
                        className='!h-11 !rounded w-full !text-white'
                    >
                        {t('addToCart')}
                    </ButtonCustom>
                </Box>
            </Box>
        </Box>
    );
};

export default withStyles(styles)(BestSellerItemComponent);