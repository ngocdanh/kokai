import { Box } from '@mui/material';
import { WithStyles, withStyles } from '@mui/styles';
import { COLORS } from '@constants/colors';
import ButtonCustom from '@helpers/ButtonCustom';
import TextCustom from '@helpers/TextCustom';
import { isEmpty, range, uniqueId } from 'lodash';
import { useTranslation } from 'next-i18next';
import SwiperCore, { Autoplay, Pagination } from 'swiper';
import { Swiper, SwiperSlide } from 'swiper/react';
import styles from './../styles';
import { ISlider } from './../types';
import { useRouter } from 'next/router';
import SkeletonCustom from '@helpers/SkeletonCustom';
import { useSelector } from 'react-redux';


const SliderComponent = (props: ISlider & WithStyles<typeof styles>) => {
    const {
        classes,
        dataSlider,
    } = props;
    
    const router = useRouter();
    const lang = router.locale as 'vi' | 'en';
    const {isLoading} = useSelector((state: any) => state.global);
    
    const {t} = useTranslation();
    SwiperCore.use([Pagination]);

    return (
        <Box className={classes.sliderComponent}>
            <Swiper
                spaceBetween={15}
                autoplay={!isLoading}
                slidesPerView={1}
                pagination={true}
                centeredSlides={true}
                centeredSlidesBounds={true}
                loop={true}
                speed={2000}
                modules={[Pagination, Autoplay]}
            >
                {!isEmpty(dataSlider) && dataSlider?.map(slider => (
                    <SwiperSlide key={uniqueId()}>
                        <Box className='slider-item'>
                            <img src={slider?.background}/>
                            <Box className="content">
                                <TextCustom display2
                                    className='mb-8 title'
                                    color={COLORS.textHeading}>{slider?.title[lang]}</TextCustom>
                                <TextCustom h4
                                    className='description'
                                    color={COLORS.textBody}>{t('titleButtonSubscribe')}</TextCustom>
                                <Box className="box-input">
                                    <i className="text-body fi fi-rs-paper-plane w-6 h-6"></i>
                                    <input type="text"
                                        placeholder={t('enterYourEmail')} />
                                    <ButtonCustom normal>{t('subscribe')}</ButtonCustom>
                                </Box>
                            </Box>
                        </Box>
                    </SwiperSlide>
                ))}
                {isLoading && range(4).map(r => (
                    <SwiperSlide key={uniqueId()}>
                        <Box className='slider-item'>
                            <SkeletonCustom 
                                imgHeight={'calc(100vh - 300px)'}
                                disableText
                            />
                        </Box>
                    </SwiperSlide>
                ))}
            </Swiper>
        </Box>
    );
};

export default withStyles(styles)(SliderComponent);