import { Theme } from '@mui/material';
import { createStyles } from '@mui/styles';
import { } from '@mui/styles/';

const styles = (theme: Theme) => createStyles({
    root: {
        paddingTop: 33,
        paddingBottom: 33,
    },
    bannerComponent: {
        '& .banner-item': {
            borderRadius: '10px !important',
            overflow: 'hidden',
            position: 'relative',
            height: 300,
            width: '100%',
            display: 'flex',
            '& img': {
                position: 'absolute',
                top: 0,
                left: 0,
                width: '100%',
                zIndex: -1,
                height: '100%',
                objectFit: 'cover',
            },
            '& .content': {
                paddingTop: 70,
                paddingLeft: 50,
                paddingRight: 50,
                marginBottom: 15,
                '& .title': {
                    height: 120,
                },
                '& .title span': {
                    display: 'block',
                    maxWidth: 270,
                    marginBottom: 20,
                },
                '& .button-custom': {
                    height: 30,
                    borderRadius: '3px !important',
                }
            },
        }
    },
});

export default styles;