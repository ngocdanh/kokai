import { Box, Grid } from '@mui/material';
import { WithStyles, withStyles } from '@mui/styles';
import { COLORS } from '@constants/colors';
import imageAssets from '@constants/imageAssets';
import ButtonCustom from '@helpers/ButtonCustom';
import TextCustom from '@helpers/TextCustom';
import { useTranslation } from 'next-i18next';
import styles from './styles';
import { IBanner } from './types';
import { isEmpty, range, uniqueId } from 'lodash';
import Link from 'next/link';
import SkeletonCustom from '@helpers/SkeletonCustom';
import { useSelector } from 'react-redux';


const BannerComponent = (props: IBanner & WithStyles<typeof styles>) => {
    const {
        classes,
    } = props;
    const {isLoading} = useSelector((state: any) => state.global);

    const { t } = useTranslation();

    const dataBanner = [
        {
            image: imageAssets.banner1,
            title: t('bannerTitle1'),
            url: '#',
        },
        {
            image: imageAssets.banner2,
            title: t('bannerTitle2'),
            url: '#',
        },
        {
            image: imageAssets.banner3,
            title: t('bannerTitle3'),
            url: '#',
        },
    ];

    return (
        <Box className={classes.bannerComponent}
            paddingBottom={8}>
            <Grid container
                columnSpacing={2}>
                {!isEmpty(dataBanner) && !isLoading && dataBanner.map(banner => (
                    <Grid item
                        key={uniqueId()}
                        xs={12}
                        marginBottom={3}
                        md={4}>
                        <Box className="banner-item">
                            <img src={banner.image}
                                alt="" />
                            <Box className="content"
                                display='block'>
                                <TextCustom h4
                                    className='title w-full'
                                    color={COLORS.textHeading}>
                                    <span>{banner.title}</span>
                                </TextCustom>
                                <ButtonCustom iconEnd={'fi-rs-arrow-small-right ml-2'}>
                                    <Link href={banner.url}>
                                        <a>
                                            {t('shopNow')}
                                        </a>
                                    </Link>
                                </ButtonCustom>
                            </Box>
                        </Box>
                    </Grid>
                ))}
                {isLoading && range(3).map(r => (
                    <Grid item
                        key={uniqueId()}
                        xs={12}
                        marginBottom={3}
                        md={4}>
                        <SkeletonCustom 
                            imgHeight={300} 
                            disableText
                        />
                    </Grid>
                ))}
            </Grid>
        </Box>
    );
};

export default withStyles(styles)(BannerComponent);