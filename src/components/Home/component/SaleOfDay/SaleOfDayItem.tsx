import React from 'react';
import { Box, Rating } from '@mui/material';
import { WithStyles, withStyles } from '@mui/styles';
import { COLORS } from '@constants/colors';
import ButtonCustom from '@helpers/ButtonCustom';
import TextCustom from '@helpers/TextCustom';
import { isEmpty } from 'lodash';
import Link from 'next/link';
import { useTranslation } from 'react-i18next';
import styles from './styles';
import { convertGroupPrice, convertNumber } from '@common/convertData';
import Countdown from '@helpers/Countdown';
import { IDataItem } from '@components/Home/types';
import { routeRoot } from '@common/config';
import { toast } from 'react-toastify';

const SaleOfDayItemComponent = (props: IDataItem & WithStyles<typeof styles>) => {
    const {
        classes,
        dataItem,
    } = props;

    const { t } = useTranslation();

    const dayInMs = 20 * 24 * 60 * 60 * 1000;
    const nowInMs = new Date().getTime();
    const countDownTime = nowInMs + dayInMs;
    const {price, sale_price} = convertGroupPrice(dataItem);
    const handleAddToCart = (e: React.MouseEvent<HTMLButtonElement>) => {
        e.stopPropagation();
        toast.success(String(t('addedToCart')), {toastId: dataItem?.slug});
    };

    return (
        <Box className={classes.saleOfDayComponent}>
            <Box className="sale-item">
                <Box className='sale-image'>
                    {!isEmpty(dataItem?.image) && (
                        <>
                            <img src={dataItem?.image.original} />
                        </>
                    )}
                </Box>
                <Box className="sale-content-wrap">
                    <Box className='flex justify-center items-center sale-countdown'>
                        <Countdown targetDate={countDownTime}/>
                    </Box>
                    <Box className='sale-content'>
                        <Link
                            href={`${routeRoot.product}/${dataItem?.slug}`}>
                            <a className='block w-fit sale-content-heading'>
                                <TextCustom headingSM
                                    className='mb-1 mt-1 title'
                                    color={COLORS.textHeading}>{dataItem?.name}</TextCustom>
                            </a>
                        </Link>
                        <Box className="flex rating items-center mb-3">
                            <Rating
                                name="simple-controlled"
                                value={dataItem?.vote}
                                precision={0.5}
                                max={5}
                                size={'small'}
                                readOnly
                            />
                            <TextCustom textXS
                                className='!ml-3'
                                color={COLORS.textBody}>
                                { convertNumber(dataItem?.vote)}
                            </TextCustom>
                        </Box>
                        <Box className="flex items-center justify-between">
                            <Box className="group-price">
                                <span className="new-price">
                                    {sale_price || price}
                                </span>
                                <span className="old-price">
                                    {sale_price && price}
                                </span>

                            </Box>
                            <ButtonCustom
                                onClick={handleAddToCart}
                                iconEnd={'fi-rs-plus-small'}
                                className='!h-7 !rounded button-add'
                            >
                                {t('add')}
                            </ButtonCustom>
                        </Box>
                    </Box>
                </Box>
            </Box>
        </Box>
    );
};

export default withStyles(styles)(SaleOfDayItemComponent);