import { withStyles, WithStyles } from '@mui/styles';
import styles from './styles';
import { isEmpty, range, uniqueId } from 'lodash';
import { Box, Grid } from '@mui/material';
import TextCustom from '@helpers/TextCustom';
import { COLORS } from '@constants/colors';
import { useTranslation } from 'react-i18next';
import SaleOfDayItem from './SaleOfDayItem';
import { ISaleOfDay } from './types';
import SkeletonCustom from '@helpers/SkeletonCustom';
import { useSelector } from 'react-redux';

const SaleOfDayComponent = (props: ISaleOfDay & WithStyles<typeof styles>) => {

    const { classes, dataSaleOfDay } = props;

    const { t } = useTranslation();
    const {isLoading} = useSelector((state: any) => state.global);

    return (
        <Box className={classes.saleOfDayComponent}>
            <Box className="flex justify-between items-end mb-12">
                <TextCustom h3 
                    color={COLORS.textHeading}>{t('dealsOfTheDay')}</TextCustom>
                <Box className="flex items-center cursor-pointer">
                    <TextCustom
                        textMedium
                        color={COLORS.textHeading}
                    >
                        {t('allDeals')}
                    </TextCustom>
                    <Box className="flex items-center justify-center">
                        <i className='fi-rs-angle-small-right ml-2 h-3'></i>
                    </Box>
                </Box>
            </Box>
            <Box className="list-sale">
                <Grid container
                    spacing={2}>
                    {!isEmpty(dataSaleOfDay) && dataSaleOfDay?.map(data => (
                        <Grid item
                            xs={6}
                            lg={3}
                            md={6}
                            key={uniqueId()}>
                            <SaleOfDayItem dataItem={data}/>
                        </Grid>
                    ))}
                    {isLoading && range(4).map(r => (
                        <Grid item
                            xs={6}
                            lg={3}
                            md={6}
                            key={uniqueId()}>
                            <SkeletonCustom
                                imgHeight={300}
                            />
                        </Grid>
                    ))}
                </Grid>
            </Box>
        </Box>
    );
};

export default withStyles(styles)(SaleOfDayComponent); 