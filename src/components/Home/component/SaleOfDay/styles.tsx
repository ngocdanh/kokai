import { Theme } from '@mui/material';
import { createStyles } from '@mui/styles';
import { } from '@mui/styles/';
import { COLORS } from '@constants/colors';

const styles = (theme: Theme) => createStyles({
    root: {
        paddingTop: 33,
        paddingBottom: 33,
    },
   
    saleOfDayComponent: {
        '& .list-sale': {
            '& .sale-item': {
                position: 'relative',

                '& .sale-image': {

                    '& img': {
                        cursor: 'pointer',
                        width: '100%',
                        height: '100%',
                        borderRadius: '15px',
                        boxShadow: '20px 20px 40px rgba(24, 24, 24, 0.07)',
                        objectFit: 'cover',
                    },
                },
                '& .sale-content-wrap': {
                    position: 'relative',
                    marginTop: '-120px',
                    transition: 'all .4s ease',
                    '& .sale-content': {
                        maxWidth: '92%',
                        padding: '25px 30px',
                        backgroundColor: COLORS.white,
                        borderRadius: '10px',
                        boxShadow: '5px 5px 15px rgb(0 0 0 / 5%)',
                        margin: '0 auto',

                        '& .title': {
                            transition: 'all .3s ease',

                            '&:hover': {
                                color: COLORS.brand1,
                            }
                        },

                        '& .sale-content-heading': {
                            minHeight: '40px',
                        },

                        '& .button-add': {
                            background: '#DEF9EC !important',
                            color: `${COLORS.brand1} !important`,
                            fontWeight: '700 !important',
                            '& i': {
                                marginLeft: 8,
                                fontWeight: '700 !important',
                            },
                            '&:hover': {
                                backgroundColor: `${COLORS.warring} !important`,
                                color: `${COLORS.white} !important`,
                            }
                        },

                        '& .new-price': {
                            marginRight: '8px',
                            color: COLORS.brand1,
                            fontSize: '20px',
                            fontWeight: '700',
                        },

                        '& .old-price': {
                            color: COLORS.textBody,
                            fontSize: '12px',
                            fontWeight: '500',
                            textDecoration: 'line-through',
                        },
                    },

                    '& .sale-countdown': {
                        width: '100%',
                        textAlign: 'center',
                        position: 'relative',
                        top: '-15px',
                    },
                },

                '&:hover .sale-content-wrap': {
                    transform: 'translateY(-8px)',
                    
                },
            }
        },
    },
});

export default styles;