import { IBreadcrumbItem } from '@helpers/BreadcrumbCustom/types';

export type IRegister = {
    fistName: string,
    lastName: string, 
    phone: string, 
    email: string, 
    fullName?: string, 
    password: string,
    confirm_password: string,
};


export interface IProps {
    breadcrumbs: IBreadcrumbItem[],
    loadingSubmit: boolean,
    onSubmit: (data: IRegister) => void,
    onChangeGoogleRecaptcha: (token: string| null) => void,
    isRobot: boolean,
}
export type Inputs = {
    fistName: string,
    lastName: string, 
    phone: string, 
    email: string, 
    fullName?: string, 
    password: string,
    confirm_password: string,
};

export type IDataCreateUser = {
    fistName: string,
    lastName: string, 
    phone: string, 
    email: string, 
    password: string,
};

