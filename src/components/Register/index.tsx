import { COLORS } from '@constants/colors';
import BreadcrumbCustom from '@helpers/BreadcrumbCustom';
import TextCustom from '@helpers/TextCustom';
import { Box, Button } from '@mui/material';
import { WithStyles, withStyles } from '@mui/styles';
import { useTranslation } from 'next-i18next';
import styles from './styles';
import { IProps, IRegister } from './types';
import { useForm} from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import React from 'react';
import TextFieldCustom from '@helpers/TextFieldCustom';
import {schemaRegister} from '@constants/Validate';
// @ts-ignore
import ReCAPTCHA from 'react-google-recaptcha';
import { isEmpty } from 'lodash';


const RegisterComponent = (props: IProps & WithStyles<typeof styles>) => {
    const {
        classes,
        breadcrumbs,
        loadingSubmit,
        onSubmit,
        onChangeGoogleRecaptcha,
        isRobot,
    } = props;

    const googleRecaptchaKey = process.env.GOOGLE_RECAPTCHA_KEY || '';
    
    const { handleSubmit, formState: { errors }, control } = useForm<IRegister>({
        resolver: yupResolver(schemaRegister)
    });
    const { t } = useTranslation();
    return (
        <React.Fragment>
            <BreadcrumbCustom list={breadcrumbs} />
            <Box className={classes.root}>
                <Box className={classes.LoginForm}>
                    <Box className="register-header">
                        <Box className='text-title'><TextCustom className="title"
                            h2
                            color={' #253D4E'}>
                            {t('createAccount')}
                        </TextCustom>
                        <TextCustom 
                            textXS
                            color={COLORS.textMuted}
                            className="sub-title !mt-5 !mb-6">
                            {t('createAccountSubtitle')}
                        </TextCustom></Box>
                        <Box className='space-title'></Box>
                    </Box>
                    <Box className="register-main">
                        <Box component={'form'}
                            onSubmit={handleSubmit(onSubmit)}
                            className={classes.TextFieldCss}
                            autoComplete="off">
                            <TextFieldCustom control={control}
                                errors={errors.fistName}
                                name='fistName' 
                                label={t('fistName')}/>
                            <TextFieldCustom control={control}
                                errors={errors.lastName}
                                name='lastName' 
                                label={t('lastName')}/>
                            <TextFieldCustom control={control}
                                errors={errors.email}
                                name='email' 
                                label={t('email')}/>
                            <TextFieldCustom control={control}
                                errors={errors.phone}
                                name='phone' 
                                label={t('phone')}/>
                            <TextFieldCustom control={control}
                                errors={errors.password}
                                name='password' 
                                label={t('password')}
                                type='password'/>
                            <TextFieldCustom control={control}
                                errors={errors.confirm_password}
                                name='confirm_password' 
                                label={t('confirmPassword')}
                                type='password'/>
                            <Box className="security-code">
                                <ReCAPTCHA
                                    sitekey={googleRecaptchaKey}
                                    onChange={onChangeGoogleRecaptcha}
                                />
                            </Box>
                            <Box>
                                <Button variant="contained"
                                    type='submit'
                                    className='btn-submit'
                                    disabled={!isEmpty(errors) ||  isRobot}>
                                    {t('submitRegister')}
                                </Button></Box>
                        </Box>
                        {/* <Box className="register-sign-with">
                            <Box className='register-border'>
                                <Button variant="contained"
                                    className='social-sign sign-facebook text-white'>
                                    <ImgFacebook className='social-icon'/>
                                    {t('continueFacebook')}
                                </Button>
                                <Button variant="contained"
                                    className='social-sign sign-google'>
                                    <ImgGoogle className='social-icon'/>
                                    {t('continueGoogle')}
                                </Button>
                            </Box>
                        </Box> */}
                    </Box>
                </Box>
            </Box>
        </React.Fragment>
    );
};
export default withStyles(styles)(RegisterComponent);