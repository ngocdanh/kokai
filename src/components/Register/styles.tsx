import { COLORS } from '@constants/colors';
import { Theme } from '@mui/material';
import { createStyles } from '@mui/styles';
import { } from '@mui/styles/';

const styles = (theme: Theme) => createStyles({
    root: {
        paddingTop: 33,
        paddingBottom: 33,
    },

    LoginForm: {
        display:'flex',
        flexDirection: 'column',
        alignItems: 'center',
        gap: 18,
        '& .register-header': {
            width: '70%',
            display: 'flex',
            justifyContent: 'space-between',
            '& .text-title': {
                width: '48%',
            },
            '& .space-title': {
                width: '45%',
            },
        },
        '& .register-main': {
            width: '70%',
            display: 'flex',
            alignItems: 'flex-start',
            justifyContent: 'space-between',

            '& .register-sign-with': {
                width: '45%',
                '& .register-border': {
                    width:'90%',
                    display: 'flex',
                    flexDirection:'column',
                    justifyContent: 'flex-end',
                    alignItems: 'center',
                    gap: 20,
                    border: `1px solid ${COLORS.borderColor}`,
                    padding: '40px 20px',
                    borderRadius: 15,
                },
                '& .social-sign': {
                    borderRadius: 15,
                    width: '90%',
                    textAlign: 'center',
                    padding: 10,
                    lineHeight: 2,
                    color: COLORS.white,
                    fontSize: 18,
                    fontWeight: 600,
                    display: 'flex',
                    justifyContent: 'flex-start',
                    alignItems: 'center',
                    gap: 15,
                    boxShadow: 'none'
                },
                '& .sign-facebook': {
                    backgroundColor: '#1877F2',
                },
                '& .sign-google': {
                    backgroundColor: '#F2F3F4',
                    color: COLORS.textBody,
                }

            }
        },
    },

    TextFieldCss: {
        width: '50%',
        display: 'flex',
        gap: 20,
        flexDirection: 'column',
        '& .security-code': {
            display: 'flex',
            gap: 20,

            '& .security-number':{
                height: '100%',
                padding: '20px 20px',
                borderRadius: 10,
            }
        },
        '& .terms-policy': {
            display: 'flex',
            justifyContent: 'space-between',
            alignItems: 'center',
        },
        '& .btn-submit': {
            minWidth: 180,
            width: 'fit-content',
            borderRadius: 10,   
            fontWeight: 700,
            lineHeight: 3,
            padding: '10px 30px',
            backgroundColor: '#253D4E',
            color: COLORS.white,
        },
    }
});

export default styles;