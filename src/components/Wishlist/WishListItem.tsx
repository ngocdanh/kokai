import { routeRoot } from '@common/config';
import { convertGroupPrice } from '@common/convertData';
import { COLORS } from '@constants/colors';
import TextCustom from '@helpers/TextCustom';
import { Box, TableCell, TableRow, Rating, Button } from '@mui/material';
import { WithStyles, withStyles } from '@mui/styles';
import { isEmpty } from 'lodash';
import { useTranslation } from 'next-i18next';
import styles from './styles';
import { IWisLisItem } from './types';
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { useCallback } from 'react';
import Link from 'next/link';

const WishlistItem = (props: IWisLisItem & WithStyles<typeof styles>) => {
    const {
        classes,
        wishListItemData,
        handleDelete
    } = props;

    const {t} = useTranslation();
    const { price , sale_price } = convertGroupPrice(wishListItemData);

    const notify = useCallback((message: string) => toast.success(message), []);
    const handleAddToCard = () => {
        notify(t('addedToCart'));
    };

    return (
        <TableRow
            className={classes.WishListItem}
            hover={true}
            key={wishListItemData.name}
            sx={{ border: 0 }}
        >
            <TableCell>
                <Box className={ classes.NameItem }>
                    <Box className="image">
                        {!isEmpty(wishListItemData?.image) && (
                            <img src={wishListItemData?.image.original} />
                        )}
                    </Box>
                    <Box className="content pl-5 pr-2">
                        <Link href={`${routeRoot.product}/${wishListItemData?.slug}`}>
                            <a>
                                <TextCustom headingSM
                                    className='mb-1 mt-1 title'
                                    color={COLORS.textHeading}>{wishListItemData?.name}</TextCustom>
                            </a>
                        </Link>
                        <Box className="flex rating mt-4">
                            <Rating
                                name="simple-controlled"
                                value={wishListItemData?.vote}
                                precision={0.5}
                                max={5}
                                size={'small'}
                                readOnly
                            />
                            <TextCustom textXS
                                className='ml-1'
                                color={COLORS.textBody}>
                                {`(${wishListItemData?.vote?.toFixed(1)})`}
                            </TextCustom>
                        </Box>
                    </Box>
                </Box>
            </TableCell>
            <TableCell align="center">
                <Box className="box-price !flex !items-center justify-center">
                    <TextCustom className='!mr-2'
                        h4
                        color={COLORS.brand1}>{ sale_price || price }</TextCustom>  
                    {wishListItemData?.price && !!wishListItemData?.sale_price && <TextCustom
                        textXS
                        className='line-through'
                        color={COLORS.textBody}>{`$${wishListItemData.price}`}</TextCustom>}

                </Box>
            </TableCell>
            <TableCell align="center">{wishListItemData?.in_stock===1 &&
                <Box><TextCustom className="in-stock">
                    {t('inStock')}
                </TextCustom></Box>
            }</TableCell>
            <TableCell align="center"><Button variant="contained"
                className="btn-card"
                onClick={() => handleAddToCard()}
            >
                {t('addToCart')} 
            </Button>
            </TableCell>
            <TableCell align="center">
                <i className="fi fi-rr-cross-circle delete"
                    onClick={(e: React.MouseEvent<HTMLElement>) => 
                        handleDelete(wishListItemData?._id)}></i>                
            </TableCell>
        </TableRow>
    );
};

export default withStyles(styles)(WishlistItem);