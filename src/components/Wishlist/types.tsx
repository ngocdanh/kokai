import { typeProductItem } from '@common/types';
import { IBreadcrumbItem } from '@helpers/BreadcrumbCustom/types';

export interface IProps {
    breadcrumb: IBreadcrumbItem[],
    wishListData: typeProductItem[],
    total: number, 
    handleDelete: ( id: string) => void;
}

export interface IWisLisItem {
    wishListItemData: typeProductItem,
    handleDelete: ( id: string) => void;
}

export type WishListData = {
    
}