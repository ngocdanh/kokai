import { COLORS } from '@constants/colors';
import { Theme } from '@mui/material';
import { createStyles } from '@mui/styles';
import { } from '@mui/styles/';

const styles = (theme: Theme) => createStyles({
    root: {
       
    },

    WishList: {

        '& .header-table': {
            backgroundColor: '#F5F5F6',
        },
        '& .MuiTableRow-root th:first-child': {
            borderRadius: '15px 0 0 15px',
        },
          
        '& .MuiTableRow-root th:last-child': {
            borderRadius: '0 15px 15px 0',
        },

        '& .MuiTableCell-root': {
            border: 0,
        },

        '& .table-container': {
            display: 'flex',
            margin : '40px auto',
            width: '85%',
        },


        '& .product-quantity': {
            color: COLORS.brand1
        }
    },

    WishListItem: {

        '& .delete': {
            fontSize: 20,
            cursor: 'pointer',
            color: COLORS.textMuted

        },

        '& .in-stock': {
            fontWeight: 600,
            color: COLORS.brand1,
            backgroundColor: '#DEF9EC',
            padding: '4px 10px',
            borderRadius: 5,
        },

        '& .out-stock': {
            fontWeight: 600,
            color: COLORS.white,
            backgroundColor: COLORS.textHeading2,
            padding: '4px 10px',
            borderRadius: 5,
        },

        '& .btn-card': {
            borderRadius: 4,
            backgroundColor: `${COLORS.brand1}`,
            padding: '15px 22px',
            color: COLORS.white,
            fontSize: 16,
            fontWeight: 600,
            lineHeight: '20px',
            boxShadow: 'none',
            '&:hover': {
                backgroundColor: `${COLORS.brand2}`,
            }
        },

    },

    NameItem: {
        display: 'inline-flex',
        alignItems: 'center',
        padding: 0,
        overflow: 'hidden',
        '& .image': {
            display: 'flex',
            justifyContent: 'center',
            width: 100,  
            height: 100,
            '& img': {
                width: '100%',
                height: '100%',
                objectFit: 'contain',
                transition: 'all .3s ease',
            },
      
        },
        '& .content': {
            width: 'fit-content',
            '& .title': {
                transition: 'all .3s ease',
                '&:hover': {
                    color: `${COLORS.brand1} !important`,
                }
            },
            '& .rating': {
                '& i': {
                    color: '#CDCDCD',
                },
                '& .active': {
                    color: COLORS.warring,
                },
                '& .not-active': {
                    color: '#CDCDCD',
                }
            },
        },
    }
});

export default styles;