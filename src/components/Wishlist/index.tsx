import { typeProductItem } from '@common/types';
import { COLORS } from '@constants/colors';
import BreadcrumbCustom from '@helpers/BreadcrumbCustom';
import TextCustom from '@helpers/TextCustom';
import { Box, Paper, Table, TableBody, TableCell, TableContainer, TableHead, TableRow } from '@mui/material';
import { WithStyles, withStyles } from '@mui/styles';
import { uniqueId, isEmpty } from 'lodash';
import { useTranslation } from 'next-i18next';
import styles from './styles';
import { IProps } from './types';
import WishlistItem from './WishListItem';

const WishlistComponent = (props: IProps & WithStyles<typeof styles>) => {
    const {
        classes,
        breadcrumb,
        wishListData,
        handleDelete,
        total
    } = props;
    
    const {t} = useTranslation();

    return (
        <Box className={classes.WishList}>
            <BreadcrumbCustom list={breadcrumb}/>
            <Box className='flex flex-col'>
                <TextCustom h2
                    color={COLORS.textHeading}>
                    {t('wishList')}
                </TextCustom>
                {total? 
                    <TextCustom h6
                        color={COLORS.textBody}
                        dangerouslySetInnerHTML={{__html: t('totalProductWishList', {
                            total: total
                        })}}>
                    </TextCustom>
                    :
                    null
                }
                
            </Box>
            {total? 
                <TableContainer component={Paper}
                    elevation={0}
                    className={'table-container'}>
                    <Table sx={{ minWidth: 650 }}
                        aria-label="wish-list-table">
                        <TableHead
                            className={'header-table'}>
                            <TableRow className='header-row'>
                                <TableCell align='center'>
                                    <TextCustom h6
                                        color={COLORS.textHeading2}>{t('products')}</TextCustom>
                                </TableCell>
                                <TableCell align="center">
                                    <TextCustom h6
                                        color={COLORS.textHeading2}>{t('unitPrice')}</TextCustom>
                                </TableCell>
                                <TableCell align="center"><TextCustom h6
                                    color={COLORS.textHeading2}>{t('stockStatus')}</TextCustom></TableCell>
                                <TableCell align="center"><TextCustom h6
                                    color={COLORS.textHeading2}>{t('action')}</TextCustom></TableCell>
                                <TableCell align="center"><TextCustom h6
                                    color={COLORS.textHeading2}>{t('remove')}</TextCustom></TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {
                                !isEmpty(wishListData) &&  wishListData?.map((item: typeProductItem) => (
                                    <WishlistItem wishListItemData={item}
                                        handleDelete={handleDelete}
                                        key={uniqueId()}/>
                                ))
                            }
                        </TableBody>
                    </Table>
                </TableContainer>
                :
                <TextCustom h3
                    className={'!pt-4 !pb-4 pl-10'}
                    color={COLORS.black}>
                    {t('noProducts')}
                </TextCustom>
                
            }
            
        </Box>
    );
};

export default withStyles(styles)(WishlistComponent);