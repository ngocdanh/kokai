import { Theme } from '@mui/material';
import { createStyles } from '@mui/styles';
import { COLORS } from '@constants/colors';
import imageAssets from '@constants/imageAssets';

const styles = (theme: Theme) => createStyles({
    root: {
        '& .category-filter': {
            backgroundColor: `${COLORS.white}`,
            border: `1px solid ${COLORS.borderColor}`,
            borderRadius: '15px',
            padding: '30px',
            boxShadow: '5px 5px 15px rgb(0 0 0 / 5%)',
            marginBottom: '30px',
            background: `url(${imageAssets.filterWidgetBg}) no-repeat 100% 100%`,
            backgroundSize: '150px',


            '& .category-filter-heading': {
                paddingBottom: '10px',
                borderBottom: '1px solid #ececec',
                marginBottom: '30px',
            },

            '& .price-filter': {
                margin: '10px 0 20px 0',
            },

            '& .type-filter-wrapper': {
                marginBottom: '20px',
            },

            '& .brand-filter-wrapper': {
                marginBottom: '20px',
            },

            '& .filter-button': {
                padding: '0 30px',
                backgroundColor: '#DEF9EC !important',
                color: `${COLORS.brand1} !important`,
                fontWeight: '700',

                '&:hover': {
                    backgroundColor: `${COLORS.warring} !important`,
                    color: `${COLORS.white} !important`,
                },
            },
        },
    }
});

export default styles;