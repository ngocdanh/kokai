import { COLORS } from '@constants/colors';
import TextCustom from '@helpers/TextCustom';
import { Box, FormGroup, FormControlLabel, Checkbox } from '@mui/material';
import { WithStyles, withStyles } from '@mui/styles';
import styles from './styles';
import { ICategoryFilter } from './types';
import ButtonCustom from '@helpers/ButtonCustom';
import { useTranslation } from 'next-i18next';
import RangeSlider from '@helpers/RangeSlider';


const CategoryFilterComponent = (props: ICategoryFilter & WithStyles<typeof styles>) => {
    const {
        classes,
    } = props;

    const {t} = useTranslation();

    return (
        <Box className={classes.root}>
            <Box className="category-filter">
                <TextCustom h4
                    color={COLORS.textHeading}
                    className="category-filter-heading w-full"
                >
                    {t('filterItems')}
                </TextCustom>
                <Box className="price-filter-wrapper">
                    <Box className="price-range flex items-center">
                        <TextCustom textMedium
                            color={COLORS.textMuted}>{t('priceRange')}: </TextCustom>
                        <Box className="detail-price ml-3">
                            <TextCustom h5
                                color={COLORS.brand1}
                                className="!mr-2">$16 -</TextCustom>
                            <TextCustom h5
                                color={COLORS.brand1}> $173</TextCustom>
                        </Box>
                    </Box>
                    <Box className="price-filter">
                        <RangeSlider/>
                    </Box>
                </Box>
                <Box className="type-filter-wrapper">
                    <TextCustom textMedium
                        color={COLORS.textMuted}>{t('type')}: </TextCustom>
                    <Box className="type-filter">
                        <FormGroup>
                            <FormControlLabel control={<Checkbox/>}
                                label="Furniture"></FormControlLabel>
                            <FormControlLabel control={<Checkbox/>}
                                label="Clothing"></FormControlLabel>
                            <FormControlLabel control={<Checkbox/>}
                                label="Bags"></FormControlLabel>
                            <FormControlLabel control={<Checkbox/>}
                                label="Grocery"></FormControlLabel>
                            <FormControlLabel control={<Checkbox/>}
                                label="Smoothie"></FormControlLabel>
                        </FormGroup>
                    </Box>
                </Box>
                <Box className="brand-filter-wrapper">
                    <TextCustom textMedium
                        color={COLORS.textMuted}>{t('brand')}:</TextCustom>
                    <Box className="type-filter">
                        <FormGroup>
                            <FormControlLabel control={<Checkbox/>}
                                label="Cobblestone"></FormControlLabel>
                            <FormControlLabel control={<Checkbox/>}
                                label="McVitie's"></FormControlLabel>
                            <FormControlLabel control={<Checkbox/>}
                                label="Tastykake"></FormControlLabel>
                            <FormControlLabel control={<Checkbox/>}
                                label="Warburtons"></FormControlLabel>
                            <FormControlLabel control={<Checkbox/>}
                                label="Wonder Bread"></FormControlLabel>
                        </FormGroup>
                    </Box>
                </Box>
                <ButtonCustom cartSmall 
                    iconStart="fi-rs-filter"
                    className='!h-11 filter-button'>Filter</ButtonCustom>
            </Box>
        </Box>
    );           
};

export default withStyles(styles)(CategoryFilterComponent);