import { Theme } from '@mui/material';
import { createStyles } from '@mui/styles';
import { COLORS } from '@constants/colors';
import imageAssets from '@constants/imageAssets';

const styles = (theme: Theme) => createStyles({
    root: {
        '& .category-archive-header': {
            background: `url(${imageAssets.headerBackground}) no-repeat 50%`,
            borderRadius: '20px',
            height: '235px',
            backgroundSize: 'cover',
            padding: '65px 80px',
            marginTop: '33px',
        },

        '& .category-body': {
            margin: '50px 0',
        },
        
        '& .css-12wnr2w-MuiButtonBase-root-MuiCheckbox-root.Mui-checked': {
            color: `${COLORS.brand1}`,
        },
    },
});

export default styles;