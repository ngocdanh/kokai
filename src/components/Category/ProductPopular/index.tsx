import { Box } from '@mui/material';
import { WithStyles, withStyles } from '@mui/styles';
import { COLORS } from '@constants/colors';
import TextCustom from '@helpers/TextCustom';
import { isEmpty, uniqueId } from 'lodash';
import styles from './styles';
import { IProduct } from './types';
import { useTranslation } from 'next-i18next';
import ProductItemSimple from '@helpers/ProductItem/Simple';


const ProductPopularComponent = (props: IProduct & WithStyles<typeof styles>) => {
    const {
        classes,
        dataListProduct,
    } = props;
    
    const { t } = useTranslation();

    return (
        <Box className={classes.productComponent}>
            <Box className="product-container">
                <Box className="flex items-end justify-between mb-13 box-title">
                    <TextCustom h4
                        color={COLORS.textHeading}
                        className="product-heading w-full">{t('popularItems')}</TextCustom>
                </Box>
                <Box className="list-product">
                    <Box>
                        {!isEmpty(dataListProduct) && dataListProduct?.map(o => (
                            <Box
                                key={uniqueId()}>
                                <ProductItemSimple dataItem={o} />
                            </Box>
                        ))}
                    </Box>
                </Box>
            </Box>
        </Box>
    );
};

export default withStyles(styles)(ProductPopularComponent);