import { typeProductItem } from '@common/types';


export interface IProduct {
    dataListProduct?: typeProductItem[],
}

export interface IDataItem {
    dataItem?: typeProductItem
}
