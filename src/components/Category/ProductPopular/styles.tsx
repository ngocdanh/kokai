import { Theme } from '@mui/material';
import { createStyles } from '@mui/styles';
import { COLORS } from '@constants/colors';

const styles = (theme: Theme) => createStyles({
    productComponent: {
        '& .product-container': {
            backgroundColor: `${COLORS.white}`,
            border: `1px solid ${COLORS.borderColor}`,
            borderRadius: '15px',
            padding: '30px',
            boxShadow: '5px 5px 15px rgb(0 0 0 / 5%)',
            marginBottom: '30px',

            '& .product-heading': {
                paddingBottom: '10px',
                borderBottom: '1px solid #ececec',
            }
        },
    },

    productItem: {

        '& .product': {
            display: 'flex',
            alignItems: 'center',

            '& .content': {
                width: '55%',

                '& .title': {
                    transition: 'all .3s ease',
                    fontSize: '16px',

                    '&:hover': {
                        color: COLORS.brand1,
                    }
                },
            },

            '& .image': {
                width: '45%',
                height: 'auto',
                marginRight: '20px',
  
                '& img': {
                    width: '100%',
                    height: '100%',
                    borderRadius: '10px',
                },
            },
        },
          
        
    },
});

export default styles;