import { COLORS } from '@constants/colors';
import TextCustom from '@helpers/TextCustom';
import { Box } from '@mui/material';
import styles from './styles';
import { WithStyles, withStyles } from '@mui/styles';
import { ICategoryBanner } from './types';
import ButtonCustom from '@helpers/ButtonCustom';
import imageAssets from '@constants/imageAssets';


const CategoryBannerComponent = (props: ICategoryBanner & WithStyles<typeof styles>) => {
    const {
        classes,
    } = props;

    return (
        <Box className={classes.root}>
            <Box className="category-ads">
                <Box className="category-ads-img">
                    <img src={imageAssets.banner11}
                        alt="Category Banner" />
                </Box>
                <Box className="category-ads-content flex flex-col items-start">
                    <Box>
                        <TextCustom h5
                            color={COLORS.brand2}
                            className="!mb-5">Oganic</TextCustom>
                        <TextCustom h2
                            color={COLORS.textHeading}
                            className="!mb-40">Save 17% on Oganic Juice</TextCustom>
                    </Box>
                    <ButtonCustom cartSmall
                        iconEnd="fi-rs-arrow-small-right"
                        className="!mt-8">Shop Now</ButtonCustom>
                </Box>
            </Box>
        </Box>
        
    );           
};

export default withStyles(styles)(CategoryBannerComponent);