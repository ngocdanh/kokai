import { Theme } from '@mui/material';
import { createStyles } from '@mui/styles';

const styles = (theme: Theme) => createStyles({
    root: {
        '& .category-ads': {
            borderRadius: '15px',
            position: 'relative',

            '& .category-ads-img': {
                height: '500px',

                '& img': {
                    width: '100%',
                    height: '100%',
                    borderRadius: '15px',
                },
            },

            '& .category-ads-content': {
                position: 'absolute',
                top: '40px',
                padding: '0 70px',
            },
        },
    }
});

export default styles;