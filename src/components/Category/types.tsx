import { typeProductItem } from '@common/types';
import { IFilterCategory } from '@pages/category/[slug]';

export interface IDataItem {
    dataItem?: typeProductItem
}

export interface IProps {
    categoryDetail: {
        name: string,
        slug: string,
        icon: string,
        promotional_sliders: {
            original: string,
            thumbnail: string,
        }[]
    },
    dataListProduct: typeProductItem[],
    listProductOfCategory: typeProductItem[],
    filters: IFilterCategory,
    handleChangeFilters: (name: 'page' | 'limit' | 'sortKey', val: any) => void,
    totalProductOfCategory: number,
    pageCountProductOfCategory: number,
    handleChangePage: (e: React.ChangeEvent<unknown>, value: number) => void,
}
