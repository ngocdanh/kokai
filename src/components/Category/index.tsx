import { COLORS } from '@constants/colors';
import TextCustom from '@helpers/TextCustom';
import { Box, Grid } from '@mui/material';
import { WithStyles, withStyles } from '@mui/styles';
import { useTranslation } from 'next-i18next';
import styles from './styles';
import { IProps } from './types';
import Filter from './Filter';
import Banner from './Banner';
import ProductPopular from './ProductPopular';
import ProductList from './ProductList';
import { uniqueId } from 'lodash';
import { IBreadcrumbItem } from '@helpers/BreadcrumbCustom/types';
import { routeRoot } from '@common/config';
import BreadcrumbCustom from '@helpers/BreadcrumbCustom';
import { useDispatch, useSelector } from 'react-redux';
import { userInfo } from '@common/shared';
import { wishlistAction } from '@redux/actions';
import { toast } from 'react-toastify';
import * as React from 'react';

const CategoryComponent = (props: IProps & WithStyles<typeof styles>) => {
    const {
        classes,
        categoryDetail,
        dataListProduct,
        listProductOfCategory,
        handleChangeFilters,
        totalProductOfCategory,
        pageCountProductOfCategory,
        handleChangePage,
        filters,
    } = props;
    const dispatch = useDispatch();
    const {t} = useTranslation();
    const {isSuccess: isSuccessWishlist, message:messageWishlist} = useSelector((state: any) => state.wishlist);
    const {
        name: titleCategory,
        slug: slugCategory
    } = categoryDetail;
    const breadcrumbs: IBreadcrumbItem[] = [
        {
            label: t('home'),
            url: routeRoot.home,
        },
        {
            label: titleCategory,
        },
    ];

    React.useEffect(() => {
        if(isSuccessWishlist && messageWishlist) {
            toast.success(messageWishlist, {
                toastId: uniqueId()
            });
        }
    }, [messageWishlist]);

    const handleAddToWishlist = (e: React.MouseEvent<HTMLElement>,id: string) => {
        e.stopPropagation();
        const productObjId = id;
        if(!userInfo?.userObjId && !productObjId) return;
        const set = {
            product: productObjId,
            userObjId: userInfo?.userObjId
        };
        dispatch(wishlistAction.fetchAddWishlist(set));
    };


    return (
        <Box className={classes.root}>
            <Box className="category-archive-header"
                key={uniqueId()}>
                <TextCustom h1
                    color={COLORS.textHeading}
                    className="!mb-4"
                >
                    {titleCategory}
                </TextCustom>
                <BreadcrumbCustom list={breadcrumbs} />
            </Box>
            <Box className="category-body">
                <Grid container
                    spacing={2}>
                    <Grid item
                        xs={3.5}
                        className="category-left">
                        <Filter/>
                        <ProductPopular 
                            dataListProduct={dataListProduct}
                        />
                        <Banner/>
                    </Grid>
                    <Grid item
                        xs={8.5}
                        className="category-right">
                        <ProductList 
                            handleChangePage={handleChangePage}
                            totalProductOfCategory={totalProductOfCategory}
                            pageCountProductOfCategory={pageCountProductOfCategory}
                            listProduct={listProductOfCategory}
                            handleChangeFilters={handleChangeFilters}
                            filters={filters}
                            handleAddToWishlist={handleAddToWishlist}
                        />
                    </Grid>
                </Grid>
            </Box>
        </Box>
    );
};

export default withStyles(styles)(CategoryComponent);