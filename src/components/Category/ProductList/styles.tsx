import { Theme } from '@mui/material';
import { createStyles } from '@mui/styles';
import { COLORS } from '@constants/colors';

const styles = (theme: Theme) => createStyles({
    productDetailList: {
        '& .product-list': {
            paddingBottom: '30px',
        },

        '& .product-tools': {
            backgroundColor: `${COLORS.bgMuted}`,
            padding: '26px 20px',
            borderRadius: '15px',
            marginBottom: '30px',

            '& .product-total': {
                fontSize: '16px',
                '& .product-quantity': { 
                    color: COLORS.brand1,
                },
            },
           

            '& .product-sort-area': {
                display: 'flex',

                '& .sort-by-cover': {

                    '& .sort-by-product-wrap': {
                        backgroundColor: 'transparent',
                        color: `${COLORS.textBody}`,
                        cursor: 'pointer',

                        '& select': {
                            outline: 'none',
                            border: 'none',
                            backgroundColor: 'transparent',
                        },
                    },
                },

                '& .sort-by-dropdown-wrap': {
                    position: 'relative',

                    '& option': {
                        width: '30px',  
                    },
                }
            },
        },
    },
    
    productDetailItem: {
        border: `1px solid ${COLORS.borderColor}`,
        borderRadius: '15px',
        overflow: 'auto',
        position: 'relative',
        transition: 'all .3s ease',
        padding: 19,

        '& .image': {
            position: 'relative',
            width: '100%',
            paddingBottom: 20,
            minHeight: 200,

            '& img': {
                transition: 'all .6s ease',
                width: '100%',
                objectFit: 'contain',
                height: 200,
            },
            [theme.breakpoints.down('md')]: {
                '& img': {
                    height: 300,
                }
            },
        },
        '& .content': {
            '& .category-link': {
                color: `${COLORS.textBody}`,
                transition: 'all .3s ease',

                '&:hover': {
                    color: COLORS.brand1,
                }
            },

            '& .category': {
                transition: 'all .3s ease',

                '&:hover': {
                    color: COLORS.brand1
                }
            },
            '& .title': {
                transition: 'all .3s ease',
                minHeight: 40,

                '&:hover': {
                    color: `${COLORS.brand1} !important`,
                }
            },
            '& .rating': {
                '& i': {
                    color: '#CDCDCD',
                    width: 12,
                    height: 12,
                },
                '& .active': {
                    color: COLORS.warring,
                },
                '& .not-active': {
                    color: '#CDCDCD',
                }
            },

            '& .new-price': {
                marginRight: 8,
                color: `${COLORS.brand1}`,
                fontSize: 20,
                fontWeight: 700,
            },

            '& .old-price': {
                color: `${COLORS.textBody}`,
                fontSize: 12,
                fontWeight: 500,
                textDecoration: 'line-through',
            },

            '& .button-add': {
                background: '#DEF9EC !important',
                color: `${COLORS.brand1} !important`,
                fontWeight: '700 !important',
                '& i': {
                    marginLeft: 8,
                    fontWeight: '700 !important',
                },
                '&:hover': {
                    backgroundColor: `${COLORS.warring} !important`,
                    color: `${COLORS.white} !important`,
                }
            }
        },
        '&:hover': {
            border: `1px solid ${COLORS.brand1}`,
            boxShadow: '20px 20px 40px rgba(24, 24, 24, 0.07)',
        }

    }
});

export default styles;