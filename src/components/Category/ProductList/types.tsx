import { typeProductItem } from '@common/types';
import { IFilterCategory } from '@pages/category/[slug]';
import * as React from 'react';


export interface IProductList {
    listProduct?: typeProductItem[],
    filters: IFilterCategory,
    handleChangeFilters: (name: 'page' | 'limit' | 'sortKey', val: any) => void,
    totalProductOfCategory: number,
    pageCountProductOfCategory: number,
    handleChangePage: (e: React.ChangeEvent<unknown>, value: number) => void,
    handleAddToWishlist?: (e: React.MouseEvent<HTMLElement>,_id: string) => void,
}

export interface IDataItem {
    dataItem?: typeProductItem
}