import { Box, Grid } from '@mui/material';
import { WithStyles, withStyles } from '@mui/styles';
import { COLORS } from '@constants/colors';
import styles from './styles';
import { IProductList } from './types';
import { useTranslation } from 'next-i18next';
import TextCustom from '@helpers/TextCustom';
import { isEmpty, uniqueId } from 'lodash';
import PaginationCustom from '@helpers/PaginationCustom';
import ProductItemDefault from '@helpers/ProductItem/Default';


const DetailProductComponent = (props: IProductList & WithStyles<typeof styles>) => {
    const {
        classes,
        listProduct,
        filters,
        handleChangeFilters,
        totalProductOfCategory,
        pageCountProductOfCategory,
        handleChangePage,
        handleAddToWishlist,
    } = props;
    
    const { t } = useTranslation();

    const listSortBy = [
        {
            label: t('sortKey.featured'),
            value: 'featured',
        },
        {
            label: t('sortKey.selling'),
            value: 'selling',
        },
        {
            label: t('sortKey.trending'),
            value: 'trending',
        },
        {
            label: t('sortKey.recently'),
            value: 'recently',
        },
        {
            label: t('sortKey.rated'),
            value: 'rated',
        },
    ];

    const listLimit = [10, 20, 50, 100];
    return (
        <Box className={classes.productDetailList}>
            <Box className="product-tools flex items-center justify-between">
                <TextCustom headingSM
                    color={COLORS.textBody}
                    dangerouslySetInnerHTML={{__html: t('showTotalProductOfCategory', {
                        total: totalProductOfCategory
                    })}}
                    className="product-total" 
                />
                <Box className="product-sort-area">
                    <Box className="sort-by-cover">
                        <Box className="sort-by-product-wrap flex items-center">
                            <Box className="sort-by flex items-center">
                                <i className="fi-rs-apps sort-icon mr-2"></i>
                                <TextCustom textMedium
                                    color={COLORS.textBody}>
                                    {t('show')}:
                                </TextCustom>
                            </Box>
                            <Box className="sort-by-dropdown-wrap">
                                <select value={Number(filters.limit)} 
                                    onChange={(e: React.ChangeEvent<HTMLSelectElement>) => {
                                        handleChangeFilters('limit', Number(e.target.value));
                                    }}
                                >
                                    {!isEmpty(listLimit) && listLimit?.map(limitItem => (
                                        <option key={uniqueId()}
                                            value={limitItem}>{limitItem}</option>
                                    ))}
                                </select>
                            </Box>
                        </Box>
                    </Box>
                    <Box className="sort-by-cover ml-6">
                        <Box className="sort-by-product-wrap flex items-center">
                            <Box className="sort-by flex items-center">
                                <i className="fi-rs-apps-sort sort-icon mr-2"></i>
                                <TextCustom textMedium
                                    color={COLORS.textBody}>
                                    {t('sortBy')}:
                                </TextCustom>
                            </Box>
                            <Box className="sort-by-dropdown-wrap">
                                <select onChange={(e: React.ChangeEvent<HTMLSelectElement>) => {
                                    handleChangeFilters('sortKey', e.target.value);
                                }}
                                value={filters.sortKey}
                                >
                                    {!isEmpty(listSortBy) && listSortBy?.map(o => (
                                        <option key={uniqueId()}
                                            value={o.value}>{o.label}</option>
                                    ))}
                                </select>
                            </Box>
                        </Box>
                    </Box>

                </Box>
            </Box>
            <Box className="product-list">
                <Grid container
                    spacing={3}>{!isEmpty(listProduct) && listProduct?.map((data: any) => (
                        <Grid item
                            xs={6}
                            lg={3}
                            md={4}
                            key={uniqueId()}>
                            <ProductItemDefault 
                                dataItem={data} 
                                handleAddToWishlist={handleAddToWishlist}/>
                        </Grid>
                    ))}</Grid>
            </Box>
            {!isEmpty(listProduct) && (
                <PaginationCustom 
                    page={filters.page}
                    handleChange={handleChangePage}
                    count={pageCountProductOfCategory}/>
            )}
        </Box>
    );
};

export default withStyles(styles)(DetailProductComponent);