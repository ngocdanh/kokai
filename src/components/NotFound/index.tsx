import { Box } from '@mui/material';
import { WithStyles, withStyles } from '@mui/styles';
import { useTranslation } from 'next-i18next';
import styles from './styles';
import { IProps } from './types';
import TextCustom from '@helpers/TextCustom';
import { COLORS } from '@constants/colors';
import ButtonCustom from '@helpers/ButtonCustom';
import Link from 'next/link';
import { routeRoot } from '../../common/config';


const NotFoundComponent = (props: IProps & WithStyles<typeof styles>) => {
    const {
        classes,
    } = props;

    const {t} = useTranslation();
 
    return (
        <Box className={classes.root}>
            <Box className="flex justify-center">
                <img 
                    src='/assets/imgs/page/page-404.png'
                    alt="" />
            </Box>
            <Box className="py-5">
                <TextCustom display2
                    center
                    color={COLORS.textHeading}>{t('pageNotFound')}</TextCustom>
            </Box>
            <Box className='pb-10'>
                <TextCustom textMedium
                    color={COLORS.textBody}
                    center
                    dangerouslySetInnerHTML={{__html: t('descNotFound')}} />
            </Box>
            <Box className="flex justify-center">
                <ButtonCustom iconStart='fi fi-rr-home'>
                    <Link href={routeRoot.home}>
                        <a>{t('backToGoHome')}</a>
                    </Link>
                </ButtonCustom>
            </Box>
        </Box>
    );
};

export default withStyles(styles)(NotFoundComponent);