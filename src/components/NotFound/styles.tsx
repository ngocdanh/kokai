import { COLORS } from '@constants/colors';
import { Theme } from '@mui/material';
import { createStyles } from '@mui/styles';
import { } from '@mui/styles/';

const styles = (theme: Theme) => createStyles({
    root: {
        paddingTop: 100,
        paddingBottom: 100,
        '& img': {
            maxWidth: 500,
            width: '100%',
        },
        '& .text-custom a': {
            color: COLORS.brand1
        },
        '& .button-custom': {
            height: 47,
            paddingLeft: 15,
            paddingRight: 15,
        }
    },
});

export default styles;