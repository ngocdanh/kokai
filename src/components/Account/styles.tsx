import { COLORS } from '@constants/colors';
import { Theme } from '@mui/material';
import { createStyles } from '@mui/styles';
import { } from '@mui/styles/';

const styles = (theme: Theme) => createStyles({
    root: {
       
    },

    Account: {

        margin: '60px auto',

        '& 	.MuiTab-root': {
            borderRadius: '10px',
            border: `1px solid ${COLORS.borderColor}`,
            marginBottom: '10px',
            padding: '18px 30px',
            justifyContent: 'flex-start',
        },
        '& 	.MuiTabs-indicator': {
            display: 'none',
        },

        '& .MuiTab-labelIcon': {
            lineHeight: '20px',
            fontWeight: 700,
            fontSize: '16px',
            
            '& i': {
                fontSize: '20px',
            }
        },

        '& .Mui-selected': {
            color: `${COLORS.white} !important`,
            backgroundColor: COLORS.brand1,
        }

    },

});

export default styles;