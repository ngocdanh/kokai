import { Box, Grid, Tab, Tabs } from '@mui/material';
import { WithStyles, withStyles } from '@mui/styles';
import { useTranslation } from 'next-i18next';
import styles from './styles';
import { IProps, listTabs } from './types';
import BreadcrumbCustom from '@helpers/BreadcrumbCustom';
import { useState } from 'react';
import OrderListComponent from './OrderList';
import DashboardComponent from './Dashboard';
import TrackOrderComponent from './TrackOrder';
import MyAddressComponent from './MyAddress';
import AccountDetail from './AccountDetail';
import { useRouter } from 'next/router';


const AccountComponent = (props: IProps & WithStyles<typeof styles>) => {
    const {
        classes,
        breadcrumbs,
        yourOrderData,
        onSubmitTracking,
        onSubmitAccountDetail,
        AccountDetailData,
    } = props;

    const {t} = useTranslation();
    const router = useRouter();

    const [value, setValue] = useState(1);

    const handleChange = (event: React.SyntheticEvent , newValue: number) => {
        if(newValue == 6){
            router.push('/login');
        }
        setValue(newValue);
    };


    const listTabs: listTabs = [
        {
            title: t('dashbroad'),
            icon: <i className="fi fi-rs-settings !mt-1"></i>,
            value: 1,
        },
        {
            title: t('orderList'),
            icon: <i className="fi fi-rs-settings !mt-1"></i>,
            value: 2,
        },
        {
            title: t('trackYourOrder'),
            icon: <i className="fi fi-rr-shopping-bag !mt-1"></i>,
            value: 3,
        },
        {
            title: t('myAddress'),
            icon: <i className="fi fi-rs-marker !mt-1"></i>,
            value: 4,
        },
        {
            title: t('accountDetail'),
            icon: <i className="fi fi-rs-user !mt-1"></i>,
            value: 5,
        },
        {
            title: t('logOut'),
            icon: <i className="fi fi-rs-sign-out !mt-1"></i>,
            value: 6,
        },
    ];


 
    return (
        <Box className={classes.root}>
            <BreadcrumbCustom list={breadcrumbs}/>

            <Grid
                container
                justifyContent={'center'}
                spacing={0}
                columns={{ md: 12 }}
                className={classes.Account}
            >
                <Grid
                    item
                    md={2.5}
                >
                    <Tabs value={value}
                        onChange={handleChange}
                        orientation="vertical"
                    >
                        {
                            listTabs.map((item: any, index: number) => {
                                return <Tab className={'tab-item'}
                                    icon={item.icon}
                                    value={item.value}
                                    iconPosition={'start'}
                                    label={item.title}
                                    key={index}/>;
                                
                            })
                        }
                    </Tabs>
                </Grid>
                <Grid
                    item
                    md={7}
                    style={{marginLeft: '70px'}}
                >
                    <DashboardComponent value={value}
                        index={1}/>
                    <OrderListComponent value={value}
                        index={2}
                        dataOrder={yourOrderData}/>
                    <TrackOrderComponent value={value}
                        onSubmitTracking={onSubmitTracking}
                        index={3}/>
                    <MyAddressComponent value={value}
                        index={4}/>
                    <AccountDetail value={value}
                        AccountDetailData={AccountDetailData}
                        onSubmitAccountDetail={onSubmitAccountDetail}
                        index={5}/>
                    
                </Grid>

            </Grid>
        </Box>
    );
};

export default withStyles(styles)(AccountComponent);