import { Box, Table, Paper, TableBody, TableCell, TableContainer, TableHead, TableRow } from '@mui/material';
import { WithStyles, withStyles } from '@mui/styles';
import { useTranslation } from 'next-i18next';
import styles from './styles';
import { IProps } from './types';
import TextCustom from '@helpers/TextCustom';
import { COLORS } from '@constants/colors';
import { isEmpty, uniqueId } from 'lodash';
import { IOderData } from '@common/types';
import OrderListItem from './OrderListItem';


const OrerListComponent = (props: IProps & WithStyles<typeof styles>) => {
    const {
        classes,
        value,
        index,
        dataOrder,
    } = props;

    const {t} = useTranslation();
 
    return (
        <Box component={'div'}
            role="tabpanel"
            hidden={value !== index}
            id={`simple-tabpanel-${index}`}
            aria-labelledby={`simple-tab-${index}`}
            className={classes.root}>
            <TextCustom h3>
                {t('yourOrder')}
            </TextCustom>
            <Box>
                {dataOrder?
                    <TableContainer component={Paper}
                        elevation={0}
                        className={'table-container flex flex-col'}>
                        <Table sx={{ minWidth: 650 }}
                            aria-label="wish-list-table">
                            <TableHead
                                className={'header-table'}>
                                <TableRow className='header-row'>
                                    <TableCell align='center'>
                                        <TextCustom h6
                                            color={COLORS.textHeading2}>{t('orderID')}</TextCustom>
                                    </TableCell>
                                    <TableCell align="center">
                                        <TextCustom h6
                                            color={COLORS.textHeading2}>{t('date')}</TextCustom>
                                    </TableCell>
                                    <TableCell align="center"><TextCustom h6
                                        color={COLORS.textHeading2}>{t('status')}</TextCustom>
                                    </TableCell>
                                    <TableCell align="center"><TextCustom h6
                                        color={COLORS.textHeading2}>{t('total')}</TextCustom>
                                    </TableCell>
                                    <TableCell align="center"><TextCustom h6
                                        color={COLORS.textHeading2}>{t('action')}</TextCustom>
                                    </TableCell>
                                </TableRow>
                            </TableHead>
                            <TableBody>
                                {
                                    !isEmpty(dataOrder) &&  dataOrder?.map((item: IOderData) => (
                                        <OrderListItem OrderListItemData={item}
                                            key={uniqueId()}/>
                                    ))
                                }
                            </TableBody>
                        </Table>
                    </TableContainer>
                
                    :
                    <TextCustom h3
                        className={'!pt-4 !pb-4 pl-10'}
                        color={COLORS.black}>
                        {t('noProducts')}
                    </TextCustom>
                }
            </Box>
        </Box>
    );
};

export default withStyles(styles)(OrerListComponent);