import { COLORS } from '@constants/colors';
import { Theme } from '@mui/material';
import { createStyles } from '@mui/styles';
import { } from '@mui/styles/';

const styles = (theme: Theme) => createStyles({
    root: {

        '& .header-table': {
            backgroundColor: '#F5F5F6',
        },

        '& .MuiTableRow-root th:first-child': {
            borderRadius: '15px 0 0 15px',
        },
          
        '& .MuiTableRow-root th:last-child': {
            borderRadius: '0 15px 15px 0',
        },

        '& .MuiTableCell-root': {
            border: 0,
        },

        '& .table-container': {
            margin : '40px auto',
        },

       
    },

    OrderListItem: {
        
    },

    Modal: {

        position: 'absolute',
        top: '50%',
        left: '50%',
        transform: 'translate(-50%, -50%)',
        width: 'fit-content',
        backgroundColor: COLORS.white,
        borderRadius: '12px',
        padding: '15px 25px',

        '& .confirm-delete': {
            marginTop: '10px',
            float: 'right',

            '& button': {
                height: 'fit-content'
            }
        }

    }
});

export default styles;