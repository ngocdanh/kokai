import { IOderData } from '@common/types';

export interface IProps {

    value: number;
    index: number;
    dataOrder: IOderData[];
  
}

export interface IOrderListItem {

    OrderListItemData: IOderData,
  
}