import { COLORS } from '@constants/colors';
import TextCustom from '@helpers/TextCustom';
import { Box, Button, Modal, TableCell, TableRow, Typography } from '@mui/material';
import { WithStyles, withStyles } from '@mui/styles';
import { isEmpty } from 'lodash';
import { useTranslation } from 'next-i18next';
import styles from './styles';
import 'react-toastify/dist/ReactToastify.css';
import { IOrderListItem } from './types';
import { useState } from 'react';
import ButtonCustom from '@helpers/ButtonCustom';

const OrderListItem = (props: IOrderListItem & WithStyles<typeof styles>) => {
    const {
        classes,
        OrderListItemData,
    } = props;

    const {t} = useTranslation();

    const [openModal, setOpenModal] = useState<boolean>(false);
    const handleOpenModal = () => setOpenModal(true);
    const handleCloseModal = () => {
        setOpenModal(false);
    };

    return (
        <TableRow
            className={classes.OrderListItem}
            hover={true}
            key={OrderListItemData.orderId}
            sx={{ border: 0 }}
        >
            <TableCell style={{padding: '0 25px'}}>
                <Box className={'order-id'}>
                    {!isEmpty(OrderListItemData?.orderId) && (
                        <TextCustom textMedium
                            color={COLORS.textBody}>
                            {OrderListItemData.orderId}
                        </TextCustom>)
                    }
                </Box>
            </TableCell>
            <TableCell align="center">
                <TextCustom textMedium
                    color={COLORS.textBody}
                    className="box-price !flex !items-center justify-center">
                    {OrderListItemData.date}

                </TextCustom>
            </TableCell>
            <TableCell align="center">
                {(OrderListItemData?.status === 1)?
                    <TextCustom textMedium
                        className="completed"
                        color={COLORS.brand1} >
                        Completed
                    </TextCustom>
                    :
                    <TextCustom className={'processing'}
                        color={COLORS.warring}>
                        Processing
                    </TextCustom>
                }
            </TableCell>
            <TableCell align="center">
                <TextCustom textMedium
                    color={COLORS.textBody}>
                    {OrderListItemData.total}
                </TextCustom>
            </TableCell>
            <TableCell align="center">
                <Button
                    style={{backgroundColor: `${COLORS.white} !important`}}
                    onClick={handleOpenModal}>
                    <TextCustom textMedium>
                        View    
                    </TextCustom>   
                </Button>            
            </TableCell>
            <Modal
                open={openModal}
                onClose={handleCloseModal}
                aria-labelledby="modal-modal-title"
                aria-describedby="modal-modal-description"
            >
                <Box className={classes.Modal}>
                    <Typography id="modal-modal-title"
                        variant="h6"
                        component="h2">
                        Không có gì để xem đâu
                    </Typography>
                    <Typography id="modal-modal-description"
                        sx={{ mt: 2 }}>
                        Chức năng này làm gì có đâu mà xem
                    </Typography>
                    <Box className={'confirm-delete flex gap-3'}>
                        <ButtonCustom
                            onClick={handleCloseModal}>
                            Ừ
                        </ButtonCustom>
                    </Box>
                </Box>
                
            </Modal>
        </TableRow>
    );
};

export default withStyles(styles)(OrderListItem);