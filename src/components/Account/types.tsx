import { IOderData } from '@common/types';
import { IBreadcrumbItem } from '@helpers/BreadcrumbCustom/types';
import { ITrackingOrder } from './TrackOrder/types';
import { IAccountDetail } from './AccountDetail/types';

export interface IProps {

    breadcrumbs: IBreadcrumbItem[],
    yourOrderData: IOderData[],
    onSubmitTracking: (data: ITrackingOrder) => void
    onSubmitAccountDetail: (data: IAccountDetail) => void
    AccountDetailData: IAccountDetail,
  
}

export type listTabs = {
    title: string;
    icon: any;
    value: number;
}[]
