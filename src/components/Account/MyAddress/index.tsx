import { Box } from '@mui/material';
import { WithStyles, withStyles } from '@mui/styles';
import { useTranslation } from 'next-i18next';
import styles from './styles';
import { IProps } from './types';
import TextCustom from '@helpers/TextCustom';


const MyAddressComponent = (props: IProps & WithStyles<typeof styles>) => {
    const {
        classes,
        value,
        index,
    } = props;

    const {t} = useTranslation();
 
    return (
        <Box component={'div'}
            role="tabpanel"
            hidden={value !== index}
            id={`simple-tabpanel-${index}`}
            aria-labelledby={`simple-tab-${index}`}
            className={classes.root}>
            <Box className={'flex justify-around'}>
                <Box
                    className={'billing-address'}>
                    <TextCustom h2>
                        {t('billingAddress')}
                    </TextCustom>
                    <Box component={'ul'}
                        className='mt-5'>
                        <li>Tam kỳ 6, Đại Tự,</li>
                        <li>Yên Lạc,</li>
                        <li>Vĩnh Phúc,</li>
                        <li>Việt Nam,</li>
                        <li>Trái Đất</li>
                    </Box>
                </Box>
                <Box
                    className={'shipping-address'}>
                    <TextCustom h4>
                        {t('shippingAddress')}
                    </TextCustom>
                    <Box component={'ul'}
                        className='mt-6'>
                        <li>Tam kỳ 6, Đại Tự,</li>
                        <li>Yên Lạc,</li>
                        <li>Vĩnh Phúc,</li>
                        <li>Việt Nam,</li>
                        <li>Trái Đất</li>
                    </Box>
                </Box>
            </Box>
            
        </Box>
    );
};

export default withStyles(styles)(MyAddressComponent);