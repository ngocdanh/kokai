import { COLORS } from '@constants/colors';
import { Theme } from '@mui/material';
import { createStyles } from '@mui/styles';
import { } from '@mui/styles/';

const styles = (theme: Theme) => createStyles({
    root: {
       

        '& li': {
            color: COLORS.textBody,
            fontSize: '16px',
            fontWeight: 500,
            lineHeight: '26px'
        }
    },
});

export default styles;