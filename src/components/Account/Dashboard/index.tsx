import { Box } from '@mui/material';
import { WithStyles, withStyles } from '@mui/styles';
import { useTranslation } from 'next-i18next';
import styles from './styles';
import { IProps } from './types';
import TextCustom from '@helpers/TextCustom';
import { COLORS } from '@constants/colors';


const DashbroadComponent = (props: IProps & WithStyles<typeof styles>) => {
    const {
        classes,
        value,
        index,
    } = props;

    const {t} = useTranslation();
 
    return (
        <Box component={'div'}
            role="tabpanel"
            hidden={value !== index}
            id={`simple-tabpanel-${index}`}
            aria-labelledby={`simple-tab-${index}`}
            className={classes.root}>
            <TextCustom h3>
                {t('hello')} Danh
            </TextCustom>
            <Box>
                <TextCustom
                    h6
                    style={{lineHeight: '26px', marginTop: '20px'}}
                    color={COLORS.textBody}
                >
                    {t('dashBroardSubtitle')}
                </TextCustom>
            </Box>
        </Box>
    );
};

export default withStyles(styles)(DashbroadComponent);