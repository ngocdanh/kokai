

export interface IProps {

    value: number;
    index: number;
    onSubmitAccountDetail: (data: IAccountDetail) => void;
    AccountDetailData: IAccountDetail;
  
}

export type IAccountDetail = {
    fistName: string,
    lastName: string,
    email: string,
    phone: string,
}