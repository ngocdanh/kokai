import { Avatar, Box, Button } from '@mui/material';
import { WithStyles, withStyles } from '@mui/styles';
import { useTranslation } from 'next-i18next';
import styles from './styles';
import { IAccountDetail, IProps } from './types';
import TextCustom from '@helpers/TextCustom';
import { COLORS } from '@constants/colors';
import Link from 'next/link';
import { schemaAccountDetail } from '@constants/Validate';
import { useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import TextFieldCustom from '@helpers/TextFieldCustom';
import imageAssets from '@constants/imageAssets';


const AccountDetailComponent = (props: IProps & WithStyles<typeof styles>) => {
    const {
        classes,
        value,
        index,
        AccountDetailData,
        onSubmitAccountDetail
    } = props;

    const {t} = useTranslation();

    const { handleSubmit, formState: { errors }, control } = useForm<IAccountDetail>({
        resolver: yupResolver(schemaAccountDetail)
    });
 
    return (
        <Box component={'div'}
            role="tabpanel"
            hidden={value !== index}
            id={`simple-tabpanel-${index}`}
            aria-labelledby={`simple-tab-${index}`}
            className={classes.root}>
            <Box>
                <TextCustom h4
                    className='!block !mb-3'>
                    {t('accountDetail')}
                </TextCustom>
                <TextCustom textMedium
                    color={COLORS.textBody}>
                    {t('haveAccount')}
                        
                    <Link href={'/login'}>
                        <TextCustom textMedium
                            className='cursor-pointer'
                            color={COLORS.brand1}>
                            {t('loginInstead')}
                        </TextCustom>
                    </Link>
                </TextCustom>
            </Box>
            <Box>
                <Box>
                    <Box component={'form'}
                        onSubmit={handleSubmit(onSubmitAccountDetail)}
                        className={classes.TextFieldCss}
                        autoComplete="off">
                        <Box className='flex items-center'>
                            <Avatar alt="Avatar"
                                src={imageAssets.thumbnail11}
                                style={{width: '200px', height: '200px'}} />
                            <Box style={{width: '100%', marginLeft: '15px'}}>
                                <TextFieldCustom control={control}
                                    className='!mb-5'
                                    value={AccountDetailData.fistName}
                                    errors={errors.fistName}
                                    name='fistName' 
                                    label={t('fistName')}/>
                                <TextFieldCustom control={control}
                                    value={AccountDetailData.lastName}
                                    errors={errors.lastName}
                                    name='lastName' 
                                    label={t('lastName')}/>
                            </Box>
                        </Box>
                        <TextFieldCustom control={control}
                            value={AccountDetailData.email}
                            errors={errors.email}
                            name='email' 
                            label={t('email')}/>
                        <TextFieldCustom control={control}
                            value={AccountDetailData.phone}
                            errors={errors.phone}
                            name='phone' 
                            label={t('phone')}/>

                        <Button variant="contained"
                            type='submit'
                            className='btn-submit'
                            disabled={false}
                        >
                            {t('saveChange')}
                        </Button>
                    </Box>
                </Box>
            </Box>

        </Box>
    );
};

export default withStyles(styles)(AccountDetailComponent);