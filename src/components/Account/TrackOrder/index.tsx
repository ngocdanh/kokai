import { Box, Button } from '@mui/material';
import { WithStyles, withStyles } from '@mui/styles';
import { useTranslation } from 'next-i18next';
import styles from './styles';
import { IProps, ITrackingOrder } from './types';
import TextCustom from '@helpers/TextCustom';
import { COLORS } from '@constants/colors';
import { useForm } from 'react-hook-form';
import { schemaTrackingOrder } from '@constants/Validate';
import { yupResolver } from '@hookform/resolvers/yup';
import TextFieldCustom from '@helpers/TextFieldCustom';


const TrackOrderComponent = (props: IProps & WithStyles<typeof styles>) => {
    const {
        classes,
        value,
        index,
        onSubmitTracking

    } = props;

    const {t} = useTranslation();

    const { handleSubmit, formState: { errors }, control } = useForm<ITrackingOrder>({
        resolver: yupResolver(schemaTrackingOrder)
    });
 
    return (
        <Box component={'div'}
            role="tabpanel"
            hidden={value !== index}
            id={`simple-tabpanel-${index}`}
            aria-labelledby={`simple-tab-${index}`}
            className={classes.root}>
            <TextCustom h3
                style={{display: 'block'}}>
                {t('orderTracking')}
            </TextCustom>
            <TextCustom h6
                className='!mt-6 !mb-6'
                style={{lineHeight: '20px'}}
                color={COLORS.textBody}
            >
                {t('orderTrackingsubtitle')}
            </TextCustom>
            <Box component={'form'}
                onSubmit={handleSubmit(onSubmitTracking)}
                className={classes.TextFieldCss}
                autoComplete="off">
                <TextFieldCustom control={control}
                    errors={errors.orderId}
                    name={'orderId'}
                    label={t('foundOder')}
                />
                <TextFieldCustom control={control}
                    
                    errors={errors.email}
                    name='email' 
                    label={t('billingEmail')}
                />

                <Box><Button variant="contained"
                    type='submit'
                    className='btn-submit'
                    disabled={false}
                >
                    {t('track')}
                </Button></Box>
            </Box>

        </Box>
    );
};

export default withStyles(styles)(TrackOrderComponent);