

export interface IProps {

    value: number;
    index: number;
    onSubmitTracking: (data: ITrackingOrder) => void
    
  
}

export type ITrackingOrder = {
    orderId: string,
    email: string
}