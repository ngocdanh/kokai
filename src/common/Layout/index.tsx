import FooterComponent from '@common/Footer';
import HeaderComponent from '@common/Header';
import Loading from '@helpers/Loading';
import Row from '@helpers/Row';
import useMenu from '@lib/hook/useMenu';
import { useQueryMenu } from '@lib/query/menu';
import { getStaticProps } from '@lib/query/ssr/layouts.srr';
import { withStyles, WithStyles } from '@mui/styles';
import { isEmpty } from 'lodash';
import Head from 'next/head';
import { useTranslation } from 'next-i18next';
import styles from './styles';
import { ILayout } from './types';
import { ScrollToTopIcon } from '@helpers/icons/scroll-to-top';
import { useWindowScrollPositions } from '@lib/hook/useWindowScrollPositions';
import { useRouter } from 'next/router';

export { getStaticProps };

const Layout = (props: ILayout & WithStyles<typeof styles>) => {
    const {
        children,
        titlePage,
    } = props;
    const router = useRouter();
    const { t, i18n } = useTranslation();
    const {scrollY} = useWindowScrollPositions();
    const onScrollToTop = () => {
        window.scroll({top: 0, left: 0, behavior: 'smooth' });
    };

    const handleChangeLanguage = () => {
        const { pathname, asPath, query, locale, locales } = router;        
        router.push(
            { pathname, query },
            asPath, 
            { locale: locales?.find(o => o!== locale) }
        );
    };

    const {
        open: openCategory,
        openMenu: handleOpenMenuCategory, 
        closeMenu: handleCloseMenuCategory,
    } = useMenu();
    
    const {listMenu} = useQueryMenu();
    
    const menuHeader = !isEmpty(listMenu) ? listMenu.filter(o => o?.name === 'menu-header') :[];
    const menuFooter = !isEmpty(listMenu) ? listMenu.filter(o => o?.name === 'menu-footer') :[];
    const menuCategory = !isEmpty(listMenu) ? listMenu.filter(o => o?.name === 'menu-category') :[];
    return (
        <>
            <Head>
                <title>{t(titlePage || 'titlePage')}</title>
                <meta name="description"
                    content="KoKai - Quality genuine shopping site" />
                <meta name="viewport"
                    content="width=device-width, initial-scale=1.0" />
                <meta httpEquiv="Content-Security-Policy"
                    content="upgrade-insecure-requests"/> 
                <link rel="icon"
                    href="/favicon.ico" />
            </Head>
            <Loading>
                <main>
                    <Row>
                        <HeaderComponent 
                            openCategory={openCategory}
                            menuHeader={menuHeader}
                            menuCategory={menuCategory}
                            handleOpenMenuCategory={handleOpenMenuCategory}
                            handleCloseMenuCategory={handleCloseMenuCategory}
                            handleChangeLanguage={handleChangeLanguage}
                        />
                        {children}
                        <FooterComponent
                            menuFooter={menuFooter}
                        />
                    </Row>
                    {scrollY > 300 && (
                        <ScrollToTopIcon onClick={onScrollToTop}
                            className='scroll-to-top'/>
                    )}
                </main>
            </Loading>
        </>
    );
};

export default withStyles(styles)(Layout);