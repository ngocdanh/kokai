import { ReactNode } from 'react';

export interface ILayout {
    children?: ReactNode,
    menuHeader?: [],
    titlePage?: string,
}