import { Box, Grid } from '@mui/material';
import { WithStyles, withStyles } from '@mui/styles';
import clsx from 'clsx';
import { COLORS } from '@constants/colors';
import imageAssets, { ImageLogo } from '@constants/imageAssets';
import TextCustom from '@helpers/TextCustom';
import { isEmpty, range, uniqueId } from 'lodash';
import Image from 'next/image';
import Link from 'next/link';
import { useTranslation } from 'next-i18next';
import styles from './styles';
import { IFooter } from './types';
import ButtonCustom from '@helpers/ButtonCustom';
import { useRouter } from 'next/router';
import SkeletonCustom from '@helpers/SkeletonCustom';
import { useSelector } from 'react-redux';


const FooterComponent = (props: IFooter & WithStyles<typeof styles>) => {
    const {
        classes,
        menuFooter,
    } = props;
    const router = useRouter();
    const lang = router.locale as 'vi' | 'en';
    const { t } = useTranslation();
    const { banner12, banner13 } = imageAssets;
    const {isLoading} = useSelector((state: any) => state.global);

    const listInfo = [
        {
            name: t('address'),
            content: t('addressCompany'),
            icon: 'fi-rs-marker',
            key: 'address',
        },
        {
            name: t('callUs'),
            content: '+84 985 832 533',
            icon: 'fi-rs-headset',
            key: 'tel',
        },
        {
            name: t('email'),
            content: 'danhnotes@gmail.com',
            icon: 'fi-rs-paper-plane',
            key: 'email',
        },
        {
            name: t('hours'),
            content: '08:00 - 18:00, T2-T6',
            icon: 'fi-rs-time-fast',
            key: 'hours'
        },
    ];

    return (
        <Box className={classes.root}>
            <Box className={classes.banner}>
                <img className="img"
                    src={banner12} />
                <img src={banner13}
                    className="absolute z-50 right-0 !w-[500px]  bottom-0  !object-contain" />
                <Box className="content">
                    <TextCustom h2
                        className='mb-8 title !pr-[130px]'
                        color={COLORS.textHeading}>{t('stayHome')}</TextCustom>
                    <TextCustom h4
                        className='description'
                        color={COLORS.textBody}>{t('startDaily')}</TextCustom>
                    <Box className="box-input">
                        <i className="text-body fi fi-rs-paper-plane w-6 h-6"></i>
                        <input type="text"
                            placeholder={t('enterYourEmail')} />
                        <ButtonCustom className="!text-[19px]" >{t('subscribe')}</ButtonCustom>
                    </Box>
                </Box>
            </Box>
            <Grid container
                columnSpacing={1}
                className={classes.gridLayout}>
                <Grid item
                    lg={3}
                    md={12}>
                    <ImageLogo />
                    <Box marginBottom={3}
                        marginTop={2}>
                        <TextCustom textMedium
                            color={COLORS.textHeading}>{t('awesomeTheme')}</TextCustom>
                    </Box>
                    <Box className="list-info">
                        {!isEmpty(listInfo) && listInfo?.map(o => {
                            return (
                                <Box className='info !flex items-start  !mb-2'
                                    key={o.key}>
                                    <i className={clsx(o.icon, 'mr-2 w-4 h-4 text-brand')}></i>
                                    <TextCustom textMedium
                                        className='!flex'
                                        color={COLORS.textHeading}>
                                        <b className='mr-2 name'>{`${o.name}:`}</b>
                                        <Box component={
                                            (o.key !== 'tel' && o.key !== 'email') ? 'div' : 'a'
                                        }
                                        href={
                                            (o.key === 'tel' || o.key === 'email') ? (
                                                o.key === 'email' ? `mailto:${o.content}`
                                                    : `tel:${o.content}`
                                            ) : ''
                                        }
                                        >
                                            {o.content}
                                        </Box>
                                    </TextCustom>
                                </Box>
                            );
                        })}
                    </Box>
                </Grid>
                {!isEmpty(menuFooter) && menuFooter?.map(footer => (
                    <Grid key={uniqueId('menu-footer')}
                        item
                        lg={7 / 4}
                        md={3}
                        sm={6}
                        paddingBottom={5}
                        paddingTop={1}
                        xs={12}>
                        <Box>
                            <TextCustom
                                h5
                                className='!mb-7'
                                color={COLORS.textHeading}
                            >{footer?.title![lang]}</TextCustom>
                            {!isEmpty(footer.children) && footer.children?.map(item => (
                                <Link key={uniqueId()}
                                    href={item.url}>
                                    <a className='block mb-2'>
                                        <TextCustom
                                            className={classes.textMenuItem}
                                            textMedium
                                            color={COLORS.textHeading}
                                        >{item?.title![lang]}</TextCustom>
                                    </a>
                                </Link>
                            ))}
                        </Box>
                    </Grid>
                ))}
                {isLoading && range(4).map(r => (
                    <Grid key={uniqueId('menu-footer')}
                        item
                        lg={7 / 4}
                        md={3}
                        sm={6}
                        paddingBottom={5}
                        paddingRight={5}
                        xs={12}>
                        <SkeletonCustom
                            disableImage
                            textHeight={50}
                            className='mb-7'
                            lineText={1}
                        />
                        <SkeletonCustom
                            disableImage
                            lineText={5}
                        />
                    </Grid>
                ))}
                <Grid item
                    md={2}
                    xs={12}>
                    <Box>
                        <TextCustom
                            className='w-full'
                            h5
                            color={COLORS.textHeading}
                        >
                            {t('appAndPayment')}
                        </TextCustom>
                        <TextCustom
                            textSmall
                            color={COLORS.textBody}
                            className='!my-6'>
                            {t('installApplicationDescription')}
                        </TextCustom>
                    </Box>
                    <Box className='flex'>
                        <Box className='mr-6 cursor-pointer 
                        hover:translate-y-1 w-32 transition-all duration-300 ease-in-out'>
                            <Image src={imageAssets.imageAppstore} />
                        </Box>
                        <Box className='mr-6  w-32 cursor-pointer 
                        hover:translate-y-1 transition-all duration-300 ease-in-out'>
                            <Image src={imageAssets.googlePlay} />
                        </Box>
                    </Box>
                    <TextCustom className='py-5 text-body'
                        textSmall>{t('securedPayment')}</TextCustom>
                    <Box height={32}
                        position='relative'>
                        <img className='w-56'
                            src={imageAssets.payment}
                            alt="" />
                    </Box>
                    <Box>
                    </Box>
                </Grid>
            </Grid>
            <Box
                paddingY={3}
                display='flex'
                alignItems={'center'}
                justifyContent={'space-between'}
                className={classes.footerInfo}>
                <TextCustom
                    textMedium
                    color={COLORS.textHeading}
                    dangerouslySetInnerHTML={{ __html: t('infoFooterPage') }} />
                <Box>
                    <Box display="flex"
                        alignItems={'center'}>
                        <TextCustom className='!mr-4'
                            h6
                            color={COLORS.textHeading}>{t('followUs')}</TextCustom>
                        <Box
                            display={'flex'}
                            className='list-icon'
                        >
                            <Box component={'a'}
                                href='#'
                                marginRight={0.5}>
                                <Box width={16}
                                    height={30}
                                    left={7}
                                    position='relative'>
                                    <Image src={imageAssets.facebook}
                                        layout={'fill'}
                                        objectFit={'contain'} />
                                </Box>
                            </Box>
                            <Box component={'a'}
                                href='#'
                                marginRight={0.5}>
                                <Box width={16}
                                    height={30}
                                    left={7}
                                    position='relative'>
                                    <Image src={imageAssets.twitter}
                                        layout={'fill'}
                                        objectFit={'contain'} />
                                </Box>
                            </Box>
                            <Box component={'a'}
                                href='#'
                                marginRight={0.5}>
                                <Box width={16}
                                    height={30}
                                    left={7}
                                    position='relative'>
                                    <Image src={imageAssets.instagram}
                                        layout={'fill'}
                                        objectFit={'contain'} />
                                </Box>
                            </Box>
                            <Box component={'a'}
                                href='#'
                                marginRight={0.5}>
                                <Box width={16}
                                    height={30}
                                    left={7}
                                    position='relative'>
                                    <Image src={imageAssets.youtube}
                                        layout={'fill'}
                                        objectFit={'contain'} />
                                </Box>
                            </Box>
                        </Box>
                    </Box>
                    <TextCustom
                        textMedium
                        color={COLORS.textBody}>{t('upToSubscribe')}</TextCustom>
                </Box>
            </Box>
        </Box>
    );
};

export default withStyles(styles)(FooterComponent);