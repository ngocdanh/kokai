import { Theme } from '@mui/material';
import { createStyles } from '@mui/styles';
import { } from '@mui/styles/';
import { COLORS } from '@constants/colors';

const styles = (theme: Theme) => createStyles({
    root: {
        '& .list-info': {
            '& .info': {
                '& .name': {
                    // minWidth: 100,
                    minWidth: 'max-content',
                }
            },
            '& a': {
                transition: 'all .3s ease',
                '&:hover': {
                    color: `${COLORS.brand1} !important`
                }
            }
        },
    },
    gridLayout: {
        paddingTop: 39,
        paddingBottom: 39,
        borderBottom: '1px solid #E5E5E5',
    },
    textMenuItem: {
        transition: 'all .3s ease',
        '&:hover': {
            color: COLORS.brand1,
            transform: 'translateX(2px)',
        }
    },
    footerInfo: {
        borderTop: '1px solid #D8F1E5',
        '& a': {
            display: 'inline-block',
            color: COLORS.brand1,
        },
        '& .list-icon': {
            '& a': {
                width: 30,
                height: 30,
                borderRadius: '30px',
                backgroundColor: COLORS.brand1,
                display: 'inline-block',
                transition: 'all .3s ease',
                '&:hover': {
                    backgroundColor: COLORS.warring
                }
            }
        },

    },
    banner: {
        position: 'relative',
        borderRadius: '25px',
        overflow: 'hidden',
        marginBottom: 60,

        '& .img': {
            width: '100%',
            objectFit: 'cover',
            backGroundColor: COLORS.brand1,
            height: 383,
        },
        '& .content': {
            position: 'absolute',
            top: 0,
            left: 0,
            width: '100%',
            height: '100%',
            maxWidth: 900,
            display: 'flex',
            flexDirection: 'column',
            justifyContent: 'center',
            padding: 100,
            paddingRight: 0,
            '& .title': {
            },
            '& .description': {
                marginTop: 33,
                fontWeight: 400,
                fontSize: 18,
            },
            '& .box-input': {
                marginTop: 68,
                position: 'relative',
                width: 450,
                '& input': {
                    borderRadius: '30px',
                    background: COLORS.white,
                    height: 50,
                    fontSize: 15,
                    fontWeight: 400,
                    width: '100%',
                    lineHeight: '18px',
                    color: COLORS.textBody,
                    paddingLeft: 57,
                    paddingTop: 15,
                    paddingBottom: 15,
                    '&::placeholder': {
                        color: '#B6B6B6',
                    }
                },
                '& .button-custom': {
                    position: 'absolute',
                    right: 0,
                    borderRadius: '30px !important',
                    width: 150,
                    display: 'flex',
                    justifyContent: 'center',
                    top: 0,
                    bottom: 0,
                },
                '& img': {
                    width: 24,
                    height: 24,
                },
                '& i': {
                    position: 'absolute',
                    left: -40,
                    borderRadius: '30px !important',
                    width: 150,
                    zIndex: 100,
                    display: 'flex',
                    justifyContent: 'center',
                    top: 17,
                    bottom: 0,
                }
            }
        }
    },
});

export default styles;