export interface IFooter {
    title?: string,
    menuFooter: {
        title: {
            en: string,
            vi: string
        },
        children: {
            title: {
                en: string, 
                vi: string
            },
            url: string,
        }[]
    }[]
}