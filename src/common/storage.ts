import { STORAGE } from '@constants/constants';
import { isObject, isString } from 'lodash';

type typeDataLocalStorage = string | object

/* START USER LOCAL */
export const setDataUserLocal = (data: typeDataLocalStorage ) => {
    try {
        let dataString = '';
        if(isObject(data)) {
            dataString = JSON.stringify(data);
        } else if(isString(data)) {
            dataString = data;
        } else {
            return;
        }
        if (typeof window !== 'undefined') {
            window.localStorage.setItem(STORAGE.USER, dataString);
        }
    } catch (error){
        return null;
    }
}; 

export const getDataUserLocal = () => {
    let data;
    if (typeof window !== 'undefined') {
        data  = window.localStorage.getItem(STORAGE.USER);
    }
    try {
        if(data) {
            const objData = JSON.parse(data);
            return objData || data;
        } else {
            return null;
        }
    } catch (error) {
        return null;
    }
   
}; 
/* END USER LOCAL */

/* START ACCESS_TOKEN LOCAL */
export const setAccessTokenLocal = (data: typeDataLocalStorage ) => {
    try {
        let dataString = '';
        if(isObject(data)) {
            dataString = JSON.stringify(data);
        } else if(isString(data)) {
            dataString = data;
        } else {
            return;
        }
        if (typeof window !== 'undefined') {
            window.localStorage.setItem(STORAGE.ACCESS_TOKEN, dataString);
        }
    } catch(error) {
        return null;
    }
}; 

export const getAccessTokenLocal = () => {
    try {
        if (typeof window !== 'undefined') {
            const data =  window.localStorage.getItem(STORAGE.ACCESS_TOKEN);
            return data || null;
        }
    } catch (error) {
        return null;
    }
}; 
/* END USER LOCAL */

/* START REFRESH_TOKEN LOCAL */
export const setRefreshTokenLocal = (data: typeDataLocalStorage ) => {
    try {
        let dataString = '';
        if(isObject(data)) {
            dataString = JSON.stringify(data);
        } else if(isString(data)) {
            dataString = data;
        } else {
            return;
        }
        if (typeof window !== 'undefined') {
            window.localStorage.setItem(STORAGE.REFRESH_TOKEN, dataString);
        }
    } catch (error) {
        return null;
    }
}; 

export const getRefreshTokenLocal = () => {
    try {
        if (typeof window !== 'undefined') {
            const data =  window.localStorage.getItem(STORAGE.REFRESH_TOKEN);
            return data || null;
        }
    } catch (error) {
        return null;
    }
}; 
/* END REFRESH_TOKEN LOCAL */
