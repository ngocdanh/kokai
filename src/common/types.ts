export type typeProductItem = {
    _id: string,
    in_stock: number,
    name: string,
    slug: string,
    sale_price: number,
    weight?: number,
    price: number,
    description: string,
    discount?: string | null,
    categories?: {
        name?: string,
        slug?: string,
    }[],
    quantity?: number,
    vote: number,
    image: {
        original: string,
        thumbnail: string,
    },
    sold?: number,
    total?: number,
    gallery?:{
        original: string,
        thumbnail: string,
    }[] | null,
    handleClose?: ((event: {}, reason: any) => void) | undefined,
};

export type typeDefaultReducer = {
    isSuccess?: boolean,
    isLoading?: boolean,
    error?: any,
}

export interface IReducerGlobal {
    register?: typeDefaultReducer
}

export interface typeCartItem {
    name: string
    slug: string
    price: number | any
    sale_price: number | any
    quantity: number
    cart_quantity: number
    in_stock: number
    id_: string
    image: Image
    categories: Category[]
    vote: number
    discount: any
    total: number
    seller_rank: number
    sold: number
    promotion_code: any
}
  
export interface Image {
    original: string
    thumbnail: string
}
  
export interface Category {
    name: string
    slug: string
}


export type IOderData = {
    orderId: string,
    date: string,
    status: number,
    total: string,
    actions: string
}

export type IMyAddress = {
    billingAddress: [string],
    shippingAddress: [string]
}
