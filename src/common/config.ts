export const isDev = process.env.NODE_ENV !== 'production';

export const server = isDev 
    ? 'http://localhost:1900' 
    : 'https://kokai.vercel.app';

export const BASE_URL = process.env.BASE_URL;
export const API_URL = process.env.API_URL;
export const API_URL_AUTH = process.env.API_URL + 'auth/';

export const routeRoot = {
    home: '/',
    register: '/register',
    login: '/login',
    product: '/product',
    category: '/category',
    forgotPassword: '/forgot-password',
    contactUs: '/contact-us',
    blog: '/blog',
    compare: '/compare',
    account: '/account',
    checkout: '/checkout',
    cart: '/cart',
    vendor: '/vendor',
    wishlist: '/wishlist',
    order: '/order',
    setting: '/setting',
    voucher: '/voucher',
    signout: '/signout'
};