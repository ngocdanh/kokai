import { COLORS } from '@constants/colors';
import ButtonCustom from '@helpers/ButtonCustom';
import MenuCustom from '@helpers/MenuCustom';
import TextCustom from '@helpers/TextCustom';
import { Box, Grid } from '@mui/material';
import { WithStyles, withStyles } from '@mui/styles';
import clsx from 'clsx';
import { isEmpty, range, uniqueId } from 'lodash';
import Image from 'next/image';
import { useTranslation } from 'next-i18next';
import styles from '../styles';
import { IHeaderComponent } from '../types';
import { useRouter } from 'next/router';
import Link from 'next/link';
import SkeletonCustom from '@helpers/SkeletonCustom';
import { useSelector } from 'react-redux';

const MenuList = (props: IHeaderComponent & WithStyles<typeof styles>) => {
    const {
        classes,
        menuHeader,
        menuCategory,
        openCategory,
        handleOpenMenuCategory,
        handleCloseMenuCategory,
    } = props;
    const {isLoading} = useSelector((state: any) => state.global);

    const {t} = useTranslation();
    const router = useRouter();
    const lang = router.locale as 'vi' | 'en';

    const renderMenuItem = (subMenu: typeof menuHeader) => {
  
        return (
            <Box className='sub-menu'
                component={'ul'}>
                {!isEmpty(subMenu) && subMenu?.map(sub => (
                    <Box key={uniqueId('sub-menu')}
                        className="sub-menu-item"
                        component={'li'}>
                        <Link href={sub.url!}>
                            <a>
                                <TextCustom headingSM
                                    hoverColor
                                    color={COLORS.textBody}>
                                    {sub?.title![lang]}
                                </TextCustom>
                            </a>
                        </Link>
                    </Box>
                ))}
            </Box>
        );
    };

    const renderButtonCategory = ()=> (
        <ButtonCustom 
            onClick={handleOpenMenuCategory}
            iconStart={'fi fi-rs-apps'} 
            small
            iconEnd={'fi fi-rs-angle-small-down'}
            className='!mr-9 !rounded'
            customStyle={{
                iconStart: {
                    color: COLORS.white,
                    width: 16,
                    height: 16,
                },
                iconEnd: {
                    color: COLORS.white,
                    width: 10,
                    marginLeft: 10,
                }
            }}>
            <TextCustom h6
                color={COLORS.white}>{t('browseAllCategories')}</TextCustom>
        </ButtonCustom>
    );
    return (
        <Box 
            display={'flex'} 
            alignItems={'center'}
            paddingY={3}
            justifyContent={'space-between'}
            className={classes.MenuList}>
            <Box className='flex items-center'>
     
                <MenuCustom
                    button={renderButtonCategory()}
                    open={openCategory!}
                    className='category'
                    onClose={handleCloseMenuCategory!}
                    paper={classes.paper}
                >
                    <Grid 
                        container
                        spacing={2}
                        width={'100%'}
                        marginLeft={0}
                    >
                        {!isEmpty(menuCategory) && menuCategory?.map((category)=> (
                            <Grid 
                                item
                                key={uniqueId()}
                                md={6}
                                xs={12}
                                display={'flex'}
                                alignItems={'center'} 
                                className='category-item'
                            >
                                <Box 
                                    className='cursor-pointer item flex items-center px-4 w-full'
                                    onClick={handleCloseMenuCategory}
                                >
                                    <Box className="relative mr-3 px-4 py-3 rounded">
                                        <Image alt={category?.title![lang]} 
                                            src={category.icon}
                                            layout='fill' 
                                            objectFit='contain'
                                            className='mr-2 !w-[30px] !min-w-[30px]' />
                                    </Box>
                                    <TextCustom color={COLORS.textHeading} 
                                        headingSM>{category?.title![lang]}</TextCustom>
                                </Box>
                            </Grid>
                        ))}
                    </Grid>
                </MenuCustom>
                <Box display={'flex'}
                    component={'ul'}
                    className='menu'>
                    {!isEmpty(menuHeader) && menuHeader?.map((menu) => (
                        <Box 
                            component={'li'} 
                            key={uniqueId('menu-item')} 
                            className='flex mr-10 items-center cursor-pointer menu-item'>
                            {menu.icon && (
                                <i className={clsx(menu.icon, 'mr-2 text-brand w-5 h-5')}></i>
                            )}
                            <Link href={menu.url!}>
                                {!isEmpty(menu.children) ? <div>
                                    <TextCustom h6
                                        hoverColor
                                        color={COLORS.textHeading}>
                                        {menu?.title![lang]}
                                    </TextCustom>
                                    {!isEmpty(menu.children) && (
                                        <i className='fi fi-rs-angle-small-down ml-[5px] text-body' />
                                    )}
                                    {!isEmpty(menu.children) && renderMenuItem(menu.children)}
                                </div>
                                    : (
                                        <a>
                                            <TextCustom h6
                                                hoverColor
                                                color={COLORS.textHeading}>
                                                {menu?.title![lang]}
                                            </TextCustom>
                                            {!isEmpty(menu.children) && (
                                                <i className='fi fi-rs-angle-small-down ml-[5px] text-body' />
                                            )}
                                            {!isEmpty(menu.children) && renderMenuItem(menu.children)}
                                        </a>
                                    )
                                }
                                
                            </Link>
                        </Box>
                    ))}
                    {isLoading && range(8).map(r => (
                        <Box 
                            component={'li'} 
                            key={uniqueId('menu-item')} 
                            className='flex mr-10 items-center cursor-pointer menu-item'>
                            <SkeletonCustom 
                                disableImage
                                lineText={1}
                            />
                        </Box>
                    ))}
                </Box>
            </Box>
            <Box 
                display={'flex'}
                alignItems={'center'}
            >
                <i className="fi fi-rs-headset text-4xl text-heading mr-[13px]"></i>
                <Box textAlign={'center'}
                    component='a'
                    href={'tel:19008888'}
                    className='flex flex-col'>
                    <TextCustom color={COLORS.brand1}
                        h4>1900 - 8888</TextCustom>
                    <TextCustom className='text-xs'
                        color={COLORS.textBody}>{t('support247')}</TextCustom>
                </Box>
            </Box>
   
        </Box>
    );
};

export default withStyles(styles)(MenuList);