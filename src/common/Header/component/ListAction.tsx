import { routeRoot } from '@common/config';
import { COLORS } from '@constants/colors';
import imageAssets from '@constants/imageAssets';
import TextCustom from '@helpers/TextCustom';
import { Box, Menu, MenuItem, Button } from '@mui/material';
import { WithStyles, withStyles } from '@mui/styles';
import { useTranslation } from 'next-i18next';
import Link from 'next/link';
import { useRouter } from 'next/router';
import React from 'react';
import styles from '../styles';
import { IListAction } from '../types';




const ListAction = (props: IListAction & WithStyles<typeof styles>) => {
    const {
        classes,
        handleChangeLanguage,
    } = props;
    const { t } = useTranslation();
    const router = useRouter();
    const lang = router.locale;

    let currentlyHovering = false;

    const [anchorEl, setAnchorEl] = React.useState(null);

    function handleClick(event: any) {
        if (anchorEl !== event.currentTarget) {
            setAnchorEl(event.currentTarget);
        }
    }

    function handleHover() {
        currentlyHovering = true;
    }

    function handleClose() {
        setAnchorEl(null);
    }

    function handleCloseHover() {
        currentlyHovering = false;
        setTimeout(() => {
            if (!currentlyHovering) {
                handleClose();
            }
        }, 50);
    }

    return (
        <Box display={'flex'}
            className={classes.listAction}>
            <Box className="language">
                <Box className="select-lang cursor-pointer"
                    onClick={handleChangeLanguage}>
                    <img src={lang === 'en' ? imageAssets.iconUS: imageAssets.iconVN}/>
                </Box>
            </Box>
            <Link href={routeRoot.compare}>
                <Box display={'flex'}
                    className={'cursor-pointer item hover-color'}
                    marginRight={3}>
                    <i className="fi fi-rs-refresh text-heading mr-3"></i>
                    <TextCustom textMedium
                        hoverColor
                        color={COLORS.textBody}>
                        {t('compare')}
                    </TextCustom>
                </Box>
            </Link>
            <Link href={routeRoot.wishlist}>
                <Box display={'flex'}
                    component='a'
                    className={'cursor-pointer item hover-color'}
                    marginRight={3}>
                    <Box className="box-icon">
                        <i className="fi fi-rs-heart text-heading mr-3"></i>
                        <Box component={'div'}
                            className={'notification'}>
                        5
                        </Box>
                    </Box>
                    <TextCustom textMedium
                        hoverColor
                        color={COLORS.textBody}>
                        {t('wishlist')}
                    </TextCustom>
                </Box>
            </Link>

            <Link href={routeRoot.cart}>
                <Box display={'flex'}
                    className={'cursor-pointer item hover-color'}
                    marginRight={3}>
       
                    <Box className="box-icon">
                        <i className="fi fi-rs-shopping-cart text-heading mr-3"></i>
                        <Box component={'div'}
                            className={'notification'}>
                        4
                        </Box>
                    </Box>
                    <TextCustom textMedium
                        hoverColor
                        color={COLORS.textBody}>
                        {t('cart')}
                    </TextCustom>
                </Box>
            </Link>
            <Box>
                <Button aria-owns={anchorEl ? 'simple-menu' : undefined}
                    aria-haspopup="true"
                    disableElevation
                    disableRipple={true}
                    onClick={handleClick}
                    onMouseOver={handleClick}
                    onMouseLeave={handleCloseHover}>
                    <Link href={routeRoot.account}
                        
                    >
                        <Box display={'flex'}
                            className={'cursor-pointer item hover-color'}
                            marginRight={3}>
                            <i className="fi fi-rs-user text-heading mr-3"></i>
                            <TextCustom textMedium
                                hoverColor
                                color={COLORS.textBody}>
                                {t('account')}
                            </TextCustom>
                        </Box>
                    </Link>
                </Button>
                <Menu
                    className={classes.MenuDrop}
                    id="simple-menu"
                    anchorEl={anchorEl}
                    open={Boolean(anchorEl)}
                    onClose={handleClose}
                    autoFocus={false}
                    elevation={1}
                    MenuListProps={{
                        onMouseEnter: handleHover,
                        onMouseLeave: handleCloseHover,
                        style: { pointerEvents: 'auto' }
                    }}
                    anchorOrigin={{ horizontal: 'right', vertical: 'bottom' }}
                    transformOrigin={{ vertical: 'top', horizontal: 'right' }}
                    transitionDuration={{
                        appear: 400,
                        enter: 500,
                        exit: 400 }}
                    PopoverClasses={{
                        root: classes.popOverRoot
                    }}
                >
                    <MenuItem onClick={handleClose}>
                        <Link href={routeRoot.account}>
                            <Box
                                className={'flex items-center cursor-pointer item hover-color'}
                                marginRight={3}>
                                <i className="fi fi-rs-user text-heading mr-3"></i>
                                <TextCustom textSmall
                                    hoverColor
                                    color={COLORS.textHeading2}>
                                    {t('account')}
                                </TextCustom>
                            </Box>
                        </Link>
                    </MenuItem>
                    <MenuItem onClick={handleClose}>
                        <Link href={routeRoot.order}>
                            <Box
                                className={'flex items-center cursor-pointer item hover-color'}
                                marginRight={3}>
                                <i className="fi fi-rr-map-marker text-heading mr-3"></i>
                                <TextCustom textSmall
                                    hoverColor
                                    color={COLORS.textHeading2}>
                                    {t('order')}
                                </TextCustom>
                            </Box>
                        </Link>
                    </MenuItem>
                    <MenuItem onClick={handleClose}>
                        <Link href={routeRoot.voucher}>
                            <Box
                                className={'flex items-center cursor-pointer item hover-color'}
                                marginRight={3}>
                                <i className="fi fi-rr-ticket text-heading mr-3"></i>
                                <TextCustom textSmall
                                    hoverColor
                                    color={COLORS.textHeading2}>
                                    {t('voucher')}
                                </TextCustom>
                            </Box>
                        </Link>
                    </MenuItem>
                    <MenuItem onClick={handleClose}>
                        <Link href={routeRoot.wishlist}>
                            <Box
                                className={'flex items-center cursor-pointer item hover-color'}
                                marginRight={3}>
                                <i className="fi fi-rr-heart text-heading mr-3"></i>
                                <TextCustom textSmall
                                    hoverColor
                                    color={COLORS.textHeading2}>
                                    {t('wishlist')}
                                </TextCustom>
                            </Box>
                        </Link>
                    </MenuItem>
                    <MenuItem onClick={handleClose}>
                        <Link href={routeRoot.setting}>
                            <Box
                                className={'flex items-center cursor-pointer item hover-color'}
                                marginRight={3}>
                                <i className="fi fi-rr-settings-sliders text-heading mr-3"></i>
                                <TextCustom textSmall
                                    hoverColor
                                    color={COLORS.textHeading2}>
                                    {t('setting')}
                                </TextCustom>
                            </Box>
                        </Link>
                    </MenuItem>
                    <MenuItem onClick={handleClose}>
                        <Link href={routeRoot.signout}>
                            <Box
                                className={'flex items-center cursor-pointer item hover-color'}
                                marginRight={3}>
                                <i className="fi fi-br-sign-out text-heading mr-3"></i>
                                <TextCustom textSmall
                                    hoverColor
                                    color={COLORS.textHeading2}>
                                    {t('signout')}
                                </TextCustom>
                            </Box>
                        </Link>
                    </MenuItem>
                    
                </Menu>
            </Box>
        </Box>
    );
};

export default withStyles(styles)(ListAction);
