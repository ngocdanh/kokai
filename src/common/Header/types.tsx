import { MouseEvent } from 'react';

export interface IHeaderComponent {
    title?: string,
    menuHeader?: {
        title?: {
            en: string,
            vi: string,
        } ,
        icon?: string,
        url?: string,
        children?: []
    }[],
    menuItem?: {
        title?: {
            en: string,
            vi: string,
        },
        icon?: string,
        url?: string,
        children?: []
    },
    menuCategory?: {
        title?: {
            en: string,
            vi: string,
        } ,
        icon: string,
        url: string,
    }[],
    openCategory?: null | HTMLElement,
    handleOpenMenuCategory?: (event: MouseEvent<HTMLElement>) => void,
    handleCloseMenuCategory?: ()=> void,
    handleChangeLanguage?: () => void,
    lang?: string,
}

export interface IListAction {
    handleChangeLanguage?: () => void,
    lang?: string,
}