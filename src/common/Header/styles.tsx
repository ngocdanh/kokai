import { Theme } from '@mui/material';
import {createStyles} from '@mui/styles';
import { COLORS } from '@constants/colors';

const styles = (theme: Theme) => createStyles({
    root: {
        paddingTop: 28,
        paddingBottom: 28,
        borderBottom: '1px solid #E5E5E5',
    },  
    logo: {
        marginBottom: '-5px !important',

        '& svg': {
            width: 220,
        }
    },
    listAction: {
        alignItems: 'center',
        '& .item': {
            transition: 'all .3s ease',  
            '&:hover': {
                transform: 'translateY(-2px)',
            },
            '& .box-icon': {
                position: 'relative',
            },
            '& .notification': {
                width: 20,
                height: 20,
                borderRadius: '50%',
                backgroundColor: COLORS.brand1,
                display: 'flex',
                justifyContent: 'center',
                alignItems: 'center',
                fontSize: 12,
                color: COLORS.white,
                lineHeight: '18px',
                position: 'absolute',
                top: -8,
                right: 2,
            }
        },
        '& i': {

            fontSize: 20,
        },
        '& .language': {
            marginRight: 20,
            padding: 5,
            transition: 'all .3s ease',
            borderRadius: '5px',
            '& img': {
                width: 25,
                objectFit: 'contain',
            },
            '&:hover': {
                background: '#e7e7e7e8',
            }
        },
        '& .modal-language': {
            '& img': {
                width: 25,
                objectFit: 'contain',
            }
        }
    },
    groupInput: {
        border: `2px solid ${COLORS.border3}`,
        height: 48,
        position: 'relative',
        maxWidth: 600,
        width: '100%',
        borderRadius: '5px',
        '& select': {
            width: '100%',
            height: '100%',
            border: '1px solid #ececec',
            borderRight: '0',
            padding: '3px 35px 4px 20px',
            backgroundColor: COLORS.white,
            transition: 'all .3s ease',
            '-webkit-appearance': 'none',
            '-moz-appearance': 'none',
            fontSize: 14,
            lineHeight: '20px',
            color: COLORS.textHeading,
            fontWeight: 700,
        },
        '& input': {
            backgroundColor: COLORS.white,
            width: '100%',
            height: '100%',
            padding: '5px 10px',
            '&::placeholder': {
                fontSize: 12,
                fontWeight: 400,
                lineHeight: '18px',
            }
        },
        '& > i': {
            position: 'absolute',
            top: 14,
            right: 22,
            width: 20,
            height: 20,
            color: COLORS.textBody,
        },
        '& .box-select': {
            width: 300,
            position: 'relative',
            '&::before': {
                content: '""',
                position: 'absolute',
                backgroundColor: '#CACACA',
                width: 1,
                height: '60%',
                top: '20%',
                right: 0,
            },
            '& i': {
                position: 'absolute',
                right: 15,
                top: 14,
                width: 11,
                color: COLORS.textBody,
            }
        },
        '& .box-input': {
        }
    },
    MenuList: {
        '& .menu': {
            '&-item': {
                position: 'relative',
                '& .skeleton-custom': {
                    minWidth: 70,
                    height: 10,
                    marginBottom: 15,
                    '& .MuiSkeleton-root.MuiSkeleton-text': {
                        height: '25px !important',
                    }
                },
                '&:hover p': {
                    color: COLORS.brand1,
                },
                '&:hover .sub-menu p': {
                    color: COLORS.textBody,
                },
                '&:hover i': {
                    color: COLORS.brand1,
                },
                '& .sub-menu': {
                    width: 300,
                    position: 'absolute',
                    top: 50,
                    left: 0,
                    border: `1px solid ${COLORS.bgGrey}`,
                    padding: '20px 15px',
                    borderRadius: '10px',
                    backgroundColor: COLORS.white,
                    transition: 'all .3s ease',
                    opacity: 0,
                    visibility: 'hidden',
                    transform: 'translateY(-10px)',
                    zIndex: 200,
                    '&-item': {
                        padding: '10px 10px',
                        '& .text-custom': {
                            '&:hover': {
                                color: COLORS.brand1,
                            }
                        }
                    }
                },
                '&:hover': {
                    '& .sub-menu': {
                        opacity: 1,
                        visibility: 'visible',
                        transform: 'translateY(0)',
                    }
                }
            }
        },
    },
    paper: {
        width: 500,
        border: `1px solid ${COLORS.brand1} !important`,
        '& .category': {
            '&-item': {
                paddingTop: '15px !important',
       
                '& .item': {
                    border: `1px solid ${COLORS.bgGrey}`,
                    borderRadius: '5px',
                    marginRight: 18,
                    height: 48,
                    display: 'flex',
                    alignItems: 'center',
                    transition: 'all .3s ease',
                    '&:hover': {
                        border: `1px solid ${COLORS.brand1}`,
                        boxShadow: '5px 5px 15px rgba(24, 24, 24, 0.05)',
                    }
                }
            }
        }
    },
    paperLang: {
        padding: 10,
        boxShadow: '-5px 0px 15px 4px rgba(0,0,0,0.1) !important',
    },

    popOverRoot: {
        pointerEvents: 'none'
    },

    MenuDrop: {
        '& .MuiPaper-root': {
            padding: '10px 5px',
            minWidth: 200,
        },

        '& .MuiMenu-list li': {
            padding: '10px 18px'
        }
    }


});

export default styles;