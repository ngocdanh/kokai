import { Box, Divider } from '@mui/material';
import { WithStyles, withStyles } from '@mui/styles';
import { ImageLogo } from '@constants/imageAssets';
import ListAction from './component/ListAction';
import MenuList from './component/MenuList';
import styles from './styles';
import { IHeaderComponent } from './types';
import { useTranslation } from 'next-i18next';
import Link from 'next/link';
import { routeRoot } from '@common/config';

const HeaderComponent = (props: IHeaderComponent & WithStyles<typeof styles>) => {
    const {
        classes,
        openCategory,
        menuCategory,
        menuHeader,
        handleOpenMenuCategory,
        handleCloseMenuCategory,
        lang,
        handleChangeLanguage,
    } = props;

    const { t } = useTranslation();

    return (
        <Box>
            <Box 
                display={'flex'}
                alignItems={'end'}
                justifyContent={'space-between'}
                className={'pb-9 pt-7'}
            >
      
                <Box display='flex'
                    alignItems='end'>
                    <Box className={classes.logo}>
                        <Link href={routeRoot.home}>
                            <a><ImageLogo /></a>
                        </Link>
                    </Box>
                    <Box marginLeft={5}
                        display={'flex'}
                        
                        className={classes.groupInput}>
                        <Box className='box-select'>
                            <i className="fi fi-rs-angle-small-down"></i>
                            <select>
                                <option value="all">{t('all')}</option>
                                <option value="categories">{t('allCategories')}</option>
                                <option value="products">{t('allProducts')}</option>
                                <option value="vendor">{t('vendor')}</option>
                            </select>
                        </Box>
                        <Box className='box-input w-full'>
                            <input placeholder='Search for item'/>
                        </Box>
       
                        <i className="fi fi-rs-search cursor-pointer"></i>
                    </Box>
                </Box>
                <Box justifyContent={'flex-end'}
                    className={classes.logo}>
                    <ListAction 
                        lang={lang}
                        handleChangeLanguage={handleChangeLanguage}
                    />
                </Box>
            </Box>
            <Divider/>
            <MenuList 
                menuHeader={menuHeader}
                menuCategory={menuCategory}
                openCategory={openCategory}
                handleOpenMenuCategory={handleOpenMenuCategory}
                handleCloseMenuCategory={handleCloseMenuCategory}
            />
            <Divider/>
        </Box>
    );
};

export default withStyles(styles)(HeaderComponent);