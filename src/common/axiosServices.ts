import axios from 'axios';
import { GET } from '@constants/index';
import { isEmpty } from 'lodash';
import queryString from 'query-string';
import { toast } from 'react-toastify';
import { i18n } from 'next-i18next';
import { getAccessTokenLocal } from './storage';
import { DELETE, POST, PUT, OPTIONS, PATCH } from '../constants/constants';

type Methods = 'head' | 'options' | 'put' | 'post' | 'patch' | 'delete' | 'get';
interface IAxios {
    url: string, 
    method?: Methods, 
    body?: any, 
    headers?: any, 
    delay?: number, 
    params?: object
}

const axiosServices = (props: IAxios) => {
    const { 
        url, 
        method = GET, 
        body = null, 
        headers = null, 
        params
    } = props;
    /* CONFIG URI AXIOS */
    let uri = url;
    if(method === GET && !isEmpty(params)) {
        uri = `${url}?${queryString.stringify(params)}`;
    }
    /* CONFIG HEADER AXIOS */
    const accessToken = getAccessTokenLocal() ?? '';
    let headerConfig = {
        headers: {
            'x-access-token': accessToken,
        }
    };
    if(headers) {
        headerConfig = {
            ...headerConfig,
            ...headers,
        };
    }
    if(method === GET || method === DELETE || method === OPTIONS) {
        return axios[method](uri, headerConfig)
            .then((res: any) => {
                if(!res?.data?.success) {
                    toast.error(res?.data?.message || i18n?.t('errorAPI'));
                }
                return res.data.data;
            })
            .catch((err: any) => {
                return err;
            });
    } else if(method === POST || method === PUT || method === PATCH) {
        return axios[method](uri, body, headerConfig)
            .then((res: any) => {
                if(!res?.data?.success) {
                    toast.error(res?.data?.message || i18n?.t('errorAPI'));
                }
                return res;
            })
            .catch((err: any) => {
                return err;
            });
    }
   
};

export default axiosServices;