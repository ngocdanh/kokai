import { COLORS } from '@constants/colors';
import { createTheme } from '@mui/material';

export const theme = createTheme({
    typography: {
        fontFamily: '\'Quicksand\', \'Source Sans Pro\', \'Arial\', sans-serif',
        button: {
            textTransform: 'none'
        }
    },
    components: {
        MuiCssBaseline: {
            styleOverrides: {
                '@font-face': {
                    fontFamily: '\'Quicksand\', \'Source Sans Pro\', \'Arial\', sans-serif',
                },
            }
        }
    },
    palette: {
        primary: {
            main: COLORS.brand1
        },
        secondary: {
            main: COLORS.brand2
        },
    }
});
