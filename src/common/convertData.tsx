import { round } from 'lodash';

export const convertNumber = (numberData : any) => {
    return numberData ? `(${round(numberData).toFixed(1)})`: '';
};

export const convertToDiscountStatus = (data: any) => {
    const {sale_price, price} = data;
    if(!price ||  !sale_price) {
        return ({
            value: '',
            css_class: '',
        });
    }
    const percentDiscount = round((1 - sale_price/ price)*100, 0);
    let value = `${percentDiscount}%`;
    let css_class = '';
    if(percentDiscount > 20) {
        css_class = 'red';
        value = 'hot';
    } else if(percentDiscount > 10) {
        value = 'sale';
        css_class = 'yellow';
    }
    return ({
        value,
        css_class,
    });
};

export const convertPrice = (num: any) => num ? `$${num}` : '';
export const convertGroupPrice = (data: any) => ({
    price: data?.price ? `$${data?.price}` : '',
    sale_price: data?.sale_price ? `$${data?.sale_price}` : '',
});
