import * as productConstants from '@redux/constants/products.constant';

const initialState = {
    isLoading: true,
    error: null,
    dataList: null,
};

const reducer = (state = initialState, action: { type: any; payload: { data?: any; error?: any; }; }) => {
    switch(action.type) {
        case productConstants.FETCH_LIST_PRODUCT: {
            return {
                ...state,
                isLoading: true,
                error: null,
                dataList: null
            };
        }
        case productConstants.FETCH_LIST_PRODUCT_SUCCESS: {
            const { data } = action.payload;
            return {
                ...state,
                isLoading: false,
                error: null,
                dataList: data.items,
            };
        }
        case productConstants.FETCH_LIST_PRODUCT_FAILED: {
            const { error } = action.payload;
            return {
                ...state,
                isLoading: false,
                error: error,
                dataList: null,
            };
        }
        default: {
            return {
                ...state,
            };
        }
    }
};

export default reducer;