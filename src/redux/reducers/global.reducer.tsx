import { globalConstants } from '@redux/constants';

const initialState = {
    isLoading: true,
};

const reducer = (state = initialState, action: { type: any; payload: {}; }) => {
    switch(action.type) {
        case globalConstants.SHOW_LOADING: {
            return {
                ...state,
                isLoading: true,
            };
        }
        case globalConstants.HIDE_LOADING: {
            return {
                ...state,
                isLoading: false,
            };
        }
        default: {
            return {
                ...state,
            };
        }
    }
};

export default reducer;