import { registerConstants } from '@redux/constants';

const initialState = {
    isLoading: true,
    isSuccess: false,
    error: null,
    message: '',
};

const reducer = (state = initialState, action: { type: any; payload: { data?: any; error?: any;message?: string }; }) => {
    switch(action.type) {
        case registerConstants.REGISTER_NEW_ACCOUNT: {
            return {
                ...state,
                isLoading: true,
                isSuccess: false,
            };
        }
        case registerConstants.REGISTER_NEW_ACCOUNT_SUCCESS: {
            const { data, message } = action.payload;
            return {
                ...state,
                isLoading: false,
                isSuccess: true,
                message: message,
            };
        }
        case registerConstants.REGISTER_NEW_ACCOUNT_FAILED: {
            const { error } = action.payload;
            return {
                ...state,
                isLoading: false,
                isSuccess: false,
                error,
            };
        }
        default: {
            return {
                ...state,
            };
        }
    }
};

export default reducer;