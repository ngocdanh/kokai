import { combineReducers } from 'redux';
import productReducer from './products';
import registerReducer from './register.reducer';
import loginReducer from './login.reducer';
import wishlistReducer from './wishlist.reducer';
import globalReducer from './global.reducer';

const rootReducer = combineReducers({
    product: productReducer,
    register: registerReducer,
    login: loginReducer,
    wishlist: wishlistReducer,
    global: globalReducer,
});

export default rootReducer;
