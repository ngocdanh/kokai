import { wishlistConstants } from '@redux/constants';

const initialState = {
    isLoading: true,
    isSuccess: false,
    error: null,
    message: '',
};

const reducer = (state = initialState, action: { type: any; payload: { data?: any; error?: any;message?: string }; }) => {
    switch(action.type) {
        case wishlistConstants.ADD_WISHLIST: {
            return {
                ...state,
                isLoading: true,
                isSuccess: false,
                message: '',
            };
        }
        case wishlistConstants.ADD_WISHLIST_SUCCESS: {
            const { data, message } = action.payload;
            return {
                ...state,
                isLoading: false,
                isSuccess: true,
                message: message,
            };
        }
        case wishlistConstants.ADD_WISHLIST_FAILED: {
            const { error } = action.payload;
            return {
                ...state,
                isLoading: false,
                isSuccess: false,
                error,
            };
        }
        case wishlistConstants.DELETE_WISHLIST: {
            return {
                ...state,
                isLoading: true,
                isSuccess: false,
                message: '',
            };
        }
        case wishlistConstants.DELETE_WISHLIST_SUCCESS: {
            const { data, message } = action.payload;
            return {
                ...state,
                isLoading: false,
                isSuccess: true,
                message,
            };
        }
        case wishlistConstants.DELETE_WISHLIST_FAILED: {
            const { error } = action.payload;
            return {
                ...state,
                isLoading: false,
                isSuccess: false,
                error,
            };
        }
        default: {
            return {
                ...state,
            };
        }
    }
};

export default reducer;