

import { STATUS_CODE } from '@constants/index';
import { wishlistAction } from '@redux/actions';
import { wishlistConstants } from '@redux/constants';
import { wishlistServices } from '@services/index';
import {
    put,
    call,
    delay,
    takeLatest,
} from 'redux-saga/effects';
interface actionType {
    type: string;
    payload?: any;
    meta?: any;
    error?: boolean;
}

function* actionAddWishlist(action: actionType): any {
    try {
        yield delay(500);
        const { dataSubmit } = action.payload;
        const response = yield call(wishlistServices.fetchAddWishlistAPI, dataSubmit);
        const { status, data } = response;
        if (status === STATUS_CODE.SUCCESS && data.success === true) {
            yield put(wishlistAction.fetchAddWishlistSuccess(data.data, data.message));           
        } else if(data.success === false) {
            const error = response?.data;
            error.message = response?.data?.message ?? '';
            yield put(wishlistAction.fetchAddWishlistFailed(error));
        }
    } catch (error) {
        yield put(wishlistAction.fetchAddWishlistFailed(error));
    }
}

function* actionDeleteWishlist(action: actionType): any {
    try {
        yield delay(500);
        const { dataSubmit } = action.payload;
        const response = yield call(wishlistServices.fetchDeleteWishlistAPI, dataSubmit);
        const { status, data } = response;
        if (status === STATUS_CODE.SUCCESS && data.success === true) {
            yield put(wishlistAction.fetchDeleteWishlistSuccess(data.data, data.message));           
        } else if(data.success === false) {
            const error = response?.data;
            error.message = response?.data?.message ?? '';
            yield put(wishlistAction.fetchDeleteWishlistFailed(error));
        }
    } catch (error) {
        yield put(wishlistAction.fetchDeleteWishlistFailed(error));
    }
}

export function *watchWishlist() {
    yield takeLatest(wishlistConstants.ADD_WISHLIST, actionAddWishlist);
    yield takeLatest(wishlistConstants.DELETE_WISHLIST, actionDeleteWishlist);
}