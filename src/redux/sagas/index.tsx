import { fork } from 'redux-saga/effects';
import { watchLogin } from './login.saga';
import { watchRegister } from './register.saga';
import { watchWishlist } from './wishlist.saga';


function *rootSaga() {
    yield fork(watchRegister);
    yield fork(watchLogin);
    yield fork(watchWishlist);
}

export default rootSaga;