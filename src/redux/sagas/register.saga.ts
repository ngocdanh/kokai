

import { setDataUserLocal, setRefreshTokenLocal } from '@common/storage';
import { STATUS_CODE } from '@constants/index';
import { registerAction } from '@redux/actions';
import { registerConstants } from '@redux/constants';
import { registerServices } from '@services/index';
import { setAccessTokenLocal } from '../../common/storage';
import {
    put,
    call,
    delay,
    takeLatest,
} from 'redux-saga/effects';
import { get } from 'lodash';
interface actionType {
    type: string;
    payload?: any;
    meta?: any;
    error?: boolean;
}

function* actionCreateNewAccount(action: actionType): any {
    try {
        yield delay(500);
        const { dataSubmit } = action.payload;
        const response = yield call(registerServices.fetchCreateNewAccountAPI, dataSubmit);
        const { status, data } = response;
        if (status === STATUS_CODE.SUCCESS && data.success === true) {
            const userInfo = get(data, 'data.info', {});
            const userObjId = get(data, 'data.userObjId', null);
            const email = get(data, 'data.email', '');
            const accessToken = get(data, 'data.accessToken', '');
            const refreshToken = get(data, 'data.refreshToken', '');
            yield put(registerAction.fetchCreateNewAccountSuccess(data.data, data.message));           
            yield setDataUserLocal({
                info: userInfo,
                userObjId,
                email,
            });
            yield setAccessTokenLocal(accessToken);
            yield setRefreshTokenLocal(refreshToken);
        } else if(data.success === false) {
            const error = response?.data;
            error.message = response?.data?.message ?? '';
            yield put(registerAction.fetchCreateNewAccountFailed(error));
        }
    } catch (error) {
        yield put(registerAction.fetchCreateNewAccountFailed(error));
    }
}

export function *watchRegister() {
    yield takeLatest(registerConstants.REGISTER_NEW_ACCOUNT, actionCreateNewAccount);
}