

import { setDataUserLocal, setRefreshTokenLocal } from '@common/storage';
import { STATUS_CODE } from '@constants/index';
import { loginAction } from '@redux/actions';
import { loginConstants } from '@redux/constants';
import { loginServices } from '@services/index';
import { setAccessTokenLocal } from '../../common/storage';
import {
    put,
    call,
    delay,
    takeLatest,
} from 'redux-saga/effects';
import { get } from 'lodash';
interface actionType {
    type: string;
    payload?: any;
    meta?: any;
    error?: boolean;
}

function* actionLoginAccount(action: actionType): any {
    try {
        yield delay(500);
        const { dataSubmit } = action.payload;
        const response = yield call(loginServices.fetchLoginAccountAPI, dataSubmit);
        const { status, data } = response;
        if (status === STATUS_CODE.SUCCESS && data.success === true) {
            const userInfo = get(data, 'data.info', {});
            const userObjId = get(data, 'data.userObjId', null);
            const email = get(data, 'data.email', '');
            const accessToken = get(data, 'data.accessToken', '');
            const refreshToken = get(data, 'data.refreshToken', '');
            yield put(loginAction.fetchLoginAccountSuccess(data.data, data.message));           
            yield setDataUserLocal({
                info: userInfo,
                userObjId,
                email,
            });
            yield setAccessTokenLocal(accessToken);
            yield setRefreshTokenLocal(refreshToken);
        } else if(data.success === false) {
            const error = response?.data;
            error.message = response?.data?.message ?? '';
            yield put(loginAction.fetchLoginAccountFailed(error));
        }
    } catch (error) {
        yield put(loginAction.fetchLoginAccountFailed(error));
    }
}

export function *watchLogin() {
    yield takeLatest(loginConstants.LOGIN_ACCOUNT, actionLoginAccount);
}