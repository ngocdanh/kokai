import * as productConstants from '@redux/constants/products.constant';

export const fetchListProduct = (params = {}) => ({
    type: productConstants.FETCH_LIST_PRODUCT,
    payload: {
        params,
    },
});

export const fetchListProductSuccess = (data: any) => ({
    type: productConstants.FETCH_LIST_PRODUCT_SUCCESS,
    payload: {
        data,
    },
});

export const fetchListProductFailed = (error: any) => ({
    type: productConstants.FETCH_LIST_PRODUCT_FAILED,
    payload: {
        error,
    },
});
