import { registerConstants } from '@redux/constants';

export const fetchCreateNewAccount = (dataSubmit: any) =>{
    return  ({
        type: registerConstants.REGISTER_NEW_ACCOUNT,
        payload: {
            dataSubmit,
        },
    });
};

export const fetchCreateNewAccountSuccess = (data: any, message: string) => ({
    type: registerConstants.REGISTER_NEW_ACCOUNT_SUCCESS,
    payload: {
        data,
        message,
    },
});

export const fetchCreateNewAccountFailed = (error: any) => ({
    type: registerConstants.REGISTER_NEW_ACCOUNT_FAILED,
    payload: {
        error,
    },
});
