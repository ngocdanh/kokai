export * as registerAction from './register.action';
export * as loginAction from './login.action';
export * as wishlistAction from './wishlist.action';
export * as globaleAction from './global.action';