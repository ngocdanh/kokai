import { globalConstants } from '@redux/constants';

export const showLoading = () => ({
    type: globalConstants.SHOW_LOADING,
});

export const hideLoading = () => ({
    type: globalConstants.HIDE_LOADING,
});

export const changeLanguage = () => ({
    type: globalConstants.CHANGE_LANGUAGE,
});