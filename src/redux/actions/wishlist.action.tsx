import { wishlistConstants } from '@redux/constants';

export const fetchAddWishlist = (dataSubmit: any) =>{
    return  ({
        type: wishlistConstants.ADD_WISHLIST,
        payload: {
            dataSubmit,
        },
    });
};

export const fetchAddWishlistSuccess = (data: any, message: string) => ({
    type: wishlistConstants.ADD_WISHLIST_SUCCESS,
    payload: {
        data,
        message,
    },
});

export const fetchAddWishlistFailed = (error: any) => ({
    type: wishlistConstants.ADD_WISHLIST_FAILED,
    payload: {
        error,
    },
});

/* action delete wishlist */
export const fetchDeleteWishlist = (dataSubmit: any) =>{
    return  ({
        type: wishlistConstants.DELETE_WISHLIST,
        payload: {
            dataSubmit,
        },
    });
};

export const fetchDeleteWishlistSuccess = (data: any, message: string) => ({
    type: wishlistConstants.DELETE_WISHLIST_SUCCESS,
    payload: {
        data,
        message,
    },
});

export const fetchDeleteWishlistFailed = (error: any) => ({
    type: wishlistConstants.DELETE_WISHLIST_FAILED,
    payload: {
        error,
    },
});
