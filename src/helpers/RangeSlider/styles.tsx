import { COLORS } from '@constants/colors';

const styles = {
    root: {
        '& .css-187mznn-MuiSlider-root': {
            color: `${COLORS.brand1}`
        }
    }
};

export default styles;