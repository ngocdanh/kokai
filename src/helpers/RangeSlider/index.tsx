import { useState } from 'react';
import { WithStyles, withStyles } from '@mui/styles';
import styles from './styles';
import { IRangeSlider } from './types';
import { Box, Slider } from '@mui/material';


const RangeSlider = (props: IRangeSlider & WithStyles<typeof styles>) => {
    const {
        classes,
    } = props;

    const [rangeValue, setRangeValue] = useState<number[]>([20, 37]);
    const minDistance = 10;

    const valueText = (value: number) => {
        return `${value}`;
    };

    const handleChange = (event: Event, newValue: number | number[], activeThumb: number) => {
        if(!Array.isArray(newValue)) {
            return ;
        }

        if(activeThumb === 0) {
            setRangeValue([Math.min(newValue[0], rangeValue[1] - minDistance), rangeValue[1]]);
        } else {
            setRangeValue([rangeValue[0], Math.max(newValue[1], rangeValue[0] + minDistance)]);
        }
    };
 
    return (
        <Box className={classes.root}>
            <Slider
                getAriaLabel={() => 'Minimum distance shift'}
                value={rangeValue}
                onChange={handleChange}
                valueLabelDisplay="auto"
                getAriaValueText={valueText}
                disableSwap
            />
        </Box>
        
    );
};

export default withStyles(styles)(RangeSlider);