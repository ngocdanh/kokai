import { Theme } from '@mui/material';
import {createStyles} from '@mui/styles';
import {} from '@mui/styles/';
import { COLORS } from '@constants/colors';

const styles = (theme: Theme) => createStyles({
    root: {
        '& .countdown': {
            display: 'flex',
            flexDirection: 'column',
            justifyContent: 'center',
            alignItems: 'center',
            backgroundColor: COLORS.white,
            borderRadius: '4px',
            width: '60px',
            margin: '0 5px',
            padding: '7px 5px',
            boxShadow: '5px 5px 15px rgb(0 0 0 / 5%)',

            '& .countdown-amount': {
                fontSize: '30px',
                lineHeight: '30px',

                '&:hover': {
                    transform: 'translateY(-3px)',
                    transition: 'all ease .3s',
                },
            },
        }
    },
});

export default styles;