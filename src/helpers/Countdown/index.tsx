import React from 'react';
import useCountdown from '@lib/hook/useCountDown';
import { withStyles, WithStyles } from '@mui/styles';
import { ICountdown } from './types';
import styles from './styles';
import clsx from 'clsx';
import TextCustom from '@helpers/TextCustom';
import { COLORS } from '@constants/colors';

const ExpiredNotice = () => {
    return (
        <div className="expired-notice">
            <span>Expired!!!</span>
            <p>Please select a future date and time.</p>
        </div>
    );
};

const DateTimeDisplay = (props : any) => {

    const { value, type, isDanger } = props;

    return (
        <div className={isDanger ? 'countdown danger' : 'countdown'}>
            <TextCustom h2 
                color={COLORS.brand1}
                className="countdown-amount">{value}</TextCustom>
            <span className="countdown-period">{type}</span>
        </div>
    );
};

const ShowCounter = (props : any) => {

    const {days, hours, minutes, seconds, className} = props;

    return (
        <div className={clsx('flex', 'items-center', 'justify-center', className)}>
            <DateTimeDisplay value={days}
                type={'Days'}
                isDanger={days <= 3} />
            <DateTimeDisplay value={hours}
                type={'Hours'}
                isDanger={false} />
            <DateTimeDisplay value={minutes}
                type={'Mins'}
                isDanger={false} />
            <DateTimeDisplay value={seconds}
                type={'Sec'}
                isDanger={false} />
        </div>
    );
};

const CountdownTimer = (props: ICountdown & WithStyles<typeof styles>) => {

    const {targetDate, classes, className} = props;

    const [days, hours, minutes, seconds] = useCountdown(targetDate);

    if (days + hours + minutes + seconds <= 0) {
        return <ExpiredNotice/>;
    } else {
        return (
            <ShowCounter
                className={clsx(className, classes.root)}
                days={days}
                hours={hours}
                minutes={minutes}
                seconds={seconds}
            />
        );
    }
};

export default withStyles(styles)(CountdownTimer);
