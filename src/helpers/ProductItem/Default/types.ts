import { typeProductItem } from '@common/types';
import { typeActiveCategory } from '@components/Home/types';
import { MouseEvent } from 'react';

export interface IProduct {
    dataListProduct?: typeProductItem[],
    handleChangeMenuProduct: (e: MouseEvent<HTMLLIElement>,cat: typeProductItem) => void,
    activeMenuProduct: typeActiveCategory,
    listCategoryProduct: typeActiveCategory[],
}
export interface IDataItem {
    dataItem?: typeProductItem,
    disableAction?: boolean,
    handleAddToWishlist?: (e: MouseEvent<HTMLElement>,_id: string) => void,
}