import { Box } from '@mui/material';
import { WithStyles, withStyles } from '@mui/styles';
import { COLORS } from '@constants/colors';
import ButtonCustom from '@helpers/ButtonCustom';
import TextCustom from '@helpers/TextCustom';
import { get, isEmpty } from 'lodash';
import Link from 'next/link';
import styles from './styles';
import { IDataItem } from './types';
import Rating from '@mui/material/Rating';
import { useTranslation } from 'next-i18next';
import { convertGroupPrice, convertToDiscountStatus } from '@common/convertData';
import clsx from 'clsx';
import { routeRoot } from '@common/config';
import QuickAction from './QuickAction';
import { useDispatch } from 'react-redux';
import * as React from 'react';
import { toast } from 'react-toastify';

const ProductItemDefault = (props: IDataItem & WithStyles<typeof styles>) => {
    const {
        classes,
        dataItem,
        disableAction = false,
        handleAddToWishlist,
    } = props;
    
    const discount = convertToDiscountStatus(dataItem);
    const dispatch = useDispatch();
    const { t } = useTranslation();
    const slugFirstCategory = get(dataItem, ['categories', '0', 'slug'],'');
    const titleFirstCategory = get(dataItem, ['categories', '0', 'name'],'');
    const handleAddToCart = (e: React.MouseEvent<HTMLButtonElement>) => {
        e.stopPropagation();
        toast.success(String(t('addedToCart')));
    };

    const {price, sale_price} = convertGroupPrice(dataItem);
    return (
        <Box className={classes.productItem}>
            <Box className='product'>
                {discount.value && (
                    <Box className={clsx('discount-status', discount.css_class)}>
                        <TextCustom textXS
                            color={COLORS.white}>{t(discount.value)}</TextCustom>
                    </Box>
                )}
                <Box className="image">
                    {!isEmpty(dataItem?.image) && (
                        <>
                            <img src={dataItem?.image?.original} />
                        </>
                    )}
                </Box>
                <Box className="content">
                    <TextCustom textXS
                        hoverColor
                        className='mb-1'
                        color={COLORS.textBody}>
                        <Link href={`${routeRoot.category}/${slugFirstCategory}`}>
                            {titleFirstCategory}
                        </Link>
                    </TextCustom>
                    <Link href={`${routeRoot.product}/${dataItem?.slug}`}>
                        <a className='block w-fit'>
                            <TextCustom headingSM
                                className='mb-1 mt-1 title'
                                color={COLORS.textHeading}>{dataItem?.name}</TextCustom>
                        </a>
                    </Link>
                    <Box className="flex rating items-center mb-2">
                        <Rating
                            name="simple-controlled"
                            value={dataItem?.vote}
                            precision={0.5}
                            max={5}
                            size={'small'}
                            readOnly
                        />
                        <TextCustom textXS
                            className='!ml-3'
                            color={COLORS.textBody}>
                            {`(${dataItem?.total} items)`}
                        </TextCustom>
                    </Box>
                    {dataItem?.weight && (
                        <TextCustom textXS
                            className='!mt-1 !mb-2'
                            color={COLORS.textBody}>
                            {`${dataItem?.weight} gam`}
                        </TextCustom>
                    )}
                    <Box className="flex justify-between items-center">
                        <Box className="box-price flex items-end">
                            <TextCustom className='!mr-2'
                                h5
                                color={COLORS.brand1}>{
                                    sale_price || price
                                }</TextCustom>
                            <TextCustom
                                textXS
                                className='line-through'
                                color={COLORS.textBody}>
                                {sale_price && price}
                            </TextCustom>

                        </Box>
                        <ButtonCustom
                            onClick={handleAddToCart}
                            iconEnd={'fi-rs-plus-small'}
                            className='!h-7 !rounded button-add'
                        >
                            {t('add')}
                        </ButtonCustom>
                    </Box>
                </Box>
            </Box>
            {!disableAction && (
                <QuickAction
                    handleAddToWishlist={handleAddToWishlist}
                    dataItem={dataItem}
                />
            )}
        </Box>
    );
};

export default withStyles(styles)(ProductItemDefault);
