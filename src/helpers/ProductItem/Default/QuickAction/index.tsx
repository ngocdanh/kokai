import { WithStyles, withStyles } from '@mui/styles';
import React, { useState } from 'react';
import { Box } from '@mui/material';
import { IQuickAction } from './types';
import QuickView from '@helpers/ProductItem/Default/QuickAction/QuickView';
import styles from './styles';
import { get } from 'lodash';

const QuickAction = (props: IQuickAction & WithStyles<typeof styles>) => {
    const {
        classes,
        dataItem,
        handleAddToWishlist,
    } = props;

    const [open, setOpen] = useState<boolean>(false);
    const handleClose = () => setOpen(false);

    const handleQuickView = (slugItem: string | undefined) => {
        setOpen(true);
    };
    const productObjId = get(dataItem, '_id', '');
    return(
        <Box className={classes.QuickAction}>
            <Box className='quick-action'>
                <a aria-label='Quick View'
                    className='quick-view'
                    onClick={ () => handleQuickView(dataItem?.slug) }>
                    <i className="fi fi-rr-eye"></i>
                </a>
                <a aria-label='Wishlist'
                    className='wish-list'
                    onClick={(e: React.MouseEvent<HTMLElement>) => {
                        handleAddToWishlist && handleAddToWishlist(e, productObjId);
                    }}>
                    <i className="fi fi-rr-heart"></i>
                </a>
                <a aria-label='Compare'
                    className='compare'>
                    <i className="fi fi-rr-shuffle"></i>
                </a>
            </Box>
            {open && 
                <QuickView
                    open={open}
                    handleClose={handleClose}
                    slug={dataItem?.slug}
                ></QuickView>
            }
        </Box>
            
    );
};
export default withStyles(styles)(QuickAction);