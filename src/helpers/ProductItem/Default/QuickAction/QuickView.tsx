import styles from './styles';
import { WithStyles, withStyles } from '@mui/styles';
import React, { useState } from 'react';
import Backdrop from '@mui/material/Backdrop';
import Modal from '@mui/material/Modal';
import Fade from '@mui/material/Fade';
import { useQueryProductDetail } from '@lib/query';
import { Box, Skeleton } from '@mui/material';
import { IQuickView } from './types';
import TextCustom from '@helpers/TextCustom';
import { COLORS } from '@constants/colors';
import { useTranslation } from 'next-i18next';
import { convertGroupPrice } from '@common/convertData';
import Link from 'next/link';
import { routeRoot } from '@common/config';

const QuickView = (props: IQuickView  & WithStyles<typeof styles>) => {

    const {
        open,
        classes,
        handleClose,
        slug
    } = props;



    const { product , isLoading } = useQueryProductDetail({}, {slug});
    const { price , sale_price } = convertGroupPrice(product);
    const [quantity, setQuantity] = useState<number>(1);
    const { t } = useTranslation();

    const handleChangeQuantity = (e: React.FormEvent<EventTarget>) => {
        const target = e.target as HTMLInputElement;
        if(Number(target.value)){
            if(Number(target.value) > product?.quantity){
                setQuantity(product?.quantity);
            }else{
                setQuantity(Number(target.value));
            }
        }else{
            setQuantity(1);
        }
        
    };
  

    const handleIncreaseQuantity = () => {
        if(product?.quantity > quantity) {
            setQuantity(quantity + 1);
        }
    };
    const handleDecreaseQuantity = () => {
        if(quantity !== 1){
            setQuantity(quantity - 1);
        }
    };
  
    return (
        <React.Fragment>
            
            <Modal
                open={open}
                onClose={handleClose}
                closeAfterTransition
                BackdropComponent={Backdrop}
                BackdropProps={{
                    timeout: 500,
                }}
            > 
                <Fade in={open}>
                    <Box className={classes.Modal}>
                        {isLoading?
                            <Box className='flex gap-8'>
                                <Skeleton
                                    variant="rounded"
                                    width={'50%'}
                                    animation="wave"

                                    height={500}></Skeleton>

                                <Box className='content-skeleton flex flex-col gap-10 w-1/2'>
                                    <Skeleton variant="rounded"
                                        width={'100%'}
                                        animation="wave"

                                        height={110}>
                                      
                                    </Skeleton>
                                    <Skeleton variant="rounded"
                                        width={'80%'}
                                        animation="wave"

                                        height={90}>
                                    </Skeleton>
                                    <Skeleton variant="rounded"
                                        width={'100%'}
                                        animation="wave"

                                        height={70}>
                                      
                                    </Skeleton>
                                    <Skeleton variant="rounded"
                                        width={'100%'}
                                        animation="wave"

                                        height={80}>
                                      
                                    </Skeleton>
                                </Box>
                            </Box>
                            :
                            <Box className={classes.QuickView}>
                                
                                <Box className="image w-1/2">
                                    <img src={product?.image?.original}
                                        alt="Product Image"/>
                                </Box>
                                <Box className='quick-view-content w-1/2'>
                                    <i className="fi fi-br-cross close"
                                        onClick={(event: React.MouseEvent<HTMLElement> , reason='Close') => handleClose( event , reason)}></i>
                                    {product?.in_stock===1?
                                        <Box><TextCustom className="in-stock">
                                          In Stock
                                        </TextCustom></Box>
                                        :
                                        null
                                    }
                                    <Box className="product-name">
                                        <TextCustom
                                            h2
                                            color={COLORS.textHeading}>{product?.name}</TextCustom>
                                    </Box>
                                    <Box className='product-price flex items-end mt-2 mb-2'>
                                        <TextCustom className='!mr-4'
                                            display2
                                            color={COLORS.brand1}>{sale_price || price}</TextCustom>
                                        {product?.price && !!product?.sale_price && <TextCustom
                                            h3
                                            className='line-through pl-4 pb-2'
                                            color={COLORS.textBody}>{`$${product?.price}`}</TextCustom>}
                                    </Box>
                                    <Box className="add-to-cart mt-2 cursor-pointer flex gap-4">
                                        <Box className='input-quantity'>
                                            <input type="text"
                                                value={quantity}
                                                min={1}
                                                max={product?.quantity}
                                                onChange={(e) => handleChangeQuantity(e)}>
                                            </input>
                                            <Box className='up-down'>
                                                <i className="fi fi-br-angle-up"
                                                    onClick={handleIncreaseQuantity}></i>
                                                <i className="fi fi-br-angle-down"
                                                    onClick={handleDecreaseQuantity}></i> 
                                            </Box>
                                            
                                        </Box>
                                       
                                        <Box className="add-btn flex items-center">
                                            <i className="fi fi-rs-shopping-cart"></i>
                                            <TextCustom h6
                                                className='pl-3'>
                                                {t('addToCart')}
                                            </TextCustom>
                                        </Box>
                                    </Box>
                                    <Box className='quick-view-info'>
                                        <Box>
                                            <TextCustom textMedium >
                                                {`${t('vendor')}:`}
                                            </TextCustom>
                                            <TextCustom textMedium
                                                className='pl-2 text-muted'>
                                                {product?.shop?.name}
                                            </TextCustom>
                                        </Box>
                                        <Box className='mt-2'>
                                            <TextCustom textMedium >
                                            SKU: 
                                            </TextCustom>
                                            <TextCustom 
                                                textMedium  
                                                className='pl-2 text-muted'>
                                                {product?.sku?.toUpperCase()}
                                            </TextCustom>
                                        </Box>
                                    </Box>
                                    <Link href={`${routeRoot.product}/${product?.slug}`}>
                                        <a className='block w-fit mt-3  '>
                                            <TextCustom h6
                                                className='mb-1 mt-1 title'
                                                color={COLORS.brand1}>{t('viewFullDetail')}</TextCustom>
                                        </a>
                                    </Link>
                                </Box>
                            </Box>
                        }
                    </Box>
                </Fade>
            </Modal>
        </React.Fragment>
        
    );
};

export default withStyles(styles)(QuickView);