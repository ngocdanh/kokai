import { Theme } from '@mui/material';
import { createStyles } from '@mui/styles';
import { } from '@mui/styles/';
import { COLORS } from '@constants/colors';

const styles = (theme: Theme) => createStyles({
    root: { 
    },

    Modal: {
        position: 'absolute',
        top: '50%',
        left: '50%',
        transform: 'translate(-50%, -50%)',
        backgroundColor: COLORS.white,
        borderRadius: 40,
        boxShadow: '24',
        overflow: 'hidden',
        width: '70%',
        minWidth: 500,
        maxWidth: 1000,
        padding: 30,
    },

    QuickAction: {

        '& .quick-action': {
            backgroundColor: `${COLORS.white}`,
            border: `1px solid ${COLORS.border3}`,
            boxShadow: '5px 5px 15px rgb(0 0 0 / 5%)',
            borderRadius: 5,
            position: 'absolute',
            paddingLeft: 0,
            paddingRight: 0,
            left: '50%',
            top: '30%',
            transform: 'translateX(-50%) translateY(-50%)',
            visibility: 'hidden',
            opacity: 0,
            zIndex: 2,
            transition: 'all .5s ease',
            '& a': {
                width: 35,
                height: 35,
                padding: 0,
                borderLeft: `1px solid ${COLORS.border3}`,
                cursor: 'pointer',
                position: 'relative',
                lineHeight: '35px',
                fontSize: 14,
                color: `${COLORS.brand1}`,
                textAlign: 'center',
                transition: 'all 0.3s ease',
                display: 'inline-block',
                
                '&:before': {
                    position: 'absolute',
                    left: 'calc(50% - 7px)',
                    bottom: '100%',
                    border: '7px solid transparent',
                    borderTopColor: COLORS.brand1,
                    zIndex: 19,
                    content: '',
                    marginBottom: -13,
                    transition: 'all .3s ease,transform .3s cubic-bezier(.71,1.7,.77,1.24)',
                    opacity: 0,
                    visibility: 'hidden',
                },
                '&:after': {
                    bottom: '100%',
                    left: '-40%',
                    position: 'absolute',
                    whiteSpace: 'nowrap',
                    borderRadius: '5px',
                    fontSize: '11px',
                    padding: '7px 10px',
                    color: '#fff',
                    backgroundColor: '#3bb77e',
                    content: 'attr(aria-label)',
                    lineHeight: '1.3',
                    opacity: 0,
                    visibility: 'hidden',
                    transform: 'translateX(-50%) translateY(0)',
                },
                '&:hover': {
                    // transform: 'translateY(-5px)',
                    color: `${COLORS.brand2}`,
                    '&:before': {
                        opacity: 1,
                        visibility: 'visible',
                        transform: 'translateY(-8px)',
                    },
                    '&:after': {
                        opacity: 1,
                        visibility: 'visible',
                        transform: 'translateY(-8px)',
                    }
                }
            },
        },
    },

    QuickView: {
        display: 'flex',
        gap: 10,

        '& .close': {
            position: 'absolute',
            right: 30,
            top: 30,
            fontSize: 20,
            cursor: 'pointer',
        },

        '& .input-quantity': {
            display: 'flex',
            justifyContent: 'space-between',
            width: 120,
            alignItems: 'center',
            border: `2px solid ${COLORS.brand1}`,
            color: COLORS.brand1,
            fontSize: 20,
            borderRadius: 5,
            padding: '0px 15px 0px 20px',
            fontWeight: 700,
            '& input': {
                width: '100%',
            },
            '& .up-down': {
                display: 'flex',
                flexDirection: 'column',
                fontSize: 14,
                gap: 8,
            }
        },
        '& .quick-view-content': {
            display: 'flex',
            flexDirection: 'column',
            gap: 25,
          
            '& .in-stock': {
                fontWeight: 600,
                color: COLORS.brand1,
                backgroundColor: '#DEF9EC',
                padding: '4px 10px',
                borderRadius: 5,
            },
            '& .add-btn': {
                color: COLORS.white,
                backgroundColor: COLORS.brand1,
                border: `2px solid ${COLORS.brand1}`,
                borderRadius: 5,
                padding: '15px 30px',
                fontWeight: 700,
                transition: 'all 0.3s ease',
                '&:hover': {
                    backgroundColor: COLORS.white,
                    color: COLORS.brand1,
                }

            },
            '& .quick-view-info': {
                marginTop: 20,
                '& .text-muted': {
                    color: COLORS.textMuted,
                },
                
            },
      
        }  
        
    }
});

export default styles;