import { typeProductItem } from '@common/types';
import * as React from 'react';

export interface IQuickView {
    product?: {
        name: string,
        slug: string,
        in_stock: number,
        price: number | null,
        sale_price: number | null,
        description: string,
        categories: {
            id: number,
            name: string,
            slug: string,
        }[],
        quantity: number
    },
    slug: string | undefined,
    open: boolean,
    handleClose: ((event: React.MouseEvent<HTMLElement>, reason: any) => void),
    dataItem?: typeProductItem
}
export interface IQuickAction {
    handleAddToWishlist?: (e: React.MouseEvent<HTMLElement>,_id: string) => void,
    dataItem?: typeProductItem,
}