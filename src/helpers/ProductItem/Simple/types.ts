import { typeProductItem } from '@common/types';

export interface IProps {
    dataItem: typeProductItem
}

export type dataTrending = {
    listRecently: typeProductItem[],
    listTopRate: typeProductItem[],
    listTopSelling: typeProductItem[],
    listTrendingProducts: typeProductItem[],
};



export interface IProductTabs {
    dataTrending: dataTrending,
}