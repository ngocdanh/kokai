import { WithStyles, withStyles } from '@mui/styles';
import { Box } from '@mui/material';
import { COLORS } from '@constants/colors';
import TextCustom from '@helpers/TextCustom';
import { isEmpty } from 'lodash';
import styles from './styles';
import Link from 'next/link';
import Rating from '@mui/material/Rating';
import { convertGroupPrice } from '@common/convertData';
import { IProps } from './types';
import { routeRoot } from '@common/config';

const ProductItemSimple = (props: IProps & WithStyles <typeof styles>) => {
    const {
        classes,
        dataItem
    } = props;

    const { price , sale_price } = convertGroupPrice(dataItem);
    return (
        <Box className={ classes.productItemSimple }>
            <Box className="image">
                {!isEmpty(dataItem?.image) && (
                    <img src={dataItem?.image.original} />
                )}
            </Box>
            <Box className="content pl-5 pr-2">
                <Link href={`${routeRoot.product}/${dataItem?.slug}`}>
                    <a>
                        <TextCustom headingSM
                            className='mb-1 mt-1 title'
                            color={COLORS.textHeading}>{dataItem?.name}</TextCustom>
                    </a>
                </Link>
                <Box className="flex rating items-center mt-1">
                    <Rating
                        name="simple-controlled"
                        value={dataItem?.vote}
                        precision={0.5}
                        max={5}
                        size={'small'}
                        readOnly
                    />
                    <TextCustom textXS
                        className='ml-1'
                        color={COLORS.textBody}>
                        {`(${dataItem?.vote?.toFixed(1)})`}
                    </TextCustom>
                </Box>
                {dataItem?.weight && (
                    <TextCustom textXS
                        className='!mt-1 !mb-2'
                        color={COLORS.textBody}>
                        {`${dataItem?.weight} gam`}
                    </TextCustom>
                )}
                <Box className="flex justify-between items-center pt-5">
                    <Box className="box-price flex items-end">
                        <TextCustom className='!mr-2'
                            h5
                            color={COLORS.brand1}>{ sale_price || price }</TextCustom>  
                        {dataItem?.price && !!dataItem?.sale_price && <TextCustom
                            textXS
                            className='line-through'
                            color={COLORS.textBody}>{`$${dataItem.price}`}</TextCustom>}

                    </Box>
                </Box>
            </Box>
        </Box>
    );
};

export default withStyles(styles)(ProductItemSimple);