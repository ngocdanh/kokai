import { ShavingNeeds, Shirts, ShoulderBags, Skirts, Snacks, Sofa, Table, Tops, Wallet } from './category';
import { WomenDress } from './category/women-dress';
export { ArrowNextIcon as ArrowNext } from './arrow-next';
export { ArrowPrevIcon as ArrowPrev } from './arrow-prev';
export { default as Check } from './check-icon';

export const RenderIconCategory = (name: string)=> {
    switch(name) {
        case 'WomenDress': 
            return <WomenDress/>;
        case 'Wallet': 
            return <Wallet/>;
        case 'Tops': 
            return <Tops/>;
        case 'Table': 
            return <Table/>;
        case 'Shirts': 
            return <Shirts/>;
        case 'Sofa': 
            return <Sofa/>;
        case 'Snacks': 
            return <Snacks/>;
        case 'Skirts': 
            return <Skirts/>;
        case 'ShoulderBags': 
            return <ShoulderBags/>;
        case 'ShavingNeeds': 
            return <ShavingNeeds/>;
        default: 
            return null;
    }
};