import { Theme } from '@mui/material';
import {createStyles} from '@mui/styles';
import {} from '@mui/styles/';
import { COLORS } from '@constants/colors';

const styles = (theme: Theme) => createStyles({
    buttonCustom: {
        borderRadius: '10px',
        paddingLeft: 35,
        paddingRight: 35,
        height: 64,
        display: 'flex',
        justifyContent: 'center',
        transition: 'all .3s ease',
        alignItems: 'center',
        textTransform: 'none',
        maxHeight: '100%',
        '&:hover': {
            backgroundColor: `${COLORS.warring} !important`,
            color: `${COLORS.white} !important`,
            filter: 'drop-shadow(20px 20px 40px rgba(24, 24, 24, 0.07))'
        }
    },
    outline: {
        borderStyle: 'solid',
        borderWidth: 1,
        borderColor: `${COLORS.brand1} !important`,
        backgroundColor: `${COLORS.white} !important`,
        color: `${COLORS.brand1} !important`,
    
    },
    shadow: {
        boxShadow: '14px 14px 36px rgba(153, 153, 153, 0.22)',
        backgroundColor: `${COLORS.white} !important`,
        color: `${COLORS.brand1} !important`,
    },
    normal: {
        backgroundColor: `${COLORS.brand1} !important`,
        color: `${COLORS.white} !important`,
    },
    loading: {
        height: 50,
        paddingLeft: 30,
        paddingRight: 30,
        borderRadius: '30px',
        backgroundColor: `${COLORS.brand1} !important`,
        color: `${COLORS.white} !important`,
    },
    cartSmall: {
        borderRadius: '0px',
        height: 43,
        backgroundColor: `${COLORS.brand1} !important`,
        color: `${COLORS.white} !important`,
        fontSize: 14,
        lineHeight: 20,
        fontWeight: 700,
    },
    small: {
        height: 43,
        backgroundColor: `${COLORS.brand1} !important`,
        color: `${COLORS.white} !important`,
        fontSize: 16,
        fontWeight: 700,
        padding: '10px 18px',
        display: 'flex',
        alignItems: 'center',
        '& i': {
            display: 'flex',
            alignItems: 'center',
        }
    },
    cartBig: {
        borderRadius: '0px',
        height: 50,
        backgroundColor: `${COLORS.brand1} !important`,
        color:`${COLORS.white} !important`,
        fontSize: 18,
        lineHeight: 20,
        fontWeight: 700,
    },
});

export default styles;