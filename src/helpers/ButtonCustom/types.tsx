import { CSSProperties, MouseEvent, ReactNode } from 'react';

export interface IButtonCustom {
    children:ReactNode,
    color?: string,
    style?: CSSProperties,
    className?: string,
    outline?: boolean,
    shadow?: boolean,
    normal?: boolean,
    loading?: boolean,
    cartBig?: boolean,
    cartSmall?: boolean,
    small?: boolean,
    iconStart?: string, 
    height?: number,
    iconEnd?: string,
    borderRadius?: string,
    customStyle?: {
        root?: CSSProperties,
        iconStart?: CSSProperties,
        iconEnd?: CSSProperties,
    },
    onClick?: (event: MouseEvent<HTMLButtonElement>) => void,
}
