import { Box, Button } from '@mui/material';
import { withStyles, WithStyles } from '@mui/styles';
import clsx from 'clsx';
import { isEmpty } from 'lodash';
import styles from './styles';
import { IButtonCustom } from './types';

const ButtonCustom = (props: IButtonCustom & WithStyles<typeof styles>) => {
    const {
        children,
        className,
        color,
        style,
        outline,
        shadow,
        normal = true,
        loading,
        cartBig,
        cartSmall,
        small,
        classes,
        iconStart, 
        iconEnd,
        borderRadius,
        customStyle,
        onClick,
        ...otherProps
    } = props;
    return (
        <Button 
            className={clsx(
                className,
      
                classes.buttonCustom,
                'button-custom',
                {[classes.outline]: outline,
                    [classes.shadow]: shadow,
                    [classes.normal]: normal,
                    [classes.loading]: loading,
                    [classes.cartBig]: cartBig,
                    [classes.cartSmall]: cartSmall ,
                    [classes.small]: small,
                }
            )}
            style={{
                ...style,
                borderRadius,
                ...customStyle?.root,
            }}
            onClick={onClick}
            {...otherProps}
        >
            {!isEmpty(iconStart) && (
                <Box className={'mr-3'} 
                    style={customStyle?.iconStart}
                >
                    <i className={iconStart}/>
                </Box>
            )}
            {children}
            {!isEmpty(iconEnd) && (
                <Box className={''}
                    style={customStyle?.iconEnd}
                >
                    <i className={iconEnd} />
                </Box>
            )}
        </Button>
    );
};

export default withStyles(styles)(ButtonCustom);