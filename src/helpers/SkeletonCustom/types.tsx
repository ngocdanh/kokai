
export interface ISkeletonCustom {
    imgHeight?: number | string,
    textHeight?: number | string,
    imgWidth?: number | string,
    inline?: boolean,
    textWidth?: number | string,
    className?: string,
    disableText?: boolean,
    disableImage?: boolean,
    lineText?: number,
}

