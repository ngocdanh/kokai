import { Theme } from '@mui/material';
import { createStyles } from '@mui/styles';
import { } from '@mui/styles/';

const styles = (theme: Theme) => createStyles({
    root: {
        paddingTop: 33,
        paddingBottom: 33,
    },
    SkeletonCustom: {
        '& .MuiSkeleton-rounded': {
            borderRadius: 15
        },
        '& .list-text': {
            '& .MuiSkeleton-text': {
                '&:nth-child(2)': {
                    width: '80% !important'
                },
                '&:nth-child(3)': {
                    width: '78% !important'
                },
                '&:nth-child(4)': {
                    width: '85% !important'
                },
                '&:nth-child(5)': {
                    width: '90% !important'
                },
                '&:nth-child(6)': {
                    width: '70% !important'
                },
                '&:nth-child(7)': {
                    width: '95% !important'
                },
                '&:nth-child(8)': {
                    width: '86% !important'
                },
            }
        }
    },
    Inline: {
        
    }

});

export default styles;