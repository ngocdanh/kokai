import { WithStyles, withStyles } from '@mui/styles';
import {Box, Skeleton} from '@mui/material';
import styles from './styles';
import { ISkeletonCustom } from './types';
import React from 'react';
import clsx from 'clsx';
import { range, uniqueId } from 'lodash';

const SkeletonCustom = (props: ISkeletonCustom & WithStyles<typeof styles>) => {

    const {
        textWidth = '100%',
        inline,
        imgWidth = '100%',
        imgHeight = 180,
        textHeight = 35,
        classes,
        className,
        disableText= false,
        disableImage= false,
        lineText = 2,
    } = props;

    return(
        <Box className={clsx(
            classes.SkeletonCustom, 
            className,
            'skeleton-custom'
        )}
        sx={inline?{ display: 'flex'} : null}>
            {!disableImage && (
                <Box sx={inline?{ margin: 1 } : null}>
                    <Skeleton
                        variant='rounded'
                        width={inline? imgWidth || 100 : imgWidth}
                        height={imgHeight}
                        className={'img'}
                        // animation={'pulse'}
                    /></Box>
            )}
            {!disableText && (
                <Box 
                    sx={{ width: textWidth}}
                    className={'!mt-3 list-text'}>
                    {range(lineText).map(r => (
                        <Skeleton
                            key={uniqueId()}
                            variant='text'
                            width={'100%'}
                            height={textHeight}
                            animation={'wave'}
                        />
                    ))}
                </Box>
            )}
        </Box>
    );
};

export default withStyles(styles)(SkeletonCustom);