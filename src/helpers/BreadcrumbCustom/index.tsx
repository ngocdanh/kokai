import { COLORS } from '@constants/colors';
import TextCustom from '@helpers/TextCustom';
import { Box, Breadcrumbs } from '@mui/material';

import 'react-toastify/dist/ReactToastify.css';
import { WithStyles, withStyles } from '@mui/styles';
import { useTranslation } from 'next-i18next';
import styles from './styles';
import { IBreadcrumbCustom } from './types';
import { memo } from 'react';
import { isEmpty, uniqueId } from 'lodash';
import Link from 'next/link';

const BreadcrumbCustom = (props: IBreadcrumbCustom & WithStyles<typeof styles>) => {

    const { classes, list: listBreadcrumb } = props;
    const { t } = useTranslation();

    return(
        <Box className={classes.root}>
            <Box className={classes.Breadcrumb}>
                <Breadcrumbs aria-label="breadcrumb"
                    separator={<i className='fi-rs-angle-small-right text-muted text-lg mx-0'/>} >
                    {!isEmpty(listBreadcrumb) && listBreadcrumb?.map((breadcrumbItem, idx) => {
                        const {label, url = ''} = breadcrumbItem;
                        if(listBreadcrumb?.length - 1 === idx) {
                            return (
                                <TextCustom textLarge
                                    key={uniqueId()}
                                    className='pl-2 pr-2 active'
                                    color={COLORS.textBody}>{t(label ?? '')}</TextCustom>
                            );
                        }
                        return (
                            <Link
                                href={url}
                                key={uniqueId()}>
                                <a>
                                    <TextCustom textLarge
                                        className='pl-2 pr-2'
                                        color={COLORS.textBody}>{t(label ?? '')}</TextCustom>
                                    
                                </a>
                            </Link>
                        );
                    })}
                </Breadcrumbs>
                <br/>
                <hr/>
            </Box>
        </Box>
    );
};

export default memo(withStyles(styles)(BreadcrumbCustom));