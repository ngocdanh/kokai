import { COLORS } from '@constants/colors';
import { Theme } from '@mui/material';
import { createStyles } from '@mui/styles';
import { } from '@mui/styles/';

const styles = (theme: Theme) => createStyles({
    root: {
        paddingTop: 23,
        paddingBottom: 23,
        '& .MuiBreadcrumbs-separator': {
            margin: 0,
        }
    },
    Breadcrumb: {
        '& .text-custom.active': {
            color: COLORS.brand1
        },
        
    }
});

export default styles;