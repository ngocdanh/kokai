export interface IBreadcrumbCustom {
    list?: IBreadcrumbItem[],
}

export interface IBreadcrumbItem {
    url?: string,
    label: string | undefined,
}

export type Inputs = {
    username: string,
    email: string, 
    password: string,
    confirmpass: string,
    securitycode: string,
};