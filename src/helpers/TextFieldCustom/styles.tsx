import { COLORS } from '@constants/colors';
import { Theme } from '@mui/material';
import { createStyles } from '@mui/styles';
import { } from '@mui/styles/';

const styles = (theme: Theme) => createStyles({
    root: {
        paddingTop: 33,
        paddingBottom: 33,
    },

    TextFieldCustom: {
        width: '100%',
        '& input': {
            padding: '20px 30px',
            fontSize: 18,
            fontWeight: 500,
        },
        '& .MuiInputLabel-root': {
            paddingLeft: 30,
            lineHeight: 2.2
        },

        '& .MuiInputLabel-root.Mui-focused':{
            paddingLeft: 0,
            lineHeight: 1.4
        },
        '& .MuiInputLabel-root.MuiInputLabel-shrink':{
            paddingLeft: 0,
            lineHeight: 1.4
        },
        '& label.MuiInputLabel-root': {
            fontSize: 16,
            fontWeight: 500,
            color: COLORS.textMuted,
            transition: 'all 250ms ease',
        },
        '& label.Mui-focused': {
            color: COLORS.brand1,
        },
        '& .MuiInput-underline:after': {
            borderBottomColor: 'green',
        },
        '& .MuiOutlinedInput-root': {
            borderRadius: 10,
            '& fieldset': {
                borderColor: COLORS.borderColor,
            },
            '&:hover fieldset': {
                borderColor: COLORS.success,
            },
            '&.Mui-focused fieldset': {
                borderColor: 'green',
            },
        },
        '& .field-error': {
            fontWeight: 500,
            paddingLeft: 20,
            color: COLORS.danger,
            fontSize: 14,
            lineHeight: 2,
            paddingTop: 8,
            display: 'block',
            animation: '$shake 0.5s ease'
        },

        '& .input-field': {
            width: '100%',
        },
    },

    '@keyframes shake': {
        '0%': {transform: 'translateX(-18px)'},
        '20%': {transform: 'translateX(18px)'},
        '40%': {transform: 'translateX(-18px)'},
        '60%': {transform: 'translateX(18px)'},
        '80%': {transform: 'translateX(-8px)'},
        '100%': {transform: 'translateX(0px)'},
    }
});

export default styles;