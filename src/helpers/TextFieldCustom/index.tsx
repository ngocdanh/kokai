import {Controller} from 'react-hook-form';
import { WithStyles, withStyles } from '@mui/styles';
import {Box, TextField} from '@mui/material';
import styles from './styles';
import { ITextFieldCustom } from './types';
import React from 'react';
import clsx from 'clsx';

const TextFieldCustom = (props: ITextFieldCustom & WithStyles<typeof styles>) => {

    const { 
        classes,
        control, 
        defaultValue, 
        errors,
        name,
        label,
        value,
        className,
        disabled,
        type } = props;


    return(
        <Box className={classes.TextFieldCustom}>
            <Controller
                control={control}
                name={name}
                defaultValue={defaultValue || value || ''}
                render={({field}) => 
                    <TextField 
                        error={Boolean(errors)}
                        className={clsx('input-field', className)}
                        {...field}
                        onChange={field.onChange}
                        value={field.value}
                        type={type}
                        label={label}
                        disabled={disabled}
                        variant="outlined" />}
            />
            {errors && (
                <span className="field-error">{errors.message}</span>
            )}
        </Box>
    );
};

export default withStyles(styles)(TextFieldCustom);