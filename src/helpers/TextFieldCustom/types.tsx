import { FieldError } from 'react-hook-form';

export interface ITextFieldCustom {
    control: any,
    defaultValue?: string,
    errors?: FieldError,
    name: any,
    value?: string,
    label?: string,
    type?: string,
    className?: string,
    disabled?: boolean,
}

