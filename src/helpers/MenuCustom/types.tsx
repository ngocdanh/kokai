import { ReactNode } from 'react';

export interface IMenuCustom {
    children?:ReactNode,
    className?: string,
    body?: ReactNode,
    button?: ReactNode,
    paper?: string,
    anchorOrigin?:{ horizontal: 'center'
    | 'left'
    | 'right'
    | number, 
    vertical: 'bottom'
    | 'center'
    | 'top'
    | number }
}
