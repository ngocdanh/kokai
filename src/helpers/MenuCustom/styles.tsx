import { Theme } from '@mui/material';
import { createStyles } from '@mui/styles';
import { } from '@mui/styles/';
import { COLORS } from '@constants/colors';

const styles = (theme: Theme) => createStyles({
    menuCustom: {
    },
    paper: {
        border: `1px solid ${COLORS.bgGrey}`,
        borderRadius: '10px !important',
        padding: '25px 15px',
    },
});

export default styles;