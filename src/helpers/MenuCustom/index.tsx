import { Menu } from '@mui/material';
import { withStyles, WithStyles } from '@mui/styles';
import clsx from 'clsx';
import { IUserMenu } from '@lib/hook/types';
import { uniqueId } from 'lodash';
import styles from './styles';
import { IMenuCustom } from './types';

const MenuCustom = (props: IMenuCustom & IUserMenu &  WithStyles<typeof styles>) => {
    const {
        body,
        button,
        open,
        onClose,
        paper,
        classes,
        className,
        children,
        anchorOrigin,
    } = props;

    const isOpen = Boolean(open);
    return (
        <>
            {button}
            <Menu
                id={uniqueId('menu-custom')}
                anchorEl={open}
                disableScrollLock={true}
                open={isOpen}
                className={clsx(className, classes.menuCustom, 'menu-custom')}
                onClose={onClose}
                classes={{
                    paper: clsx(paper, classes.paper)
                }}
                elevation={0}
                anchorOrigin={anchorOrigin || {
                    vertical: 60,
                    horizontal: 0,
                }}
                transformOrigin={{
                    vertical: 'top',
                    horizontal: 'left',
                }}
                MenuListProps={{
                    'aria-labelledby': uniqueId('basic-button'),
                }}
            >
                {children && children}
                {body && body}
            </Menu>
        </>
    );
};

export default withStyles(styles)(MenuCustom);