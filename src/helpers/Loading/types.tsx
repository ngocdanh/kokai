import { ReactNode } from 'react';

export interface ILoading {
    time?: number,
    children: ReactNode
}
