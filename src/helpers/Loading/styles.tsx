import { Theme } from '@mui/material';
import {createStyles} from '@mui/styles';
import {} from '@mui/styles/';
import { COLORS } from '@constants/colors';

const styles = (theme: Theme) => createStyles({
    root: {
        position: 'fixed',
        top: 0,
        left: 0,
        bottom: 0,
        right: 0,
        backgroundColor: COLORS.white,
        display:'flex',
        justifyContent: 'center',
        alignItems: 'center',
        zIndex: 2000,
        '& img': {
            width: 80,
            height: 80,
            objectFit: 'contain',
        }
    }
});

export default styles;