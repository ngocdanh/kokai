import { Box } from '@mui/material';
import { withStyles, WithStyles } from '@mui/styles';
import clsx from 'clsx';
import imageAssets from '@constants/imageAssets';
import React, { useState } from 'react';
import styles from './styles';
import { ILoading } from './types';

const Loading = (props: ILoading &  WithStyles<typeof styles>) => {
    const {
        time = 600,
        children,
        classes,
    } = props;

    const [loading, setLoading] = useState(true);

    React.useEffect(()=> {
        const timeOutLoading = setTimeout(() => {
            setLoading(false);
        }, time);
        return () => {
            clearTimeout(timeOutLoading);
        };
    }, [time]);

    return (
        loading ? <Box className={clsx(classes.root)}>
            <img src={imageAssets.loading}/>
        </Box>
            :
            <Box>
                {children}
            </Box>
    );
};

export default withStyles(styles)(Loading);