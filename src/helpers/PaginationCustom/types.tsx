export interface IPaginationCustom {
    page: number | undefined,
    count?: number | undefined,
    className?: string,
    handleChange?: (e: React.ChangeEvent<unknown>, value: number) => void
}
