import { Box, Pagination, PaginationItem } from '@mui/material';

import 'react-toastify/dist/ReactToastify.css';
import { WithStyles, withStyles } from '@mui/styles';
import { useTranslation } from 'next-i18next';
import styles from './styles';
import { IPaginationCustom } from './types';
import ArrowBackIcon from '@mui/icons-material/ArrowBack';
import ArrowForwardIcon from '@mui/icons-material/ArrowForward';
import clsx from 'clsx';

const PaginationCustom = (props: IPaginationCustom & WithStyles<typeof styles>) => {
    const { classes, page, count, className, handleChange, ...otherProps} = props;
    
    const { t } = useTranslation();

    return(
        <Box className={classes.root}>
            <Pagination 
                className={clsx('pagination-custom', className)}
                count={Number(count || 0)}
                onChange={handleChange}
                page={Number(page || 0)}
                renderItem={(item) => (
                    <PaginationItem 
                        {...item}
                        components ={{ previous: ArrowBackIcon, next: ArrowForwardIcon  }}
                    />
                )}
                {...otherProps}
            />
        </Box>
    );
};

export default withStyles(styles)(PaginationCustom);