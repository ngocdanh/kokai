import { COLORS } from '@constants/colors';
import { Theme } from '@mui/material';
import { createStyles } from '@mui/styles';
import { } from '@mui/styles/';

const styles = (theme: Theme) => createStyles({
    root: {
        '& .pagination-custom': {
            '& .MuiPagination-ul': {
                '& button.MuiPaginationItem-root': {
                    width: 40,
                    height: 40,
                    borderRadius: '50%',
                    color: COLORS.textBody,
                    backgroundColor: COLORS.bgGrey,
                    fontWeight: 700,
                    fontSize: 16,
                    '&:hover': {
                        backgroundColor: COLORS.brand1Light,
                        color: COLORS.white
                    },
                    '&.Mui-selected': {
                        backgroundColor: COLORS.brand1,
                        color: COLORS.white
                    },
                    '&.MuiPaginationItem-previousNext': {
                        '&:hover': {
                            backgroundColor: COLORS.brand1,
                        }
                    }
                }
            }
        }
    },
});

export default styles;