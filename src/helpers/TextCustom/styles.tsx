import { COLORS } from '@constants/colors';

const styles = {
    display1: {
        fontSize: 96,
        lineHeight: '128px',
        letterSpacing: '0px',
        fontWeight: 700,
    },
    display2: {
        fontSize: 72,
        lineHeight: '96px',
        letterSpacing: '0px',
        fontWeight: 700,
        '@media (max-width: 1700px)': {
            fontSize: 60,
            lineHeight: '75px',
        }
    },
    h1: {
        fontSize: 46,
        lineHeight: '64px',
        letterSpacing: '0px',
        fontWeight: 700,
    },
    h2: {
        fontSize: 40,
        lineHeight: '48px',
        letterSpacing: '0px',
        fontWeight: 700,
    },
    h3: {
        fontSize: 32,
        lineHeight: '40px',
        letterSpacing: '0px',
        fontWeight: 700,
    },
    h4: {
        fontSize: 24,
        lineHeight: '32px',
        letterSpacing: '0px',
        fontWeight: 700,
    },
    h5: {
        fontSize: 20,
        lineHeight: '24px',
        letterSpacing: '0px',
        fontWeight: 700,
    },
    h6: {
        fontSize: 16,
        lineHeight: '20px',
        letterSpacing: '0px',
        fontWeight: 700,
    },
    headingSM: {
        fontSize: 14,
        lineHeight: '18px',
        letterSpacing: '0px',
        fontWeight: 700,
    },
    textLarge: {
        fontSize: 18,
        lineHeight: '26px',
        letterSpacing: '0px',
        fontWeight: 500,
    },
    textMedium: {
        fontSize: 16,
        lineHeight: '24px',
        letterSpacing: '0px',
        fontWeight: 500,
    },
    textSmall: {
        fontSize: 14,
        lineHeight: '21px',
        letterSpacing: '0px',
        fontWeight: 500,
    },
    textXS: {
        fontSize: 12,
        lineHeight: '18px',
        letterSpacing: '0px',
        fontWeight: 500,
    },
    textMuted: {
        fontSize: 12,
        lineHeight: '18px',
        letterSpacing: '0px',
        fontWeight: 400,
    },
    hoverColor: {
        transition: 'all .3s ease',
        '&:hover': {
            color: COLORS.brand1,
        }
    }
};

export default styles;