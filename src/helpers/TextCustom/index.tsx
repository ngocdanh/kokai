import { Typography } from '@mui/material';
import clsx from 'clsx';
import styles from './styles';
import { IText } from './types';

const TextCustom = (props: IText) => {
    const {
        children,
        className,
        color,
        style,
        display1,
        display2,
        h1,
        h2,
        h3,
        h4,
        h5,
        h6,
        headingSM,
        textLarge,
        textMedium,
        textSmall,
        hoverColor,
        textMuted,
        textXS,
        center,
        ...otherProps
    } = props;
    return (
        <Typography 
            className={clsx(className, 'text-custom')}
            style={{
                ...style,
            }}
            component='div'
            display={'inline-block'}
            sx={{
                color,
                ...display1 && styles.display1,
                ...display2 && styles.display2,
                ...h1 && styles.h1,
                ...h2 && styles.h2,
                ...h3 && styles.h3,
                ...h4 && styles.h4,
                ...h5 && styles.h5,
                ...h6 && styles.h6,
                ...headingSM && styles.headingSM,
                ...textLarge && styles.textLarge,
                ...textMedium && styles.textMedium,
                ...textSmall && styles.textSmall,
                ...textXS && styles.textXS,
                ...textMuted && styles.textMuted,
                ...hoverColor && styles.hoverColor,
                ...center && {
                    width: '100%',
                    textAlign: 'center'
                }
            }}
            {...otherProps}
        >
            {children}
        </Typography>
    );
};

export default TextCustom;