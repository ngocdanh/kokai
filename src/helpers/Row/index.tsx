import { Box } from '@mui/material';
import { withStyles, WithStyles } from '@mui/styles';
import clsx from 'clsx';
import styles from './styles';
import { IRow } from './types';

const Row = (props: IRow & WithStyles<typeof styles>) => {
    const {
        classes,
        children,
        justifyContent,
        alignItems,
        className,
        display,
        ...otherProps
    } = props;
    return (
        <Box className={className}>
            <Box 
                className={clsx(
                    'row',
                    classes.root,
          
                )}
                component={'div'}
                justifyContent={justifyContent}
                alignItems={alignItems}
                display={display}
                {...otherProps}
            >
                {children}
            </Box>
        </Box>
    
    );
};

export default withStyles(styles)(Row);