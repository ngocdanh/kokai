import { Theme } from '@mui/material';
import { createStyles } from '@mui/styles';
import { } from '@mui/styles/';

const styles = (theme: Theme) => createStyles({
    root: {
        maxWidth: '1584px',
        margin: '0 auto',
        width: '100%',
        paddingLeft: 15,
        paddingRight: 15,
    }
});

export default styles;