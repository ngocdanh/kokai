import { ReactNode } from 'react';

export interface IRow {
    className?: string,
    children: ReactNode,
    justifyContent?: string,
    alignItems?: string,
    display?: string,
}
 