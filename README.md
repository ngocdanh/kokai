## Bắt đầu


### Localhost
- nvm use 14 (nodejs version 14)
- Cài đặt docker => chạy file docker-compose.dev.yml
- Mở trình duyệt localhost:1900
## Thành viên tham gia: 
- Ngọc Danh (danhnotes@gmail.com) - Team Lead + FE + BE
- Văn Minh Trường (@vanmintruong) - FE
- Lê Mai Linh (@lemailinh22) - FE
- Tiến (@tienco201) - FE
## Website development
https://kokai.vercel.app/

## Thông tin dự án:
Dự án buid bằng React js (Next js) + Node js(Express js) + noSQL(MongoDB)
- Tìm icon SVG: https://www.flaticon.com/uicons
- Tailwind css cheat sheets: https://tailwindcomponents.com/cheatsheet/
- Trang chủ MUI: https://mui.com
- React query: https://react-query-v3.tanstack.com/