# Build target dependencies #
FROM node:14-alpine AS deps

RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app
COPY package*.json ./
RUN npm install

# Build target builder #
FROM node:14-alpine AS builder

WORKDIR /usr/src/app
ENV PORT=1910
ENV NODE_ENV=production
ENV API_URL=https://kokai-be.onrender.com/
ENV GOOGLE_RECAPTCHA_KEY=6LeXZSIjAAAAAFpYQHrwZowOiezh5w-l_6dzJz6L
COPY . .
COPY --from=deps /usr/src/app/node_modules ./node_modules
RUN npm run build

# Build target development #
FROM node:14-alpine AS runner
WORKDIR /usr/src/app
COPY . .
COPY --from=builder /usr/src/app/next.config.js ./
COPY --from=builder /usr/src/app/public ./public
COPY --from=builder /usr/src/app/.next ./.next
COPY --from=builder /usr/src/app/package.json ./
COPY --from=builder /usr/src/app/node_modules ./node_modules
CMD ["npm", "run", "dev"]